#!/usr/bin/env python
from distutils.core import setup, Command
from distutils.command.build import build
from distutils.command.sdist import sdist
import subprocess
import glob
import os
import codecs
import re


here = os.path.abspath(os.path.dirname(__file__))

# Read the version number from a source file.
# Why read it, and not import?
# see https://groups.google.com/d/topic/pypa-dev/0PkjVpcxTzQ/discussion
def find_version(*file_paths):
    # Open in Latin-1 so that we avoid encoding errors.
    # Use codecs.open for Python 2 compatibility
    with codecs.open(os.path.join(here, *file_paths), 'r', 'latin1') as f:
        version_file = f.read()

    # The version line must have the form
    # __version__ = 'ver'
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return str(version_match.group(1))
    raise RuntimeError("Unable to find version string.")


def is_newer(file_a, file_b):
    return not os.path.exists(file_b) or os.path.getmtime(file_a) >= os.path.getmtime(file_b)


def build_protobuf(subdir):
    for protofile in glob.glob('%s/*.proto' % subdir):
        target = protofile.replace('.proto', '_pb2.py')
        if is_newer(protofile, target):
            subprocess.call(['protoc',
                             '-I=%s' % subdir,
                             '--python_out=%s' % subdir,
                             protofile])

class PbBuild(build):
    def run(self):
        build_protobuf(os.path.join('pydp', 'messages'))
        build.run(self)

class BuildPb(Command):
    user_options = []
    description = 'Compile Protocol Buffer description files'

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        build_protobuf(os.path.join('pydp', 'messages'))

# Adapted from http://guide.python-distribute.org/specification.html#development-releases
class my_sdist(sdist):
    user_options = sdist.user_options + [
        ('dev', None, 'Add development version tag')
    ]
    def initialize_options(self):
        sdist.initialize_options(self)
        self.dev = 0

    def run(self):
        if self.dev:
            suffix = '.dev%s' % self.get_head_revision()
            self.distribution.metadata.version += suffix
        build_protobuf(os.path.join('pydp', 'messages'))
        sdist.run(self)

    def get_head_revision(self):
        from subprocess import Popen, PIPE
        cmdline = 'git log -n 1 --pretty=oneline'
        output = Popen(cmdline.split(), stdout=PIPE).communicate()[0]
        rev = (output.strip().split())[0]
        return rev[0:7]


setup(name="Pydp",
      version=find_version('pydp', '__init__.py'),
      cmdclass={'sdist': my_sdist,
                'build': PbBuild,
                'build_pb': BuildPb},
      description="RSN Deep Profiler software",
      author="Michael Kenney",
      author_email="mikek@apl.washington.edu",
      url="http://wavelet.apl.uw.edu/~mike/python/",
      packages=["pydp", "pydp.sensors", "pydp.messages"],
      scripts=["scripts/bmon.py",
               "scripts/bcheck.py",
               "scripts/bmonitor.py",
               "scripts/sensmgr.py",
               "scripts/mmp_mgr.py",
               "scripts/imm_mgr.py",
               "scripts/archiver.py",
               "scripts/dpcapp.py",
               "scripts/dpcmon.py",
               "scripts/dp_server.py",
               "scripts/dp_client.py",
               "scripts/dp_msg.py",
               "scripts/loadcfg.py",
               "scripts/loadsched.py",
               "scripts/dockmon.py",
               "scripts/dp_snmp.py",
               "scripts/dp_tui.py"])
