#!/usr/bin/env python

import unittest
from mock import Mock
from tempfile import mkstemp
from cStringIO import StringIO
import os
from pydp import imm

def gen_simple_command(pathname, command):
    f = open(pathname, 'w')
    xmlout = '%s\r\n<Executed/>' % command
    f.write('%s\r\nIMM>' % xmlout)
    f.close()
    return xmlout

def gen_error(pathname):
    f = open(pathname, 'w')
    xmlout = 'foo\r\n<Error type="INVALID COMMAND" msg="Not Recognized"/>'
    f.write('%s\r\nIMM>' % xmlout)
    f.close()
    return xmlout

def gen_invalid_chars(pathname, command):
    f = open(pathname, 'w')
    xmlout = '%s\r\n\x01<Output>\x02content</Output>\x03<Executed/>' % command
    f.write('%s\r\nIMM>' % xmlout)
    f.close()
    return xmlout
    
def gen_remote_command(pathname, command):
    f = open(pathname, 'w')
    xmlout = '''%s
    <RemoteReply>
    <SomeTag>
    some content
    </SomeTag>
    <Executed/>
    </RemoteReply>
    <Executed/>''' % command
    f.write('%s\r\nIMM>' % xmlout)
    f.close()
    return xmlout
    
def gen_first_part(pathname, command):
    f = open(pathname, 'w')
    xmlout = '''%s
    <Executing/>
    ''' % command
    f.write('%s\r\n' % xmlout)
    f.close()
    return xmlout

def gen_second_part(pathname):
    f = open(pathname, 'w')
    xmlout = '<Output>some content</Output>\r\n<Executed/>'
    f.write('%s\r\nIMM>' % xmlout)
    f.close()
    return xmlout

def strip_eol(s):
    l = s.splitlines()
    return ' '.join(l)
    
class ImmTest(unittest.TestCase):
    def setUp(self):
        self.log = StringIO()
        self.imm = imm.IMM('/dev/ttyUSB0', sessionlog=self.log)
        self.imm.serial = Mock()
        fd, self.pathname = mkstemp()
        os.close(fd)
        
    def tearDown(self):
        os.unlink(self.pathname)
        
    def test_open_close(self):
        """Test interface open and close"""
        self.imm.open()
        self.assertEqual(len(self.imm.stanzas), 0)
        self.imm.close()
        result = self.log.getvalue()
        self.assertTrue(self.imm.serial.open.called, 'serial.open() not called!')
        self.assertTrue(self.imm.serial.nonblocking.called, 'serial.nonblocking() not called')
        self.assertTrue(self.imm.serial.close.called, 'serial.close() not called')
        #self.assertEqual('<stream/>\n', result, 'Unexpected result: %r' % result)
        
    def test_simple_command(self):
        """Test parsing the output of a simple command"""
        xmlout = gen_simple_command(self.pathname, 'foo')
        f = open(self.pathname, 'r')
        self.imm.serial.fileno.return_value = f.fileno()
        self.imm.open()
        is_done = self.imm.on_input()
        self.assertTrue(is_done, 'parsing error!')
        results = list(self.imm.results())
        self.assertEqual('Executed', results[0].tag, 'Unexpected result: %r' % results)
        self.imm.close()
        #output = self.log.getvalue()
        #expected = '<stream>%s</stream>' % xmlout
        #self.assertEqual(strip_eol(expected), strip_eol(output),
        #                 'Unexpected result %r' % output)

    def test_error(self):
        """Test error detection."""
        xmlout = gen_error(self.pathname)
        f = open(self.pathname, 'r')
        self.imm.serial.fileno.return_value = f.fileno()
        self.imm.open()
        is_done = self.imm.on_input()
        self.assertTrue(is_done, 'parsing error!')
        self.assertRaises(imm.IMMError, lambda : list(self.imm.results()))
        self.imm.close()
        
    def test_garbled_output(self):
        """Test parsing in the presence of line noise"""
        xmlout = gen_invalid_chars(self.pathname, 'foo')
        f = open(self.pathname, 'r')
        self.imm.serial.fileno.return_value = f.fileno()
        self.imm.open()
        is_done = self.imm.on_input()
        self.assertTrue(is_done, 'parsing error!')
        results = list(self.imm.results())
        self.assertEqual('Output', results[0].tag, 'Unexpected result: %r' % results)
        self.imm.close()
        
    def test_remote_command(self):
        """Test parsing the output of a remote command"""
        xmlout = gen_remote_command(self.pathname, '!01foo')
        f = open(self.pathname, 'r')
        self.imm.serial.fileno.return_value = f.fileno()
        self.imm.open()
        is_done = self.imm.on_input()
        self.assertTrue(is_done, 'parsing error!')
        results = list(self.imm.results())
        self.assertEqual('RemoteReply', results[0].tag,
                         'Unexpected result %r' % results[0].tag)
        self.imm.close()
        #output = self.log.getvalue()
        #expected = '<stream>%s</stream>' % xmlout
        #self.assertEqual(strip_eol(expected), strip_eol(output),
        #                 'Unexpected result %r' % output)
        
    def test_delayed_output(self):
        """Test parsing the output of a long-running command"""
        xmlout = gen_first_part(self.pathname, 'foo')
        f = open(self.pathname, 'r')
        self.imm.serial.fileno.return_value = f.fileno()
        self.imm.open()
        is_done = self.imm.on_input()
        self.assertFalse(is_done, 'parsing error!')
        self.assertEqual(len(self.imm.stanzas), 1)
        f.close()
        xmlout = xmlout + gen_second_part(self.pathname)
        f = open(self.pathname, 'r')
        self.imm.serial.fileno.return_value = f.fileno()
        self.imm.open()
        is_done = self.imm.on_input()
        self.assertTrue(is_done, 'parsing error!')
        results = list(self.imm.results())
        self.assertEqual('Output', results[1].tag, 'Unexpected result: %r' % results)
        self.imm.close()
        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(ImmTest)
    unittest.TextTestRunner(verbosity=2).run(suite)