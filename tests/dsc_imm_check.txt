IMM Checkout for DSC
====================

Power the device on, test communications and power off

Initial imports::

    >>> from pydp.dio import Ts4800Dio
    >>> from pydp.power import PowerSwitch
    >>> from pydp.imm2 import IMM
    >>> from pydp.serialcom import Port
    >>> import time

Create Port object::

    >>> p = Port('/dev/ttymxc2', baud=9600)

Power the device on, and pause to allow for start-up::

    >>> dioport = Ts4800Dio()
    >>> sw = PowerSwitch(1, dioport)
    >>> sw.on()
    >>> time.sleep(1)
    >>> p.open()

Create the IMM object and wake the device up::

    >>> imm = IMM(p)
    >>> imm.activate()
    >>> imm.wakeup()

Try setting an IMM parameter::

    >>> imm.set('tmodem4', 200)
    >>> imm.get('tmodem4')
    '200'

Put the device to sleep and wait for a message. The first call to readline always
returns an empty message. The next call will timeout and return ``None``::

    >>> imm.sleep()
    >>> t, msg = imm.readline(1)
    >>> msg
    bytearray(b'')
    >>> imm.readline(1)
    (None, None)

Shut the device down.

    >>> imm.deactivate()
    >>> p.close()
    >>> sw.off()
