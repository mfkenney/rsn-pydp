#!/usr/bin/env python

import unittest
import random
import struct
from string import ascii_letters
from pydp.serialsens import FramedRecordParser, DelimitedRecordParser
from pydp.sensors import AcsRecordParser

STX = b'$'
ETX = b'\n'
DELIM = b'\r\n'

class DelimitedRecordTest(unittest.TestCase):
    def setUp(self):
        self.p = DelimitedRecordParser(DELIM)
        
    def test_single_record(self):
        """Test parsing a single delimited record"""
        content = b'this is a test'
        output = content + DELIM
        for c in output:
            self.p.feed(c)
        n = len(self.p)
        self.assertEqual(n, 1, 'Invalid record count, %d should be 1' % n)
        _, rec = self.p.next()
        self.assertEqual(rec, content, 'Invalid record content: %r' % rec)
        n = len(self.p)
        self.assertEqual(n, 0, 'Invalid record count, %d should be 0' % n)
    
class FramedRecordTest(unittest.TestCase):
    def setUp(self):
        self.p = FramedRecordParser(STX, ETX)
        
    def test_single_record(self):
        """Test parsing a single framed record"""
        content = b'this is a test'
        output = '%s%s%s' % (STX, content, ETX)
        for c in output:
            self.p.feed(c)
        n = len(self.p)
        self.assertEqual(n, 1, 'Invalid record count, %d should be 1' % n)
        _, rec = self.p.next()
        self.assertEqual(rec, content, 'Invalid record content: %r' % rec)
        n = len(self.p)
        self.assertEqual(n, 0, 'Invalid record count, %d should be 0' % n)
        
    def test_multiple_records(self):
        """Test parsing multiple framed records"""
        content = [b'this is record 1',
                   b'this is record 2',
                   b'this is record 3']
        for s in content:
            self.p.feed(STX)
            for c in s:
                self.p.feed(c)
            self.p.feed(ETX)
            for i in range(10):
                self.p.feed(random.choice(ascii_letters))
        n0 = len(content)
        n1 = len(self.p)
        self.assertEqual(n0, n1, 'Invalid record count, %d should be %d' % (n1, n0))
        for i, data in enumerate(self.p):
            _, rec = data
            self.assertEqual(rec, content[i], 'Invalid record content: %r' % rec)
        n = len(self.p)
        self.assertEqual(n, 0, 'Invalid record count, %d should be 0' % n)


class AcsRecordTest(unittest.TestCase):
    def setUp(self):
        self.p = AcsRecordParser()
            
    def test_single(self):
        """Test reading a single AC-S record"""
        s = 'hello world'
        n = len(s) + 6
        content = struct.pack('>h%ds' % len(s), n, s) + b'\x00\x00\x00'
        record = b'\xff\x00\xff\x00' + content
        for c in record:
            self.p.feed(c)
        n = len(self.p)
        self.assertEqual(n, 1, 'Invalid record count, %d should be 1' % n)
        _, rec = self.p.next()
        self.assertEqual(rec, content, 'Invalid record content: %r' % rec)
        n = len(self.p)
        self.assertEqual(n, 0, 'Invalid record count, %d should be 0' % n)
        
if __name__ == '__main__':
    for cls in (DelimitedRecordTest, FramedRecordTest, AcsRecordTest):
        suite = unittest.TestLoader().loadTestsFromTestCase(cls)
        unittest.TextTestRunner(verbosity=2).run(suite)