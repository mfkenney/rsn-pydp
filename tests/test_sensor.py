#!/usr/bin/env python

import unittest
from mock import Mock
from tempfile import mkstemp
from cStringIO import StringIO
from decimal import Decimal
import sys
import os
import gevent
import yaml
import _socket
import re
#sys.path.insert(0, os.path.join('..', 'pydp'))
from pydp.sensors import Sbe52, Paros, Sbe52Auto, Acs
from pydp.serialsens import SensorError, TimeoutError
from support import start_client, start_server, start_server_with_handler

SBE_VALUES = b'35.4789,  6.9892,  182.25, 6.768'
PAROS_VALUES = b'55.444'
paros_responses = {
    b'*0100EW*0100PI=700': b'*0001PI=700\r\n',
    b'*0100EW *0100UF = 0.683853': b'*0001UF = 0.683853\r\n',
    b'foo': b'',
    b'*0100P3': b'*0001%s\r\n' % PAROS_VALUES
}

def try_read(s, n):
    try:
        gevent.socket.wait_read(s.fileno(), timeout=1)
        return s.recv(n)
    except Exception:
        return ''
    
def sock_readline(s):
    line = []
    c = try_read(s, 1)
    while True:
        if c == b'\n':
            break
        line.append(c)
        c = try_read(s, 1)
    return ''.join(line)

def sbe_sim(ev, socket, addr):
    cmds = (b'pumpon', b'pumpoff', b'outputctdo', b'overwritemem', b'autobinavg',
            b'pcutoff', b'stopprofile')
    record = SBE_VALUES
    socket.setblocking(0)
    try:
        sock_readline(socket)
        while True:
            socket.send(b'\r\nS>')
            cmd = sock_readline(socket)
            if not cmd:
                break
            cmd = (cmd.strip().split('='))[0]
            if cmd == '' or cmd in cmds:
                continue
            elif cmd == 'ds':
                socket.send('device settings')
            elif cmd == 'dc':
                socket.send('device calibration')
            elif cmd == b'ts':
                socket.send(b'\r\n' + record)
            elif cmd == b'startprofile':
                socket.send(b'\r\nS>')
                while True:
                    socket.send(record + b'\r\n')
                    gevent.sleep(0.4)
                    if try_read(socket, 2):
                        break
            else:
                socket.send('?CMD')
    except (gevent.GreenletExit, _socket.error):
        pass
    ev.set()
    
class SerializationTest(unittest.TestCase):
    def __init__(self, cls):
        super(SerializationTest, self).__init__()
        self.cls = cls
        
    def runTest(self):
        sens = self.cls('/dev/ttyUSB0')
        s = yaml.dump(sens)
        obj = yaml.load(s)
        self.assertEqual(sens.__class__, obj.__class__)
        self.assertEqual(sens.serial.port, obj.serial.port)
        self.assertEqual(sens.stages, obj.stages)
        self.assertEqual(sens.name, obj.name)
        
    def shortDescription(self):
        return 'Testing serialization of %s' % self.cls.__name__


class CtdTest(unittest.TestCase):
    def setUp(self):
        self.log = StringIO()
        self.ctd = Sbe52('/dev/ttyUSB0')
        self.ctd.serial = Mock()
        self.pathname = '/tmp/ctd.sock'
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.task = gevent.spawn(start_server_with_handler, self.pathname, sbe_sim)
        
    def tearDown(self):
        self.task.join(timeout=2)
        if not self.task.ready():
            self.task.kill(timeout=2)
        os.unlink(self.pathname)
        #print self.log.getvalue()
     
    def test_ctd_session(self):
        """Test CTD session"""
        start_seq = [('connect', 'connected'),
                     ('sample', 'ready')]
        end_seq = [('pause', 'connected'),
                   ('disconnect', 'disconnected')]
        sock = start_client(self.pathname)
        self.ctd.serial.fileno.return_value = sock.fileno()
        self.ctd.start()
        self.assertEqual(self.ctd.state, 'disconnected', 'Invalid state: %r' % self.ctd.state)
        m = self.ctd.get_metadata()
        self.assertEqual(m['calibration'], 'n/a')
        self.assertEqual(m['setup'], 'n/a')
        for ev, state in start_seq:
            r = self.ctd.on_event(ev).get()
            self.assertEqual(self.ctd.state, state, 'Invalid state: %r' % self.ctd.state)
            if state == 'connected':
                m = self.ctd.get_metadata()
                self.assertNotEqual(m['calibration'], 'n/a')
                self.assertNotEqual(m['setup'], 'n/a')
        t, data = self.ctd.on_event('run').get()
        for ev, state in end_seq:
            r = self.ctd.on_event(ev).get()
            self.assertEqual(self.ctd.state, state, 'Invalid state: %r' % self.ctd.state)
        self.ctd.stop()
        self.assertTrue(self.ctd.ready())
        values = [Decimal(v.decode('ascii')) for v in SBE_VALUES.split(',')]
        for i, v in enumerate(values):
            self.assertEqual(data[i][1], v, 'Unexpected value for %s (%r)' % data[i])
        sock.close()
        
    def test_error(self):
        """Test CTD error detection"""
        sock = start_client(self.pathname)
        self.ctd.serial.fileno.return_value = sock.fileno()
        self.ctd.start()
        self.ctd.do_connect().get()
        self.assertEqual(self.ctd.state, 'connected', 'Invalid state: %r' % self.ctd.state)
        r = self.ctd.do_command(b'foo')
        self.assertRaises(SensorError, r.get)
        r = self.ctd.do_disconnect().get()
        self.assertEqual(self.ctd.state, 'disconnected', 'Invalid state: %r' % self.ctd.state)
        self.ctd.stop()
        self.assertTrue(self.ctd.ready())
        sock.close()
        
    def test_timeout(self):
        """Test CTD timeout detection"""
        sock = start_client(self.pathname)
        self.ctd.serial.fileno.return_value = sock.fileno()
        # Don't spawn the Greenlet for this test
        self.ctd.open()
        self.assertRaises(TimeoutError, self.ctd.recv, 2)
        self.ctd.close()
        sock.close()
        
class CtdStreamTest(unittest.TestCase):
    def setUp(self):
        self.log = StringIO()
        self.ctd = Sbe52Auto('/dev/ttyUSB0', name='ctd/1')
        self.ctd.serial = Mock()
        self.pathname = '/tmp/ctd.sock'
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.task = gevent.spawn(start_server_with_handler, self.pathname, sbe_sim)
        
    def tearDown(self):
        self.task.join(timeout=2)
        if not self.task.ready():
            self.task.kill(timeout=2)
        os.unlink(self.pathname)
        #print self.log.getvalue()
     
    def test_ctd_session(self):
        """Test CTD streaming session"""
        start_seq = [('connect', 'connected'),
                     ('reset', 'connected'),
                     ('sample', 'ready')]
        end_seq = [('pause', 'connected'),
                   ('disconnect', 'disconnected')]
        sock = start_client(self.pathname)
        self.ctd.serial.fileno.return_value = sock.fileno()
        self.ctd.start()
        self.assertEqual(self.ctd.name, 'ctd/1',
                         'Invalid sensor name: %r' % self.ctd.name)
        self.assertEqual(self.ctd.state, 'disconnected', 'Invalid state: %r' % self.ctd.state)
        for ev, state in start_seq:
            r = self.ctd.on_event(ev).get()
            self.assertEqual(self.ctd.state, state, 'Invalid state: %r' % self.ctd.state)
        dataq = self.ctd.on_event('run').get()
        for i in range(5):
            t, data = dataq.get()
        for ev, state in end_seq:
            r = self.ctd.on_event(ev).get()
            self.assertEqual(self.ctd.state, state, 'Invalid state: %r' % self.ctd.state)
        self.ctd.stop()
        self.assertTrue(self.ctd.ready())
        values = [Decimal(v.decode('ascii')) for v in SBE_VALUES.split(',')]
        for i, v in enumerate(values):
            self.assertEqual(data[i][1], v, 'Unexpected value for %s (%r)' % data[i])
        sock.close() 

class ParosTest(unittest.TestCase):
    def setUp(self):
        self.log = StringIO()
        self.sens = Paros('/dev/ttyUSB0')
        self.sens.serial = Mock()
        self.pathname = '/tmp/pr.sock'
        if os.path.exists(self.pathname):
            os.unlink(self.pathname)
        self.task = gevent.spawn(start_server, self.pathname, self.sens.eol+self.sens.prompt,
                                 paros_responses)
        
    def tearDown(self):
        self.task.join(timeout=2)
        if not self.task.ready():
            self.task.kill(timeout=2)
        os.unlink(self.pathname)
        
    def test_paros_session(self):
        """Test Paros session"""
        start_seq = [('reset', 'connected'),
                     ('invalid', 'connected'),
                     ('sample', 'ready')]
        end_seq = [('pause', 'connected'),
                   ('disconnect', 'disconnected')]
        sock = start_client(self.pathname)
        self.sens.serial.fileno.return_value = sock.fileno()
        self.sens.start()
        self.assertEqual(self.sens.state, 'disconnected', 'Invalid state: %r' % self.sens.state)
        self.sens.do_connect().get()
        for ev, state in start_seq:
            r = self.sens.on_event(ev).get()
            self.assertEqual(self.sens.state, state, 'Invalid state: %r' % self.sens.state)
        t, data = self.sens.on_event('run').get()
        for ev, state in end_seq:
            r = self.sens.on_event(ev).get()
            self.assertEqual(self.sens.state, state, 'Invalid state: %r' % self.sens.state)
        self.sens.stop()
        self.assertTrue(self.sens.ready())
        values = [Decimal(v.decode('ascii')) for v in PAROS_VALUES.split(',')]
        for i, v in enumerate(values):
            self.assertEqual(data[i][1], v, 'Unexpected value for %s (%r)' % data[i])
        sock.close()
        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(CtdStreamTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
    suite = unittest.TestLoader().loadTestsFromTestCase(CtdTest)
    unittest.TextTestRunner(verbosity=2).run(suite)    
    suite = unittest.TestLoader().loadTestsFromTestCase(ParosTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
    suite = unittest.TestSuite()
    for cls in (Paros, Sbe52, Sbe52Auto, Acs):
        suite.addTest(SerializationTest(cls))
    unittest.TextTestRunner(verbosity=2).run(suite)
    