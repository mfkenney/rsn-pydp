#!/usr/bin/env python
#
import unittest
from xml.etree import ElementTree as ET
from pydp.scanners import frame_scanner, delimiter_scanner, imm_scanner, coroutine

class FrameScannerTest(unittest.TestCase):
    last_rec = None
    stx = b'$'
    etx = b'\n'

    @coroutine
    def reader(self):
        while True:
            t, self.last_rec = (yield)

    def setUp(self):
        self.last_rec = None
        self.scanner = frame_scanner(self.reader(), self.stx, self.etx)

    def tearDown(self):
        self.scanner.close()

    def test_0_simple(self):
        """Basic record scanning test"""
        record = b'this is a data record'
        self.scanner.send(self.stx)
        for c in record:
            self.scanner.send(c)
        self.scanner.send(self.etx)
        self.assertEqual(record, self.last_rec,
                         'Unexpected result: %r' % self.last_rec)

    def test_1_unterminated(self):
        """Test scanning an unterminated record"""
        record = b'this is a data record'
        self.scanner.send(self.stx)
        for c in record:
            self.scanner.send(c)
        self.assertEqual(self.last_rec, None)

    def test_2_partial(self):
        """Test scanning a partial record"""
        record = b'this is a data record'
        for c in record:
            self.scanner.send(c)
        self.scanner.send(self.etx)
        self.assertEqual(self.last_rec, None)


class DelimScannerTest(unittest.TestCase):
    last_rec = None
    delim = b'S>'

    @coroutine
    def reader(self):
        while True:
            t, self.last_rec = (yield)

    def setUp(self):
        self.last_rec = None
        self.scanner = delimiter_scanner(self.reader(), self.delim)

    def tearDown(self):
        self.scanner.close()

    def test_0_basic(self):
        """Test basic record scanning."""
        record = b'pumpon\r\n'
        for c in record:
            self.scanner.send(c)
        for c in self.delim:
            self.scanner.send(c)
        self.assertEqual(record, self.last_rec,
                         'Unexpected result: %r' % self.last_rec)

    def test_1_multiline(self):
        """Test scanning a multi-line response."""
        record = 'ts\r\n 3.4567, 12.3456, 10.12, 20.000\r\n'
        for c in record:
            self.scanner.send(c)
        for c in self.delim:
            self.scanner.send(c)
        self.assertEqual(record, self.last_rec,
                         'Unexpected result: %r' % self.last_rec)

    def test_2_unterminated(self):
        """Test scanning an unterminated record."""
        record = 'ts\r\n 3.4567, 12.3456, 10.12, 20.000\r\n'
        for c in record:
            self.scanner.send(c)
        self.assertEqual(self.last_rec, None)

class ImmScannerTest(unittest.TestCase):
    last_rec = None
    delim = 'IMM>'

    @coroutine
    def reader(self):
        while True:
            t, self.last_rec = (yield)

    def setUp(self):
        self.last_rec = None
        self.scanner = imm_scanner(self.reader())

    def tearDown(self):
        self.scanner.close()

    def test_1_basic(self):
        """Test basic record scanning"""
        record = b'cmd\r\nresult\r\n<Executed/>\r\n'
        for c in record:
            self.scanner.send(c)
        for c in self.delim:
            self.scanner.send(c)
        self.assertEqual(self.last_rec.tag, 'output',
                         'Unexpected result: %r' % self.last_rec)
        self.assertEqual(self.last_rec.text,
                         'cmd\nresult\n',
                         'Unexpected result: %r' % self.last_rec.text)
        self.assertEqual(self.last_rec[0].tag, 'Executed',
                         'Unexpected result: %r' % self.last_rec[0])

    def test_2_noprompt(self):
        """Test a response that does not return a prompt."""
        record = b'pwroff\r\n<Executed/>\r\n<PowerOff/>'
        for c in record:
            self.scanner.send(c)
        self.assertEqual(self.last_rec.tag, 'output',
                         'Unexpected result: %r' % self.last_rec)
        self.assertEqual(self.last_rec[0].tag, 'Executed',
                         'Unexpected result: %r' % self.last_rec[0])
        self.assertEqual(self.last_rec[1].tag, 'PowerOff',
                         'Unexpected result: %r' % self.last_rec[1])


if __name__ == '__main__':
    for cls in (FrameScannerTest, DelimScannerTest, ImmScannerTest):
        suite = unittest.TestLoader().loadTestsFromTestCase(cls)
        unittest.TextTestRunner(verbosity=2).run(suite)