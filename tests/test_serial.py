#!/usr/bin/env python

import unittest
from mock import Mock
from tempfile import mkstemp
from cStringIO import StringIO
import os
from pydp import serialsens

def gen_simple_command(pathname, command, prompt):
    f = open(pathname, 'w')
    output = b'%s\r\nthis is some output\r\n' % command
    f.write(output + prompt)
    f.close()
    return output

def gen_data_stream(pathname, output, stx, etx):
    f = open(pathname, 'w')
    for rec in output:
        f.write(stx + rec + etx)
    f.close()
    
class SerialTest(unittest.TestCase):
    def setUp(self):
        self.log = StringIO()
        self.sensor = serialsens.SimpleSensor('/dev/ttyUSB0')
        self.sensor.serial = Mock()
        fd, self.pathname = mkstemp()
        os.close(fd)
        
    def tearDown(self):
        os.unlink(self.pathname)
        
    def test_open_close(self):
        """Test interface open and close"""
        self.sensor.open(sessionlog=self.log)
        self.sensor.close()
        result = self.log.getvalue()
        self.assertTrue(self.sensor.serial.open.called, 'serial.open() not called!')
        self.assertTrue(self.sensor.serial.nonblocking.called, 'serial.nonblocking() not called')
        self.assertTrue(self.sensor.serial.close.called, 'serial.close() not called')
        #self.assertEqual('<session>\n</session>\n', result,
        # 'Unexpected result: %r' % result)
        
    def test_simple_command(self):
        """Test parsing the output of a simple command"""
        out = gen_simple_command(self.pathname, 'foo', self.sensor.prompt)
        f = open(self.pathname, 'r')
        self.sensor.serial.fileno.return_value = f.fileno()
        self.sensor.open(sessionlog=self.log)
        is_done = self.sensor.on_input()
        self.sensor.close()
        self.assertTrue(is_done, 'parsing error!')
        results = list(self.sensor.results())
        self.assertEqual(results[0][1], out, 'Unexpected result: %r' % results[0][1])
        results = list(self.sensor.results())
        self.assertEqual(results, [], 'Unexpected result: %r' % results)

STX = b'$'
ETX = b'\n'
class StreamTest(unittest.TestCase):
    def setUp(self):
        self.log = StringIO()
        parser = serialsens.FramedRecordParser(STX, ETX)
        self.sensor = serialsens.SimpleSensor('/dev/ttyUSB0', parser=parser)
        self.sensor.serial = Mock()
        fd, self.pathname = mkstemp()
        os.close(fd)
        
    def tearDown(self):
        os.unlink(self.pathname)
        
    def test_open_close(self):
        """Test interface open and close"""
        self.sensor.open(sessionlog=self.log)
        self.sensor.close()
        result = self.log.getvalue()
        self.assertTrue(self.sensor.serial.open.called, 'serial.open() not called!')
        self.assertTrue(self.sensor.serial.nonblocking.called, 'serial.nonblocking() not called')
        self.assertTrue(self.sensor.serial.close.called, 'serial.close() not called')
        # self.assertEqual('<session>\n</session>\n', result,
        # 'Unexpected result: %r' % result)
        
    def test_stream(self):
        """Test data stream parsing"""
        records = [b'this is record 1',
                   b'this is record two',
                   b'this is record 3']
        gen_data_stream(self.pathname, records, STX, ETX)
        f = open(self.pathname, 'r')
        self.sensor.serial.fileno.return_value = f.fileno()
        self.sensor.open()
        is_done = self.sensor.on_input()
        self.sensor.close()
        self.assertTrue(is_done, 'parsing error!')
        for i, data in enumerate(self.sensor.results()):
            _, rec = data
            self.assertEqual(rec, records[i], 'Invalid record content: %r' % rec)
        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SerialTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
    suite = unittest.TestLoader().loadTestsFromTestCase(StreamTest)
    unittest.TextTestRunner(verbosity=2).run(suite)