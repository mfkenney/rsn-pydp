#!/usr/bin/env python

import unittest
from mock import Mock
import sys
import os
import random
import string
sys.path.insert(0, os.path.join('..', 'pydp'))
from pydp.mmp import MMPProtocol, update_crc16

LISTING = [
    'E0000000.DAT 1024',
    'E0000001.DAT 4567',
    'C0000000.DAT 3734',
    'C0000001.DAT 53',
    'A0000000.DAT 3455',
    'A0000001.DAT 464',
    'SCHEDULE.DPL 4052',
    'LASTSENT.DAT 4',
    'MMP-3_20.RUN 1273'
]

def gen_mmpdir(listing=None):
    if listing:
        count = len(listing)
        text = '\r'.join(listing) + '\r'
        crc = reduce(update_crc16, [ord(c) for c in text], 0)
        return 'ACKDIR\r\nMMPDIR %d\r\n%sMMPEOD %d\r\n' % (count, text, crc)
    else:
        return 'ACKDIR\r\nMMPDIR 0\r\nMMPEOD 0\r\n'

def gen_file_contents(n):
    b = [chr(i) for i in range(255)]
    contents = []
    for i in range(n):
        contents.append(random.choice(b))
    return ''.join(contents)
    
def gen_mmpfil(contents=''):
    if contents:
        n = len(contents)
        crc = reduce(update_crc16, [ord(c) for c in contents], 0)
        return 'ACKFIL\r\nMMPFIL foo.bin %d\r\n%sMMPEOF %d\r\n' % (n, contents, crc)
    else:
        return 'ACKFIL\r\nMMPFIL $$$$$$$$.$$$ 0\r\nMMPEOF 0\r\n'
    
def gen_mmpfil_errors(contents):
    n = len(contents)
    crc = reduce(update_crc16, [ord(c) for c in contents], 0)
    for c in string.printable:
        contents = contents.replace(c, '\x01')
    return 'ACKFIL\r\nMMPFIL foo.bin %d\r\n%sMMPEOF %d\r\n' % (n, contents, crc)
    
class MMPProtocolTest(unittest.TestCase):
    def setUp(self):
        self.listing = LISTING
        self.contents = gen_file_contents(300)
        self.p = MMPProtocol({'mmpdir': self.getlisting,
                              'mmpfil': self.getfile})
        
    def getlisting(self, ev, result):
        self.assertEqual(ev, 'MMPDIR')
        self.assertEqual(result, self.listing, 'Unexpect result: %r' % result)
        
    def getfile(self, ev, name, contents):
        self.assertEqual(ev, 'MMPFIL')
        self.assertEqual(contents, self.contents, 'Unexpected result: %r' % contents)
    
    def test_mmpobs(self):
        """Test MMPOBS command"""
        msg = b'MMPOBS 1234.56 12.02 2.15\r\n'
        end = False
        for c in msg:
            output = self.p.feed(c)
            if output is not None:
                end = True
                self.assertEqual(output, b'ACKOBS\r\n', 'Unexpected result: %r' % output)
        self.assertTrue(end)
       
    def test_mmpdck(self):
        """Test MMPDCK command"""
        msg = b'MMPDCK 1234.56 12.02\r\n'
        end = False
        for c in msg:
            output = self.p.feed(c)
            if output is not None:
                end = True
                self.assertEqual(output, b'ACKDCK\r\n', 'Unexpected result: %r' % output)
        self.assertTrue(end)
        
    def test_mmpdir(self):
        """Test MMPDIR parsing"""
        for c in gen_mmpdir(self.listing):
            output = self.p.feed(c)
            if output is not None:
                self.assertEqual(output, b'ACKEOD\r\n', 'Unexpected result: %r' % output)
                
    def test_mmpdir_empty(self):
        """Test empty MMPDIR parsing"""
        self.p.events['mmpdir'] = Mock()
        for c in gen_mmpdir():
            output = self.p.feed(c)
            if output is not None:
                self.assertEqual(output, b'ACKEOD\r\n', 'Unexpected result: %r' % output)
        self.assertFalse(self.p.events['mmpdir'].called)
        
    def test_mmpfil(self):
        """Test MMPFIL parsing"""
        eof = False
        for c in gen_mmpfil(self.contents):
            output = self.p.feed(c)
            if output is not None:
                eof = True
                self.assertEqual(output, b'ACKEOF\r\n', 'Unexpected result: %r' % output)
        self.assertTrue(eof, 'EOF not detected')
        
    def test_mmpfil_error(self):
        """Test detecting file transmission errors"""
        eof = False
        self.p.events['mmpfil'] = Mock()
        for c in gen_mmpfil_errors(self.contents):
            output = self.p.feed(c)
            if output is not None:
                eof = True
                self.assertEqual(output, b'NAKEOF\r\n', 'Unexpected result: %r' % output)
        self.assertTrue(eof, 'EOF not detected')
        self.assertTrue(self.p.events['mmpfil'].called)
        
    def test_mmpfil_empty(self):
        """Test empty MMPFIL parsing"""
        self.p.events['mmpfil'] = Mock()
        for c in gen_mmpfil():
            output = self.p.feed(c)
            if output is not None:
                self.assertEqual(output, b'ACKEOF\r\n', 'Unexpected result: %r' % output)
        self.assertFalse(self.p.events['mmpfil'].called)
                
                
if __name__ == '__main__':
    for cls in (MMPProtocolTest,):
        suite = unittest.TestLoader().loadTestsFromTestCase(cls)
        unittest.TextTestRunner(verbosity=2).run(suite)