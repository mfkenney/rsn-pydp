#!/usr/bin/env python
#
# Support functions for testing
#
import os
import gevent
from gevent import monkey
monkey.patch_socket()
import _socket
from gevent.socket import socket
from gevent.event import Event
from gevent.server import StreamServer
from functools import partial

ready = Event()

def sensor_server(ev, prompt, responses, socket, addr):
    f = socket.makefile()
    #f.readline()
    try:
        while True:
            f.write(prompt)
            f.flush()
            cmd = f.readline()
            if not cmd:
                break
            cmd = cmd.strip()
            if cmd.startswith('!wait'):
                _, secs = cmd.split()
                gevent.sleep(float(secs))
            else:
                resp = responses.get(cmd, prompt)
                f.write(resp)
                f.flush()
    except (gevent.GreenletExit, _socket.error):
        pass
    ev.set()
        
def start_server(pathname, prompt, responses):
    s = _socket.socket(_socket.AF_UNIX, _socket.SOCK_STREAM)
    s.setblocking(0)
    s.bind(pathname)
    s.listen(2)
    ev = Event()
    ready.set()
    svc = StreamServer(s, partial(sensor_server, ev, prompt, responses))
    svc.start()
    ev.wait()
    svc.stop()
    
def start_server_with_handler(pathname, handler):
    s = _socket.socket(_socket.AF_UNIX, _socket.SOCK_STREAM)
    s.setblocking(0)
    s.bind(pathname)
    s.listen(2)
    ev = Event()
    ready.set()
    svc = StreamServer(s, partial(handler, ev))
    svc.start()
    ev.wait()
    svc.stop()
    
def start_client(pathname):
    s = _socket.socket(_socket.AF_UNIX, _socket.SOCK_STREAM)
    s.setblocking(0)
    ready.wait()
    s.connect(pathname)
    ready.clear()
    return s

