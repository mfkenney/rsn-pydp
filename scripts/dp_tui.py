#!/usr/bin/env python
#
# Text-mode user interface for Deep Profiler messages
#
import sys
import zmq
import simplejson as json
import time
import npyscreen
import argparse
import curses
from decimal import Decimal


class DataField(object):
    """
    Encapsulate all of the information need to display a
    data record variable.
    """
    def __init__(self, displayname='', units='',
                 scale=1, precision='1',
                 tostr=None, order=0):
        self.displayname = displayname
        self.units = units
        self.scale = Decimal(str(scale))
        self.precision = Decimal(precision)
        self.tostr = tostr
        self.order = order


class ViewerMetaClass(type):
    def __init__(cls, name, bases, kwds):
        super(ViewerMetaClass, cls).__init__(name, bases, kwds)
        data_fields = []
        dfs = [(v.order, k, v) for k, v in kwds.items()
               if isinstance(v, DataField)]
        dfs.sort()
        for _, key, obj in dfs:
            obj.name = key
            data_fields.append(obj)
        setattr(cls, '_fields', data_fields)


class MessageViewer(npyscreen.FormBaseNew):
    """
    Base class to display the contents of a Deep Profiler
    message on a formatted screen.
    """
    __metaclass__ = ViewerMetaClass

    def create(self):
        self.timestamp = self.add(npyscreen.TitleFixedText,
                                  name='Time:',
                                  value='No data received yet...')
        self.nextrely += 1
        self.create_data_display()
        self.cmdhelp = self.add(npyscreen.FixedText,
                                name='HELP',
                                value='n=next p=previous q=quit',
                                color='STANDOUT',
                                rely=self.lines-3)

    def set_value(self, value):
        t = value['t'][0] + value['t'][1] * 1.0e-6
        self.timestamp.value = time.strftime('%Y-%m-%d %H:%M:%S',
                                             time.gmtime(t))
        if 'data' in value:
            self.update_data(value['data'])
        else:
            self.update_data(value)

    def create_data_display(self):
        for df in self._fields:
            widget = self.add(npyscreen.TitleFixedText,
                              name=df.displayname+':')
            setattr(self, df.name, widget)

    def update_data(self, data):
        dfs = [f for f in self._fields if f.name in data]
        for df in dfs:
            widget = getattr(self, df.name)
            if df.tostr:
                disp = df.tostr(data[df.name])
            else:
                vals = data[df.name]
                if not isinstance(vals, list):
                    vals = [vals]
                parts = []
                for val in vals:
                    x = Decimal(str(val))
                    x *= df.scale
                    parts.append(str(x.quantize(df.precision)))
                disp = '/'.join(parts) + ' ' + df.units
            widget.value = disp


class DockViewer(MessageViewer):
    """
    Dock status messages.
    """
    T_ambient = DataField('T_ambient', 'degC', 0.001, '0.1')
    T_heatsink = DataField('T_heatsink', 'degC', 0.001, '0.1')
    humidity = DataField('Humidity', '%', 1)
    ips_status = DataField('IPS Status', tostr=hex)
    v12 = DataField('12v Current', 'A', 0.001, '0.001')


class ProfilerViewer(MessageViewer):
    """
    Profiler status messages.
    """
    profile = DataField('Profile#', tostr=str)
    state = DataField('State', tostr=str, order=1)
    pressure = DataField('Pressure', 'dbar', 0.001, '0.1', order=2)
    itemp = DataField('Internal temp', 'degC', 0.001, '0.1', order=3)
    humidity = DataField('Internal humidity', '%', 1, order=4)
    voltage = DataField('Battery voltage', 'V', 0.001, '0.001', order=5)
    current = DataField('Battery current', 'A', 0.001, '0.001', order=6)
    rel_charge = DataField('Battery capacity', '%', 1, order=7)
    motor_current = DataField('Motor Current', 'A', 0.001, '0.001', order=8)


class EventViewer(MessageViewer):
    """
    Event messages.
    """
    def create_data_display(self):
        self.event = self.add(npyscreen.TitleFixedText,
                              name='Event:')
        self.desc = self.add(npyscreen.TitleFixedText,
                             name='Description:')
        self.attr = self.add(npyscreen.TitleMultiLine,
                             name='Attributes:',
                             editable=False)

    def update_data(self, data):
        self.event.value = str(data['name'])
        self.desc.value = str(data['desc'])
        values = []
        if 'attrs' in data:
            for k, v in data['attrs'].iteritems():
                values.append('{0}: {1}'.format(str(k), str(v)))
        self.attr.values = values


class CtdViewer(MessageViewer):
    """
    CTD data display.
    """
    condwat = DataField('Conductivity', 'mS/cm', precision='0.0001')
    tempwat = DataField('Temperature', 'degC', precision='0.0001')
    preswat = DataField('Pressure', 'dbar', precision='0.01')


class OptodeViewer(MessageViewer):
    """
    Optode data display.
    """
    doconcs = DataField('Calibrated phase', 'deg', precision='0.001')
    t = DataField('Temperature', 'degC', precision='0.001')


class FlntuViewer(MessageViewer):
    """
    FLNTU data display.
    """
    chlaflo = DataField('Chlorophyl-a conc.', 'counts')
    ntuflo = DataField('Turbidity', 'counts')


class FlcdViewer(MessageViewer):
    """
    FLCD data display.
    """
    cdomflo = DataField('Colored Dissolved Organic Matter', 'counts')


class AcmViewer(MessageViewer):
    """
    ACM data display.
    """
    va = DataField('Path velocity A', 'cm/s', precision='0.01')
    vb = DataField('Path velocity B', 'cm/s', precision='0.01')
    vc = DataField('Path velocity C', 'cm/s', precision='0.01')
    vd = DataField('Path velocity D', 'cm/s', precision='0.01')
    hx = DataField('Mag flux X', precision='0.0001', order=1)
    hy = DataField('Mag flux Y', precision='0.0001', order=1)
    hz = DataField('Mag flux Z', precision='0.0001', order=1)
    tx = DataField('Tilt X', 'deg', precision='0.01', order=2)
    ty = DataField('Tilt Y', 'deg', precision='0.01', order=2)


class BatteryViewer(MessageViewer):
    """
    Battery data display
    """
    count = DataField('# of batteries', '')
    voltage = DataField('Voltage', 'volts', precision='0.001',
                        scale=0.001)
    current = DataField('Current', 'amps', precision='0.001',
                        scale=0.001)
    rel_charge = DataField('Relative charge', '%', 1, order=1)
    energy = DataField('Battery capacity', 'Wh', 0.001, '0.001', order=2)


# List of screen descriptions:
# Name, class, keyword args
SCREENS = [
    ('dock', DockViewer, {'name': 'DSC Status'}),
    ('profiler', ProfilerViewer, {'name': 'DPC Status'}),
    ('events', EventViewer, {'name': 'Events'}),
    ('ctd_1', CtdViewer, {'name': 'Seabird 52-MP CTD'}),
    ('optode_1', OptodeViewer, {'name': 'Aanderaa Optode'}),
    ('acm_1', AcmViewer, {'name': 'FSI Acoustic Current Meter'}),
    ('flntu_1', FlntuViewer, {'name': 'Wetlabs FLNTU'}),
    ('flcd_1', FlcdViewer, {'name': 'Wetlabs FLCD'}),
    ('battery', BatteryViewer, {'name': 'Battery Status'})
]


def monitor(stdscr, *socks):
    """
    Mainloop to listen for data records on a list of ZeroMQ sockets,
    along with key-presses on standard-input and update the display.
    """
    # Create a form for each message type
    forms = []
    form_table = {}
    i = 0
    for name, klass, kwds in SCREENS:
        F = klass(minimum_lines=20, **kwds)
        form_table[name] = (i, F)
        forms.append(F)
        i += 1
    form_index = 0
    forms[0].display()

    # Initialize the poller
    poller = zmq.Poller()
    fd = sys.stdin.fileno()
    for sock in socks:
        poller.register(sock, zmq.POLLIN)
    poller.register(fd, zmq.POLLIN)

    quit_cmds = (ord('Q'), ord('q'))
    next_cmds = (ord('N'), ord('n'), curses.KEY_RIGHT)
    prev_cmds = (ord('P'), ord('p'), curses.KEY_LEFT)

    stdscr.nodelay(1)
    while True:
        ready = dict(poller.poll())
        for sock in ready:
            if sock == fd:
                c = stdscr.getch()
                if c in quit_cmds:
                    stdscr.nodelay(0)
                    return
                elif c in next_cmds:
                    forms[form_index].erase()
                    form_index = (form_index + 1) % len(forms)
                    forms[form_index].display()
                elif c in prev_cmds:
                    forms[form_index].erase()
                    form_index = (form_index - 1) % len(forms)
                    forms[form_index].display()
            else:
                mtype, contents = sock.recv_multipart()
                msg = json.loads(contents)
                entry = None
                if mtype == 'DATA':
                    entry = form_table.get(msg['name'])
                elif mtype == 'EVENT':
                    entry = form_table.get('events')
                if entry:
                    idx, F = entry
                    F.set_value(msg)
                    if idx == form_index:
                        F.display()
    stdscr.nodelay(0)


def main():
    """
    Subscribe to Deep Profiler data streams.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('publishers', nargs='+')
    args = parser.parse_args()

    ctx = zmq.Context()
    socks = []
    for endpoint in args.publishers:
        sock = ctx.socket(zmq.SUB)
        sock.connect(endpoint)
        sock.setsockopt(zmq.SUBSCRIBE, b'')
        socks.append(sock)
    curses.wrapper(monitor, *socks)


if __name__ == '__main__':
    main()
