#!/usr/bin/env python
#
# Curses based DPC battery monitor.
#
import sys
import curses
import time
import smbus
from decimal import Decimal
from datetime import datetime
from collections import namedtuple
from select import select
from pydp.battery import Battery, median_value, Mux

Field = namedtuple('Field', 'name convert units')

# (mux_address, channel)
BATTERIES = [
    (0x70, 0),
    (0x70, 1),
    (0x70, 2),
    (0x70, 3),
    (0x70, 4),
    (0x70, 5),
    (0x77, 0),
    (0x77, 1),
    (0x77, 2),
    (0x77, 3)
]


# Data display conversion functions
def cvt_voltage(mv):
    return Decimal(mv) / Decimal(1000)


def cvt_current(ma):
    # The two-byte value returned for the current readings must be
    # interpreted as a signed integer.
    if ma & 0x8000:
        ma -= 65536
    return Decimal(ma) / Decimal(1000)


def cvt_charge(p):
    return '{0}%'.format(p)


def cvt_capacity(tmwh):
    return Decimal(tmwh) / Decimal(100)


def cvt_time(t):
    return datetime.utcfromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')


def cvt_temp(k):
    return Decimal(k) / Decimal(10) - Decimal('273.15')

DISPLAY = [
    Field('index', None, None),
    Field('address', hex, None),
    Field('s/n', None, None),
    Field('time', cvt_time, None),
    Field('voltage', cvt_voltage, 'volts'),
    Field('current', cvt_current, 'amps'),
    Field('capacity', cvt_capacity, 'Wh'),
    Field('charge', cvt_charge, None),
    Field('cycles', None, None),
    Field('temperature', cvt_temp, 'degC')
]


def collect(b):
    """
    Collect the data for a Battery.

    :param b: Battery instance
    :return: dictionary of data values
    """
    if not b:
        d = {'time': time.time()}
        for key in ('s/n', 'address', 'voltage', 'current',
                    'capacity', 'charge', 'chg. current', 'pack status',
                    'temperature', 'cycles'):
            d[key] = None
    else:
        if (time.time() - b.t_init) > 45:
            b._initialize()
        d = {'time': time.time(),
             's/n': median_value(b.SerialNumber),
             'address': b.address,
             'voltage': median_value(b.Voltage, n=5),
             'current': median_value(b.Current, n=5),
             'capacity': median_value(b.RemainingCapacity, n=5),
             'charge': median_value(b.RelativeStateOfCharge, n=5),
             'chg. current': median_value(b.ChargingCurrent, n=5),
             'pack status': median_value(b.PackStatus, n=5),
             'cycles': median_value(b.CycleCount, n=5),
             'temperature': median_value(b.Temperature, n=5)}
    return d


class FieldDisplay(object):
    """
    Class to manage a Field on the curses display.
    """
    def __init__(self, row, col, width, field):
        self.win = curses.newwin(1, width, row, col)
        self.field = field
        self.width = width

    def update(self, value=None):
        self.win.addstr(0, 0, self.field.name, curses.A_BOLD)
        self.win.clrtoeol()
        if value is not None:
            if self.field.convert:
                try:
                    value = self.field.convert(value)
                except Exception:
                    value = 'n/a'
            display = '%s %6s' % (value, self.field.units or '')
        else:
            display = 'n/a'
        self.win.addstr(0, self.width - len(display) - 1, display,
                        curses.A_NORMAL)
        self.win.noutrefresh()


def run(stdscr, dspdesc, bats, title='DPC Battery Monitor', interval=5):
    """
    Run the curses display

    :param stdscr: standard screen
    :param dspdesc: list of Fields to display
    :param bats: list of Battery objects to monitor
    :param title: title string
    :param interval: sampling interval
    """
    stdscr.clear()
    max_row, max_col = stdscr.getmaxyx()
    stdscr.addstr(1, (max_col - len(title)) / 2, title)
    text = 'HELP:  q=quit n=next p=prev'
    stdscr.addstr(max_row - 1, 0, text, curses.A_DIM)

    row = 4
    stdscr.addstr(row, 0, 'Please stand by ...', curses.A_DIM)
    stdscr.noutrefresh()
    curses.doupdate()

    idx = 0
    data = {'index': idx}
    data.update(collect(bats[idx]))
    stdscr.move(row, 0)
    stdscr.clrtoeol()
    stdscr.noutrefresh()

    widgets = {}
    for field in dspdesc:
        w = FieldDisplay(row, 8, 60, field)
        w.update(data[field.name])
        widgets[field.name] = w
        row += 1
    stdscr.noutrefresh()
    curses.doupdate()
    curses.curs_set(0)

    # Single character commands from user
    quit_cmds = (ord('Q'), ord('q'))
    next_cmds = (ord('N'), ord('n'), curses.KEY_RIGHT)
    prev_cmds = (ord('P'), ord('p'), curses.KEY_LEFT)

    stdscr.nodelay(1)
    while True:
        rlist, _, _ = select([sys.stdin], [], [], interval)
        if rlist:
            c = stdscr.getch()
            if c in quit_cmds:
                break
            elif c in next_cmds:
                idx = (idx + 1) % len(bats)
            elif c in prev_cmds:
                idx = (idx - 1) % len(bats)
            data['index'] = idx

        try:
            data.update(collect(bats[idx]))
            for k, v in data.items():
                if k in widgets:
                    widgets[k].update(v)
            curses.doupdate()
        except (AssertionError, IOError):
            pass
    stdscr.nodelay(0)


def main():
    bus = smbus.SMBus(0)
    # Bit of a hack here. Create an object for each battery mux in the
    # system to ensure that only one mux is active at any given time.
    # See Mux.__new__ for details.
    Mux(bus, 0x70)
    Mux(bus, 0x77)
    bats = []
    for addr, channel in BATTERIES:
        b = Battery(bus, addr, channel)
        tries = 4
        while tries > 0:
            try:
                b._initialize()
                bats.append(b)
                break
            except IOError:
                tries -= 1
        else:
            sys.stderr.write(
                'Cannot read battery at ({0:x}, {1})\n'.format(
                    addr, channel))
    if bats:
        curses.wrapper(run, DISPLAY, bats)

if __name__ == '__main__':
    main()
