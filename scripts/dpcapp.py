#!/usr/bin/env python
"""dpcapp.py

Simple command interpreter to control the DPC via the IMM connection.
"""
import cmd
import sys
import logging
import time
import gevent
from gevent import monkey
monkey.patch_all()
from base64 import b64decode, b64encode
import redis
from pydp.serialcom import Port
from pydp.imm2 import IMM
from pydp.dio import Ts4800Dio
from pydp.power import PowerSwitch
from pydp.messaging import putmsg, line_captured
from pydp.messages import DbMessage, ProfileMessage, BatteryMessage, \
    SensorMessage, StatusMessage, pb_to_dict
from ast import literal_eval


def find_peer(imm):
    """
    Return the serial number of our peer IMM.
    """
    with line_captured(imm, delay=2, force=True):
        _, response = imm.command('disc', 5)
        e = response.find('.//Discovered')
        return e.get('SN') if e is not None else None


def args_to_dict(args, do_eval=True):
    """
    Convert a command argument list of the form NAME=VALUE
    into a dictionary.

    >>> d = args_to_dict('foo=bar baz=1,2,3 x= y').items()
    >>> d.sort()
    >>> d
    [('baz', [1, 2, 3]), ('foo', 'bar'), ('x', ''), ('y', None)]
    """
    def maybe_eval(s):
        if do_eval:
            try:
                return literal_eval(s)
            except (ValueError, SyntaxError):
                return s
        else:
            return s
    params = {}
    for arg in [a.split('=') for a in args.split()]:
        try:
            name, val = arg
        except ValueError:
            name = arg[0]
            val = None
        if (val is not None) and (',' in val):
            params[name] = [maybe_eval(v) for v in val.split(',')]
        else:
            params[name] = maybe_eval(val)
    return params


class DpcApp(cmd.Cmd):
    """
    Commands (type help <command>)
    ====================================
    go set get dock ping prf sendq
    """
    prompt = 'DPC> '
    imm = None
    peer_id = None
    rd = None

    def precmd(self, line):
        assert self.imm is not None
        assert self.peer_id is not None
        return line

    def postloop(self):
        self.imm.sleep()

    def poutput(self, text):
        sys.stdout.write(text)
    perror = poutput

    def emptyline(self):
        pass

    def do_quit(self, args):
        return True
    do_exit = do_quit
    do_q = do_quit

    def do_go(self, arg):
        """go: send a GO command to start MPC. Exits from command mode"""
        reply = putmsg(self.imm, self.peer_id, 'go', timeout=5,
                       force=True,
                       async=False)
        if reply is not None:
            try:
                self.poutput(reply.text + '\n')
            except AttributeError:
                self.perror('Unexpected response: {0}\n'.format(repr(reply)))

    def do_ping(self, arg):
        """ping: test DPC command response"""
        reply = putmsg(self.imm, self.peer_id, 'ping', timeout=5,
                       force=True,
                       async=False)
        if reply is not None:
            try:
                self.poutput(reply.text + '\n')
            except AttributeError:
                self.perror('Unexpected response: {0}\n'.format(repr(reply)))
        else:
            self.perror('Message not sent\n')

    def do_dock(self, arg):
        """dock: send the profiler to the Dock."""
        reply = putmsg(self.imm, self.peer_id, 'dock', timeout=5,
                       force=True,
                       async=False)
        if reply is not None:
            try:
                self.poutput(reply.text + '\n')
            except AttributeError:
                self.perror('Unexpected response: {0}\n'.format(repr(reply)))
        else:
            self.perror('Message not sent\n')

    def do_undock(self, arg):
        """undock: leave the Dock."""
        reply = putmsg(self.imm, self.peer_id, 'continue', timeout=5,
                       force=True,
                       async=False)
        if reply is not None:
            try:
                self.poutput(reply.text + '\n')
            except AttributeError:
                self.perror('Unexpected response: {0}\n'.format(repr(reply)))
        else:
            self.perror('Message not sent\n')

    def do_get(self, arg):
        """get KEY: get the database value for KEY."""
        if arg:
            reply = putmsg(self.imm, self.peer_id,
                           'get {0}'.format(arg), timeout=5,
                           force=True,
                           async=False)
            if reply is not None:
                try:
                    prefix, data = reply.text.strip().split(':', 1)
                except AttributeError:
                    self.perror('Bad response: {0}\n'.format(repr(reply)))
                else:
                    try:
                        result = DbMessage.deserialize(b64decode(str(data)))
                        for val in result.value:
                            self.poutput(val + '\n')
                    except Exception as e:
                        self.perror('Error: {0}'.format(repr(e)))
            else:
                self.perror('Message not sent\n')
        else:
            self.perror('You must specify a database key\n')

    def do_sens(self, arg):
        """sens NAME [NAME...]: return the latest sensor data record."""
        reply = putmsg(self.imm, self.peer_id,
                       'sens {0}'.format(arg), timeout=5,
                       force=True,
                       async=False)
        if reply is not None:
            try:
                prefix, data = reply.text.strip().split(':', 1)
            except AttributeError:
                self.perror('Unexpected response: {0}\n'.format(repr(reply)))
            else:
                try:
                    msg = SensorMessage.deserialize(b64decode(str(data)))
                    for name, rec in pb_to_dict(msg).iteritems():
                        self.poutput(name + '\n')
                        self.poutput(repr(rec) + '\n')
                except Exception as e:
                        self.perror('Error: {0}'.format(repr(e)))
        else:
            self.perror('Message not sent\n')

    def do_mpc(self, arg):
        """mpc: return the latest MPC status."""
        reply = putmsg(self.imm, self.peer_id,
                       'mpc', timeout=5,
                       force=True,
                       async=False)
        if reply is not None:
            try:
                prefix, data = reply.text.strip().split(':', 1)
            except AttributeError:
                self.perror('Unexpected response: {0}\n'.format(repr(reply)))
            else:
                try:
                    msg = StatusMessage.deserialize(b64decode(str(data)))
                    rec = pb_to_dict(msg)
                    self.poutput('mpc\n')
                    self.poutput(repr(rec) + '\n')
                except Exception as e:
                        self.perror('Error: {0}'.format(repr(e)))
        else:
            self.perror('Message not sent\n')

    def do_batt(self, arg):
        """batt: return the most recent battery status."""
        reply = putmsg(self.imm, self.peer_id,
                       'batt', timeout=5,
                       force=True,
                       async=False)
        if reply is not None:
            try:
                prefix, data = reply.text.strip().split(':', 1)
            except (AttributeError, ValueError):
                self.perror('Unexpected response: {0}\n'.format(repr(reply)))
            else:
                try:
                    b = BatteryMessage.deserialize(b64decode(str(data)))
                    ts = time.strftime('%Y-%m-%d %H:%M:%S',
                                       time.gmtime(b.time/1000000))
                    self.poutput(ts + '\n')
                    self.poutput("V: {0:.3f} volts\n".format(b.voltage/1000.))
                    self.poutput("I: {0:.3f} amps\n".format(b.current/1000.))
                    self.poutput("T: {0:.1f} degC\n".format(b.temp/10.))
                    self.poutput("C: {0:d}%\n".format(b.rel_charge))
                except Exception as e:
                    self.perror('Error: {0}'.format(repr(e)))
        else:
            self.perror('Message not sent\n')

    def do_set(self, arg):
        """set KEY [VALUE ...]: set the value of a database KEY."""
        if arg:
            args = arg.split()
            msg = DbMessage(key=args[0], value=args[1:])
            reply = putmsg(self.imm,
                           self.peer_id,
                           'set {0}'.format(b64encode(msg.serialize())),
                           timeout=5,
                           async=False,
                           force=True)
            if reply is not None:
                try:
                    self.poutput(reply.text + '\n')
                except AttributeError:
                    self.perror('Unexpected response: {0}\n'.format(repr(reply)))
            else:
                self.perror('Message not sent\n')
        else:
            self.perror('Missing arguments')

    def do_sendq(self, arg):
        """sendq: send all messages from the queue to the DPC. """
        cmd = self.rd.lpop('dpc:commands')
        while cmd is not None:
            if cmd.strip() == 'go':
                self.poutput('Skipping "go"\n')
                cmd = self.rd.lpop('dpc:commands')
                continue
            self.poutput('DPC command = {0!r}\n'.format(cmd))
            reply = putmsg(self.imm,
                           self.peer_id,
                           cmd,
                           timeout=5,
                           async=False,
                           force=True)
            if reply is not None:
                try:
                    self.poutput(reply.text + '\n')
                except AttributeError:
                    self.perror('Error response: {0}\n'.format(repr(reply)))
            else:
                self.perror('Message not sent\n')
            cmd = self.rd.lpop('dpc:commands')

    def do_prf(self, arg):
        """prf key=value key=value...: define a new profile"""
        if not arg:
            self.perror('Missing argument\n')
            return False
        d = args_to_dict(arg)
        d['dir'] = d['dir'].upper()
        if 'queueing' in d:
            d['queueing'] = d['queueing'].upper()
        if 'exclude' not in d:
            d['exclude'] = []
        elif d['exclude'][-1] == '':
            # Bit of a hack here to remove the trailing empty string
            # in a list specified on the command line:
            #
            #  exclude=acm_1, --> exclude = ['acm_1', '']
            d['exclude'] = d['exclude'][:-1]
        msg = ProfileMessage(**d)
        reply = putmsg(self.imm,
                       self.peer_id,
                       'prf {0}'.format(b64encode(msg.serialize())),
                       timeout=5,
                       async=False,
                       force=True)
        if reply is not None:
            try:
                self.poutput(reply.text + '\n')
            except AttributeError:
                self.perror('Unexpected response: {0}\n'.format(repr(reply)))
        else:
            self.perror('Message not sent\n')

    def do_pat(self, arg):
        """pat PATTERN_NAME: add a pattern to the profile queue"""
        if not arg:
            self.perror('Missing argument\n')
            return False
        msg = 'pattern {0}'.format(arg.lower())
        reply = putmsg(self.imm, self.peer_id, msg, timeout=5,
                       force=True,
                       async=False)
        if reply is not None:
            try:
                self.poutput(reply.text + '\n')
            except AttributeError:
                self.perror('Unexpected response: {0}\n'.format(repr(reply)))
        else:
            self.perror('Message not sent\n')


def main():
    logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO,
                        datefmt='[%Y-%m-%d %H:%M:%S]')
    rval = 0
    rd = redis.StrictRedis()
    cfg = rd.hgetall('imm_cfg')
    settings = rd.hgetall('imm_settings')
    dioport = Ts4800Dio()
    sw = PowerSwitch(cfg['switch'], dioport)
    port = Port(cfg['device'], baud=int(cfg['baud']))
    sw.on()
    gevent.sleep(float(cfg.get('warmup', 0)))
    port.open()
    try:
        with IMM(port) as imm:
            imm.wakeup()
            logging.info('Configuring IMM')
            for k, v in settings.items():
                imm.set(k, int(v))
            peer_id = cfg.get('peer_sn')
            if not peer_id:
                logging.info('Checking for DPC IMM')
                peer_id = find_peer(imm)
                if not peer_id:
                    peer_id = raw_input('Enter DPC IMM s/n => ')
            logging.info('DPC IMM s/n = %s', peer_id)
            app = DpcApp()
            app.imm = imm
            app.peer_id = peer_id
            app.rd = rd
            args = sys.argv[1:]
            if args:
                for arg in args:
                    app.onecmd(arg)
            else:
                app.cmdloop(DpcApp.__doc__)
    except KeyboardInterrupt:
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Unhandled exception')
        rval = 1
    finally:
        sw.off()
        return rval


if __name__ == '__main__':
    sys.exit(main())
