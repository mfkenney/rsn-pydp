#!/usr/bin/env python
#
# Implement the Deep Profiler network API
#
import sys
import urllib
from gevent_zeromq import zmq
from gevent.queue import Queue, Empty
from gevent import spawn
from gevent import monkey
monkey.patch_all()
import redis
import uuid
import logging
import os
import time
from pydp.messages import EventMessage, ProfileMessage
import simplejson as json
from base64 import b64encode


EVENT_DESC = {
    'profile:start': 'Profiler starting a new profile',
    'profile:end': 'Profiler ending profile',
    'docked': 'Profiler in dock',
    'stalled': 'Profiler stalled',
    'reply': 'Command reply received',
    'wait': 'Ready for messages'
}


def reformat_events(inmsg, rd):
    msg = EventMessage.deserialize(inmsg)
    if 't' in msg.attrs:
        t = int(msg.attrs['t'])
        del msg.attrs['t']
    else:
        t = int(time.time())
    d = {'name': msg.ev,
         't': [t, 0],
         'attrs': msg.attrs,
         'desc': EVENT_DESC.get(msg.ev, '')}
    # Bit of a hack here. The "resp" attribute of a "reply"
    # event is URL-encoded, we unencoded it before converting
    # to JSON
    if msg.ev == 'reply' and ('resp' in msg.attrs):
        d['attrs']['resp'] = urllib.unquote(d['attrs']['resp'])
    return [b'EVENT', json.dumps(d)]


def reformat_eng(inmsg, rd):
    obj = json.loads(inmsg)
    if obj['name'] == 'profiler':
        data = obj['data']
        if 'itemp' in data:
            data['itemp'] = [x*100 for x in data['itemp']]
        if 'humidity' in data:
            data['humidity'] = int(data['humidity'] / 10.)
        inmsg = json.dumps(obj)
    return [b'DATA', inmsg]


def consumer(connection, chan, outq, create_msg):
    """
    Subscribe to messages on a Redis pubsub channel and append
    them to a :class:`Queue`.

    :param connection: Redis connection object
    :param chan: channel name
    :param outq: output message queue
    :param create_msg: callable to reformat the message.
    """
    pubsub = connection.pubsub()
    pubsub.subscribe(chan)
    for msg in pubsub.listen():
        if msg['type'] == 'message':
            try:
                outq.put(create_msg(msg['data'], connection))
            except Exception:
                logging.exception('Message format error (%r)',
                                  repr(msg['data']))


def publisher(inq, sock, hb_interval=None):
    """
    Read messages from a :class:`Queue` and publish them on a ZeroMQ
    PUB socket.

    :param inq: message queue
    :param sock: ZeroMQ PUB socket
    :param hb_interval: heart-beat interval in seconds
    """
    while True:
        try:
            item = inq.get(block=True, timeout=hb_interval)
        except Empty:
            t = int(time.time())
            d = {'name': 'heartbeat',
                 't': [t, 0],
                 'desc': 'network connection heartbeat'}
            item = [b'EVENT', json.dumps(d)]
        if isinstance(item, list):
            sock.send_multipart(item)


# Map Redis pubsub channels to ZeroMQ PUB sockets and
# message formatting functions
MESSAGE_TABLE = {
    'events.mmp': ('tcp://*:5520', reformat_events, 10),
    'data.sci': ('tcp://*:5510', lambda x, _: [b'DATA', x], None),
    'data.eng': ('tcp://*:5520', reformat_eng, 10)
}

SENSORS = [
    'ctd_1',
    'optode_1',
    'acm_1',
    'flcd_1',
    'flntu_1',
    'acs_1'
]


def start_tasks(ctx):
    queues = {}
    for key, entry in MESSAGE_TABLE.items():
        endpoint, func, hb = entry
        if endpoint in queues:
            spawn(consumer, redis.StrictRedis(), key,
                  queues[endpoint], func)
        else:
            sock = ctx.socket(zmq.PUB)
            sock.bind(endpoint)
            q = Queue()
            spawn(publisher, q, sock, hb)
            queues[endpoint] = q
            spawn(consumer, redis.StrictRedis(), key, q, func)


def queue_dpc_message(rd, contents, mqueue='dpc:commands', prepend=False):
    msg_id = uuid.uuid4().hex
    if prepend:
        rd.lpush(mqueue, json.dumps({'id': msg_id,
                                     'body': contents}))
    else:
        rd.rpush(mqueue, json.dumps({'id': msg_id,
                                     'body': contents}))
    return b'QUEUED', {'id': msg_id}


def handle_req(command, payload=None):
    """
    Handle command and control message from a client.
    """
    rd = redis.StrictRedis()
    if command == b'GOTO_DOCK':
        tag, contents = queue_dpc_message(rd, 'dock', prepend=True)
    elif command == b'CONTINUE':
        tag, contents = queue_dpc_message(rd, 'continue')
    elif command == b'GO':
        tag, contents = queue_dpc_message(rd, 'go')
    elif command == b'LIST_QUEUE':
        mq = [json.loads(s) for s in rd.lrange('dpc:commands', 0, -1)]
        tag, contents = b'QUEUED', mq
    elif command == b'PROFILES':
        count = 0
        ids = []
        last_pat = ''
        for p in payload:
            if isinstance(p['dir'], basestring):
                p['dir'] = p['dir'].upper()
            if 'queueing' in p and isinstance(p['queueing'], basestring):
                p['queueing'] = p['queueing'].upper()
            if 'exclude' not in p:
                p['exclude'] = []

            try:
                pm = ProfileMessage(**p)
                count = count + 1
            except Exception as e:
                logging.exception(e)
            else:
                # We need to add a 'pattern' command to the queue
                # for each new pattern that is defined.
                pat = p.get('pattern', '')
                if pat != last_pat:
                    if last_pat:
                        _, c = queue_dpc_message(rd,
                                                 'pattern {0}'.format(last_pat))
                        ids.append(c['id'])
                    last_pat = pat
                _, c = queue_dpc_message(rd,
                                         "prf " + b64encode(pm.serialize()))
                ids.append(c['id'])

        if last_pat:
            _, c = queue_dpc_message(rd,
                                     'pattern {0}'.format(last_pat))
            ids.append(c['id'])
        if count > 0:
            queue_dpc_message(rd, "go")
        tag, contents = b'QUEUED', {'ids': ids}
    elif command == b'LIST_SENSORS':
        tag, contents = b'SENSORS', SENSORS
    else:
        tag, contents = b'ERROR', {'desc': 'Invalid command'}
    return [tag, json.dumps(contents)]


def mainloop(cmd_endpoint='tcp://*:5500'):
    ctx = zmq.Context()
    sock = ctx.socket(zmq.REP)
    sock.bind(cmd_endpoint)
    logging.info('Starting tasks')
    start_tasks(ctx)
    while True:
        try:
            cmd, payload = sock.recv_multipart()
        except ValueError:
            logging.exception('Malformed message')
            reply = [b'ERROR',
                     json.dumps({'desc': 'Bad message format'})]
        else:
            logging.debug('Got message: [{0}, {1!r}]'.format(cmd, payload))
            if payload:
                try:
                    contents = json.loads(payload)
                except Exception:
                    logging.exception(repr(payload))
                    reply = [b'ERROR',
                             json.dumps({'desc': 'Bad message frame format'})]
                else:
                    reply = handle_req(cmd, contents)
            else:
                reply = handle_req(cmd)
        sock.send_multipart(reply)

if __name__ == '__main__':
    rval = 0
    if os.environ.get('LOGDEBUG'):
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    try:
        mainloop()
    except KeyboardInterrupt:
        sys.stderr.write('Exiting ...\n')
    except Exception:
        logging.exception('Unhandled exception')
        rval = 1
    sys.exit(rval)
