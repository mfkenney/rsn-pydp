#!/usr/bin/env python
#
# DPC Sensor Manager process
#
import gevent
from gevent import monkey
monkey.patch_all()
import sys
import os
import redis
import logging
import pydp
import pydp.data
import pydp.power
import pydp.serialcom
import pydp.sensors
from pydp.dio import Ts4200LcdDio
import imp
from ast import literal_eval


class SensorStateError(Exception):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return 'Attempt to access powered-down sensor {0}'.format(self.name)


class SensorAgent(object):
    """
    Class to wrap Sensor along with its associated serial port and
    power switch.
    """
    def __init__(self, name, cls, devname, switch, baud=9600,
                 flow=None, warmup=0):
        """
        :param name: name of this sensor instance.
        :param cls: Sensor class.
        :param devname: serial device name.
        :param switch: power switch.
        :param baud: serial baud rate.
        :param flow: serial flow control
        :param warmup: warmup time in seconds.
        """
        self.name = name
        self.logger = logging.getLogger(name)
        self.port = pydp.serialcom.Port(devname, baud=baud, flow=flow)
        self.switch = switch
        self.warmup = warmup
        self.sensor_class = cls
        self.sensor = None

    def on(self):
        """
        Power-on the device, open the serial port and create a new
        Sensor instance.
        """
        self.logger.info('Powering on')
        self.switch.on()
        t = self.warmup - self.switch.timer()
        if t > 0:
            gevent.sleep(t)
        self.port.open()
        self.sensor = self.sensor_class(self.port)

    def off(self):
        """
        Delete the Sensor instance, close the serial port and power-off
        the device.
        """
        if self.sensor is not None:
            self.logger.info('Powering off')
            del self.sensor
            self.sensor = None
            self.port.close()
            self.switch.off()

    def __getattr__(self, name):
        """
        Forward attribute requests to the Sensor instance.

        :raise: :class:`SensorStateError`
        """
        if self.port.isopen():
            return getattr(self.sensor, name)
        raise SensorStateError(self.name)


def find_sensor_class(name):
    """
    Locate and return the class for a sensor using its name
    relative to the pydp.sensors package.
    """
    modname, classname = name.split('.')
    fullname = 'pydp.sensors.' + modname
    if fullname not in sys.modules:
        pkg = sys.modules['pydp.sensors']
        fp, pathname, desc = imp.find_module(modname, pkg.__path__)
        try:
            sens_mod = imp.load_module(fullname, fp, pathname, desc)
        finally:
            if fp:
                fp.close()
    else:
        sens_mod = sys.modules[fullname]
    return getattr(sens_mod, classname)


def args_to_dict(args, do_eval=True):
    """
    Convert a command argument list of the form NAME=VALUE
    into a dictionary.

    >>> d = args_to_dict('foo=bar baz=1,2,3').items()
    >>> d.sort()
    >>> d
    [('baz', [1, 2, 3]), ('foo', 'bar')]
    """
    def maybe_eval(s):
        if do_eval:
            try:
                return literal_eval(s)
            except (ValueError, SyntaxError):
                return s
        else:
            return s
    params = {}
    for name, val in [arg.split('=') for arg in args.split()]:
        if ',' in val:
            params[name] = [maybe_eval(v) for v in val.split(',')]
        else:
            params[name] = maybe_eval(val)
    return params


def force_off(rd, names):
    """
    Power off all listed sensors.
    """
    dioport = Ts4200LcdDio()
    for name in names:
        cfg = rd.hgetall(name + '_cfg')
        if cfg:
            sw = pydp.power.PowerSwitch(int(cfg['switch']), dioport)
            sw.off()


def manage_sensors(rd, names):
    dm = pydp.data.DataManager(rd)
    dioport = Ts4200LcdDio()
    for name in names:
        cfg = rd.hgetall(name + '_cfg')
        klass = find_sensor_class(cfg['class'])
        sw = pydp.power.PowerSwitch(int(cfg['switch']), dioport)
        # Be generous in how we allow the user to specify a
        # True value in the system configuration.
        keep_raw = cfg.get('keep_raw', '0')
        if keep_raw.lower() in ('1', 'true', 'yes', 'ok'):
            keep_raw = True
        else:
            keep_raw = False
        args = cfg.get('args', {})
        if args:
            args = args_to_dict(args)
        agent = SensorAgent(
            name,
            klass,
            cfg['device'],
            sw,
            baud=int(cfg['baud']),
            flow=cfg.get('flow'),
            warmup=int(cfg.get('warmup', 1)))
        dm.add_sensor(
            name, agent, int(cfg['interval']),
            timeout=int(cfg['timeout']),
            keep_raw=keep_raw, prep_args=args)
    return dm


def sensors_on(dm, disabled):
    for name in dm.sensors():
        sens = dm[name]
        if name not in disabled:
            sens.on()


def sensors_off(dm):
    for name in dm.sensors():
        dm[name].off()


def split_message(msg):
    """
    Split a message from the *events.mmp* channel into an event string
    and a dictionary of attributes.

    >>> event, attrs = split_message('profile:start t=123456789 pnum=42 mode=up')
    >>> event
    'profile:start'
    >>> attrs['pnum']
    '42'
    """
    try:
        event, args = msg.split(' ', 1)
    except ValueError:
        event = msg
        attrs = {}
    else:
        attrs = dict([s.split('=', 1) for s in args.split()])
    return event, attrs


def mainloop(rd, snames=None):
    """
    Listen for *profile:start* and *profile:stop* messages on the
    *events.mmp* Redis channel.
    """
    pubsub = rd.pubsub()
    pubsub.subscribe('events.mmp')
    dm = None
    inprofile = False
    logging.info('Power off all sensors')
    force_off(rd,
              snames or rd.lrange('sensors', 0, -1))
    logging.info('Waiting for a message')
    rd.hset('sm', 'state', 'wait')
    try:
        for msg in pubsub.listen():
            if msg['type'] == 'message':
                event, attrs = split_message(msg['data'])
                if event == 'profile:start':
                    if not inprofile:
                        logging.info('Starting profile: %r', attrs)
                        dm = manage_sensors(rd,
                                            snames or rd.lrange('sensors',
                                                                0, -1))
                        # Do not power-on sensors during a docking profile
                        if attrs.get('mode', '') != 'docking':
                            rd.hset('sm', 'state', 'sampling')
                            sensors_on(dm,
                                       rd.lrange('sens:exclude', 0, -1))
                        dm.start()
                        inprofile = True
                    else:
                        logging.warning('Profile already started')
                elif event == 'profile:end':
                    if inprofile:
                        logging.info('Ending profile: %r', attrs)
                        dm.stop()
                        sensors_off(dm)
                        rd.hset('sm', 'state', 'wait')
                        inprofile = False
                    else:
                        logging.warning('Profile already stopped')
    finally:
        if inprofile:
            logging.info('Profile interrupted')
            dm.stop()
            sensors_off(dm)


def main():
    if os.environ.get('LOGDEBUG'):
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    rd = redis.StrictRedis()
    rd.hset('sm', 'pid', os.getpid())
    sensor_list = None
    if len(sys.argv) > 1:
        sensor_list = sys.argv[1:]
    mainloop(rd, sensor_list)


if __name__ == '__main__':
    rval = 0
    try:
        main()
    except KeyboardInterrupt:
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Unhandled exception')
        rval = 1
    sys.exit(rval)
