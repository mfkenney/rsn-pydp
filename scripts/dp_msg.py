#!/usr/bin/env python
#
import sys
import zmq
import simplejson as json
import time
import argparse
from pprint import pprint


def show_message(mtype, contents, outf=sys.stdout):
    t = time.time()
    outf.write(time.strftime('[%Y-%m-%d %H:%M:%S]',
                             time.gmtime(t)))
    outf.write(' {0} \n'.format(mtype))
    pprint(contents, stream=outf)


def main():
    """
    Send a message to the Deep Profiler.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('cmd', help='message command')
    parser.add_argument('content', help='JSON format message content',
                        nargs='?', default='')
    parser.add_argument('--host', help='DP dock hostname',
                        default='localhost')
    args = parser.parse_args()

    # Validate the message content
    body = ''
    if args.content:
        if args.content.startswith('@'):
            with open(args.content[1:]) as f:
                body = f.read()
        else:
            body = args.content
        try:
            json.loads(body)
        except Exception as e:
            sys.exit('Bad message content ({0!r}'.format(e))
    ctx = zmq.Context()
    sock = ctx.socket(zmq.REQ)
    sock.connect('tcp://{0}:5500'.format(args.host))
    sock.send_multipart([bytes(args.cmd.upper()), body])
    resp = sock.recv_multipart()
    show_message(resp[0], json.loads(resp[1]))


if __name__ == '__main__':
    main()
