#!/usr/bin/env python
#
# Create a simulated Deep Profiler data stream using a previously
# collected dataset.
#
import sys
import redis
import argparse
import simplejson as json
import time


def fake_profile_events(rd, pnum, state, docked=False):
    """
    Publish fake profile:start and profile:end messages. This allows us
    to synchronize any subscribers if we start in the middle of a
    profile.
    """
    modes = {
        'PROFILE_UP': 'up',
        'PROFILE_DOWN': 'down',
        'STOPPED': 'stationary',
        'DOCKING': 'docking'
    }
    t = int(time.time())
    if pnum > 0:
        if docked:
            rd.publish('events.mmp',
                       'docked t={0:d}'.format(t))
        rd.publish('events.mmp',
                   'profile:end t={0:d} pnum={1:d}'.format(t, pnum - 1))
    rd.publish('events.mmp',
               'profile:start t={0:d} pnum={1:d} mode={2}'.format(t,
                                                                  pnum,
                                                                  modes.get(state)))


def sleep_til(t):
    dt = t - time.time()
    try:
        time.sleep(dt)
    except IOError:
        pass


def data_records(rd, infile, t_scale=1):
    t_start = int(time.time())
    pnum = -1
    mode = None
    for line in infile:
        secs, usecs, content = line.strip().split('\t')
        drec = json.loads(content)
        if drec['name'] == 'profiler':
            if pnum != drec['data']['profile']:
                docked = (mode == 'DOCKING')
                fake_profile_events(rd, drec['data']['profile'],
                                    drec['data']['state'], docked=docked)
                pnum = drec['data']['profile']
                sys.stderr.write('Starting profile {0:d}\n'.format(pnum))
            channel = 'data.eng'
            mode = drec['data']['state']
        else:
            channel = 'data.sci'
        t_send = t_start + int(secs) * t_scale
        drec['t'][0] = t_send
        sleep_til(t_send)
        yield channel, drec


def main():
    parser = argparse.ArgumentParser(description='data stream simulator')
    parser.add_argument('infile', help='input data file',
                        type=argparse.FileType('r'),
                        nargs='?', default=sys.stdin)
    parser.add_argument('--speedup', help='speed-up factor',
                        type=float, default=1)
    args = parser.parse_args()

    rd = redis.StrictRedis()
    for channel, drec in data_records(rd, args.infile, args.speedup):
        rd.publish(channel, json.dumps(drec))


if __name__ == '__main__':
    main()
