#!/usr/bin/env python
#
"""
Usage: bcheck.py [-h] muxaddr muxchan

Check the status of a Smart Battery and write the result to standard
output in CSV format. If '-h' is specified, a CSV header line is also
written.
"""
import sys
import smbus
import time
from decimal import Decimal
from datetime import datetime
from pydp.battery import Battery, Mux, median_value


# Data display conversion functions
def cvt_voltage(mv):
    return Decimal(mv) / Decimal(1000)


def cvt_current(ma):
    # The two-byte value returned for the current readings must be
    # interpreted as a signed integer.
    if ma & 0x8000:
        ma -= 65536
    return Decimal(ma) / Decimal(1000)


def cvt_time(t):
    return datetime.utcfromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')


def cvt_capacity(tmwh):
    return Decimal(tmwh) / Decimal(100)


def cvt_temp(k):
    return Decimal(k) / Decimal(10) - Decimal('273.15')

FIELDS = [
    ('s/n', 'SerialNumber', None),
    ('voltage', 'Voltage', cvt_voltage),
    ('current', 'Current', cvt_current),
    ('avg_current', 'AverageCurrent', cvt_current),
    ('chg_current', 'ChargingCurrent', cvt_current),
    ('temperature', 'Temperature', cvt_temp),
    ('max_error', 'MaxError', None),
    ('rel_charge', 'RelativeStateOfCharge', None),
    ('abs_charge', 'AbsoluteStateOfCharge', None),
    ('energy', 'RemainingCapacity', cvt_capacity),
    ('full_charge', 'FullChargeCapacity', cvt_capacity),
    ('time_remaining', 'RunTimeToEmpty', None),
    ('avg_time_remaining', 'AverageTimeToEmpty', None),
    ('avg_time_to_full', 'AverageTimeToFull', None),
    ('status', 'BatteryStatus', hex),
    ('cycles', 'CycleCount', None)
]


def collect(b):
    """
    Collect the data for a Battery.

    :param b: Battery instance
    :return: list of data keys, values
    """
    rec = [
        ('time', cvt_time(time.time())),
        ('mux', hex(b.mux.addr)),
        ('channel', b.mux_channel)
    ]
    for key, method, cvt in FIELDS:
        func = getattr(b, method)
        try:
            value = median_value(func, n=11)
            if cvt:
                rec.append((key, cvt(value)))
            else:
                rec.append((key, value))
        except IOError:
            rec.append((key, ''))
    return rec


def display(rec, header=False):
    if header:
        sys.stdout.write(','.join([str(k) for k, v in rec]) + '\n')
    sys.stdout.write(','.join([str(v) for k, v in rec]) + '\n')


def main():
    header = False
    try:
        if sys.argv[1] in ('-h', '--header'):
            header = True
            args = sys.argv[2:]
        else:
            args = sys.argv[1:]
        addr, chan = [int(a, 0) for a in args]
    except (IndexError, ValueError) as e:
        sys.stderr.write(__doc__)
        return 1
    bus = smbus.SMBus(0)
    # Bit of a hack here. Create an object for each battery mux in the
    # system to ensure that only one mux is active at any given time.
    # See Mux.__new__ for details.
    Mux(bus, 0x70)
    Mux(bus, 0x77)
    b = Battery(bus, addr, chan)
    try:
        b._initialize()
    except IOError as e:
        sys.stderr.write(str(e) + '\n')
        return 1
    display(collect(b), header=header)
    return 0

if __name__ == '__main__':
    sys.exit(main())
