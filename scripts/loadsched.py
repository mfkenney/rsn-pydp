#!/usr/bin/env python
"""
Load MMP schedules into a Redis data store.
"""
import yaml
import simplejson as json
import redis
import argparse
import sys


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('infile', help='YAML configuration file',
                        type=argparse.FileType('r'),
                        nargs='?',
                        default=sys.stdin)
    parser.add_argument('--host', help='hostname of Redis server',
                        default='localhost')
    parser.add_argument('--db', help='Redis database index',
                        type=int,
                        default=0)
    args = parser.parse_args()

    rd = redis.StrictRedis(host=args.host, db=args.db)
    try:
        cfg = yaml.load(args.infile)
    except IOError:
        sys.stderr.write('Cannot read config file\n')
        sys.exit(1)

    # Save the schedule file contents in a separate dictionary ...
    schedule_db = dict((e['name'], e['contents']) for e in cfg['available'])
    # ... and remove from the configuration dictionary
    for sched in cfg['available']:
        del sched['contents']

    rd.set('schedules', json.dumps(cfg))
    rd.hmset('schedule_db', schedule_db)


if __name__ == '__main__':
    main()
