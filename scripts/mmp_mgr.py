#!/usr/bin/env python
#
# MMP Manager process
#
import sys
import gevent
from gevent import monkey
monkey.patch_all()
import os
import time
import redis
import logging
import zlib
from pydp.dio import Ts4200LcdDio
from pydp.serialcom import Port
from pydp.power import PowerSwitch
from pydp.mpc import MPC
from pydp.data import DataQueue


def gzip_expand_string(s):
    """
    Uncompress the contents of a Gzip-compressed file stored
    in a string.
    """
    # basic gzip header length
    hdrlen = 10
    # Null-terminated filename is appended to the header if the FNAME
    # header flag is set
    if (ord(s[3]) & 0x08) == 0x08:
        namelen = s[hdrlen:].find('\x00')
        hdrlen = hdrlen + namelen + 1
    return zlib.decompress(s[hdrlen:], -zlib.MAX_WBITS)


def write_schedule(rd, name, key='schedule_db', sched_file='schedule.dpl'):
    """
    Copy the contents of a McLane Mission Schedule from the Redis
    data store to a temporary file so it can be uploaded to the
    MPC.

    :param rd: Redis connection object
    :param name: schedule name
    :return: pathname of schedule file
    :raises: ValueError if schedule is not found
    """
    contents = rd.hget(key, name)
    if not contents:
        raise ValueError('Bad schedule name: {0}'.format(name))
    else:
        filename = os.path.join(os.environ.get('TMPDIR', '/tmp'), sched_file)
        with open(filename, 'wb') as f:
            f.write(gzip_expand_string(contents))
        return filename


class MpcFsm(object):
    """
    State machine to manage the interaction with the MPC. Uses Redis for
    message passing.

    Write queues:
        * archive:queue

    Read queues:
        * mmp:hold
        * mmp:messages

    Pub channels:
        * events.mmp

    """
    #: Map MMP mode codes to strings
    modes = {
        '0': 'down',
        '1': 'up',
        '2': 'stationary',
        '3': 'docking'
    }
    #: Variable names for MMP data record
    variables = (
        ('pnum', int),
        ('current', float),
        ('vbatt', float),
        ('pressure', float),
        ('mode', lambda n: MpcFsm.modes.get(n, n)))
    #: Incoming message queue for HOLD events
    hold_queue = 'mmp:hold'
    #: Incoming message queue for messages to MMP
    msg_queue = 'mmp:messages'
    #: Channel to publish profiler events
    event_channel = 'events.mmp'
    #: number of seconds to allow between messages in profile mode
    profile_timeout = 35
    #: number of seconds to wait at the end of a profile
    profile_end_wait = 60
    #: number of seconds to allow for a reply from the MMP
    reply_timeout = 10
    #: delay between reciept of XMTACK and start of file transfer
    xmtack_delay = 5
    #: delay between power-down and power-up for an MPC restart
    restart_delay = 10
    #: number of seconds to allow for MPC startup
    startup_timeout = 300

    def __init__(self, rd, sw, logger=None, aux_sw=None):
        self.state = 'start'
        self.t_start = 0
        self.logger = logger or logging.getLogger('mpc-fsm')
        self.data_q = DataQueue(rd, 'archive:queue')
        self.rd = rd
        self.outgoing = None
        self.pnum = -1
        self.xmtsch_tries = 0
        self.sw = sw
        self.aux_sw = aux_sw
        self.dock_requested = False
        self.last_command = None
        self.t_next_profile, self.t_listen_end = -1, -1

    def __call__(self, t=None, msg=None):
        """
        Use a message arriving from the MPC to drive the state
        machine. The return value is a two-element tuple. The
        first element is the reply message to send to the MPC
        and the second element is the number of seconds to wait
        for the next message (or ``None``)
        """
        func = getattr(self, 'state_' + self.state.upper(), self._default)
        return func(t, msg)

    def _default(self):
        self.logger.critical('Invalid state: %s', self.state)
        self.state = 'wait'
        return None, None

    def _next_message(self):
        """
        Check the incoming queue for a message. Returns a message
        to send to the MPC
        """
        rval = None
        outmsg = self.rd.lpop(self.msg_queue)
        if outmsg:
            # Schedule-file transfer
            if outmsg.startswith('XMTSCH'):
                rval = ('XMTSCH',)
                # The name of the schedule refers to a hash-table
                # entry in the Redis data store. We need to write
                # the contents to a file in order to upload the
                # schedule to the MPC.
                try:
                    self.outgoing = write_schedule(self.rd,
                                                   outmsg.split()[1])
                except (IndexError, ValueError):
                    self.logger.exception('Bad/missing schedule name')
                    self.outgoing = None
                    rval = None
                else:
                    self.xmtsch_tries = 1
                    self.logger.info('Initiate schedule download')
            else:
                rval = (outmsg,)
        return rval

    def _restart(self):
        """
        Restart the MPC
        """
        self.logger.critical('Restarting MPC')
        self.rd.publish(self.event_channel,
                        "restart t={0:d}".format(int(time.time())))
        self.sw.off()
        if self.aux_sw:
            self.aux_sw.off()
        gevent.sleep(self.restart_delay)
        self.sw.on()
        if self.aux_sw:
            self.aux_sw.on()
        self.state = 'start'

    def send_event(self, t, msg):
        ev = None
        if msg[0] == 'MMPPRF':
            args = 't={0:d} pnum={1} mode={2}'.format(int(t), msg[2],
                                                      self.modes.get(msg[3],
                                                                     msg[3]))
            ev = 'profile:start ' + args
            self.rd.hset('mmp_status', 'state', 'profiling')
        elif msg[0] == 'MMPEND':
            args = 't={0:d} pnum={1} status={2}'.format(int(t), msg[2], msg[3])
            ev = 'profile:end ' + args
        elif msg[0] == 'MMPOBS':
            ev = 'stalled t={0:d} pr={1} iter={2}'.format(
                int(t), msg[2], msg[3])
        elif msg[0] == 'MMPDCK':
            ev = 'docked t={0:d} pr={1}'.format(int(t), msg[2])
            self.rd.hset('mmp_status', 'state', 'docked')
        if ev:
            self.rd.publish(self.event_channel, ev)

    def state_START(self, t, msg):
        """
        Start-up state. Any message causes a transition to WAIT state.
        """
        if msg:
            self.state = 'wait'
            return self.state_WAIT(t, msg)
        else:
            # Timeout at start-up. All we can do is reboot the MPC
            # and hope for the best.
            self._restart()
            return None, self.startup_timeout

    def state_WAIT(self, t, msg):
        """
        Wait for a message from the MPC to determine the next state.
        """
        rval, timeout = None, None
        if msg:
            if msg[0] == 'MMPPRF':
                hold = self.rd.lpop(self.hold_queue)
                if hold:
                    self.t_start = time.time() + int(hold)
                    self.logger.info('Profile will start in %s seconds', hold)
                if time.time() > self.t_start:
                    self.state = 'profile'
                    rval = ('ACKPRF', 1)
                    self.pnum = int(msg[2])
                    self.send_event(t, msg)
                else:
                    rval = ('ACKPRF', 0)
            elif msg[0] == 'MMPPOS':
                # Force a profile:start event
                pnum = int(msg[2])
                if self.pnum != pnum:
                    if pnum > 0:
                        self.logger.info('Generating profile:end event')
                        self.send_event(t,
                                        ('MMPEND', msg[1], str(pnum - 1), '0'))
                    self.logger.info('Generating profile:start event')
                    self.send_event(t, ('MMPPRF', msg[1], msg[2], msg[6]))
                    self.pnum = pnum
                self.state = 'profile'
                timeout = self.profile_timeout
            elif msg[0] == 'MMPEND':
                wait_time = self.rd.hget('mmp_settings', 'profile_end_wait')
                if wait_time is not None:
                    wait_time = int(wait_time)
                else:
                    wait_time = self.profile_end_wait
                if self.dock_requested:
                    self.t_listen_end = -1
                else:
                    self.t_listen_end = time.time() + wait_time
                    self.logger.info('Listen loop ends at %d',
                                     self.t_listen_end)
                self.send_event(t, msg)
                rval = ('ACKEND', wait_time)
                self.state = 'checkmsg'
                timeout = 10
            elif msg[0] == 'MMPDCK':
                self.send_event(t, msg)
                rval = ('ACKDCK',)
                timeout = self.profile_timeout
                if not self.dock_requested:
                    self.logger.info('Unexpected docking')
            elif msg[0] == 'MMPSCH':
                self.t_next_profile = time.time() + int(msg[1])
                self.logger.info('Next profile at %d',
                                 int(self.t_next_profile))
            else:
                self.send_event(t, msg)
        else:
            self.state = 'checkmsg'
            timeout = 0
        return rval, timeout

    def state_PROFILE(self, t, msg):
        """
        Profiling. MPC should send MMPPOS messages on a periodic basis.
        """
        rval, timeout = None, None
        self.t_listen_end = -1
        if msg:
            self.send_event(t, msg)
            if msg[0] == 'MMPPOS':
                rec = {}
                for v, field in zip(self.variables, msg[2:]):
                    rec[v[0]] = v[1](field)
                secs = int(t)
                usecs = int((t - secs) * 1000000)
                self.data_q.put(('mmp_1', secs, usecs, rec))
                timeout = self.profile_timeout
                self.pnum = int(msg[2])
            elif msg[0] == 'MMPEND':
                wait_time = self.rd.hget('mmp_settings', 'profile_end_wait')
                if wait_time is not None:
                    wait_time = int(wait_time)
                else:
                    wait_time = self.profile_end_wait
                rval = ('ACKEND', wait_time)
                self.state = 'checkmsg'
                if self.dock_requested:
                    self.t_listen_end = -1
                else:
                    self.t_listen_end = time.time() + wait_time
                    self.logger.info('Listen loop ends at %d',
                                     self.t_listen_end)
                timeout = 10
            elif msg[0] == 'MMPDCK':
                rval = ('ACKDCK',)
                timeout = self.profile_timeout
                if not self.dock_requested:
                    self.logger.info('Unexpected docking')
            elif msg[0] == 'MMPPRF':
                self.state = 'wait'
        else:
            self.state = 'wait'
            self.logger.info('Timeout in profile state')
        return rval, timeout

    def state_CHECKMSG(self, t, msg):
        """
        Check for messages to send to the MPC
        """
        rval = self._next_message()
        self.last_command = rval
        if rval is None:
            self.state = 'wait'
            timeout = 10
        elif rval[0] == 'CTLCNT':
            self.state = 'wait'
            timeout = None
            self.dock_requested = False
        else:
            self.dock_requested = rval[0] == 'CTLCHG'
            self.state = 'msgxfer'
            timeout = self.reply_timeout
        return rval, timeout

    def state_MSGXFER(self, t, msg):
        rval, timeout = None, None
        if msg:
            self.last_command = None
            if msg[0] == 'XMTACK' and self.outgoing is not None:
                rval = ('SENDFILE', self.outgoing)
                self.logger.info('Start file transfer (%s)', self.outgoing)
                self.outgoing = None
                gevent.sleep(self.xmtack_delay)
            self.state = 'checkmsg'
            timeout = 2
        else:
            self.logger.warning('No response from MPC')
            if self.outgoing is not None and self.xmtsch_tries > 0:
                self.logger.info('Retry XMTSCH')
                rval = ('XMTSCH',)
                timeout = self.reply_timeout
                self.xmtsch_tries -= 1
            elif self.last_command is not None:
                rval = self.last_command
                timeout = self.reply_timeout
            else:
                self.state = 'wait'
                timeout = self.profile_timeout
        return rval, timeout


def mmp_control(redis_connect, dioport):
    rd = redis_connect()
    logger = logging.getLogger('mmp')
    cfg = rd.hgetall('mmp_cfg')
    sw = PowerSwitch(int(cfg['switch']), dioport)
    if cfg.get('aux_switch'):
        aux_sw = PowerSwitch(int(cfg['aux_switch']), dioport)
    else:
        aux_sw = None
    port = Port(cfg['device'], baud=int(cfg['baud']))
    logger.info('Powering on MPC')
    sw.on()
    if aux_sw:
        aux_sw.on()
    gevent.sleep(float(cfg.get('warmup', 0)))
    port.open()
    mpc = MPC(port, xmodem_debug=cfg.get('debug'))
    fsm = MpcFsm(rd, sw, logger, aux_sw=aux_sw)
    logger.debug('state: %s', fsm.state)
    timeout = fsm.startup_timeout
    try:
        while True:
            if (timeout is None) or (timeout > 0):
                t, msg = mpc.recvmsg(timeout=timeout)
            else:
                t, msg = None, None
            logger.debug('message=%r', msg)
            try:
                reply, timeout = fsm(t, msg)
            except Exception:
                logger.exception('Cannot process message: %r', msg)
                reply, timeout = None, fsm.profile_timeout
            logger.debug('state=%s', fsm.state)
            if reply is not None:
                mpc.sendmsg(reply, timeout=3)
    finally:
        logger.info('Powering off MPC')
        port.close()
        sw.off()
        if aux_sw:
            aux_sw.off()
        # Send a fake profile end event
        if fsm.state != 'wait':
            t = time.time()
            fsm.send_event(t,
                           [str(e) for e in ['MMPEND', int(t), fsm.pnum, 2]])


def main():
    if os.environ.get('LOGDEBUG'):
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    rd = redis.StrictRedis()
    rd.delete('mmp:commands')
    task = None
    logging.info('Ready for commands')
    try:
        while True:
            resp = rd.blpop('mmp:commands')
            if resp:
                msg = resp[1].lower()
                if msg == 'start':
                    if task is None:
                        task = gevent.spawn(mmp_control, redis.StrictRedis,
                                            Ts4200LcdDio())
                elif msg == 'stop' and task is not None:
                    task.kill(timeout=4)
                    task = None
    finally:
        if task is not None:
            task.kill(timeout=5)

if __name__ == '__main__':
    rval = 0
    try:
        main()
    except KeyboardInterrupt:
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Unhandled exception')
        rval = 1
    sys.exit(rval)
