#!/usr/bin/env python
#
# Initialize the Redis data store
#
"""
Usage: loadcfg.py cfgfile

Use a YAML configuration file to initialize the Redis data-store.
"""
import sys
import yaml
import redis


def main():
    try:
        cfg = yaml.load(open(sys.argv[1], 'r'))
    except IndexError:
        sys.stderr.write(__doc__)
        sys.exit(1)
    except IOError:
        sys.stderr.write('Cannot read config file\n')
        sys.exit(1)

    rd = redis.StrictRedis()

    for key, val in cfg.items():
        if isinstance(val, list):
            for v in val:
                rd.rpush(key, str(v))
        elif isinstance(val, dict):
            for k, v in val.items():
                rd.hset(key, k, str(v))
        else:
            rd.set(key, str(val))


if __name__ == '__main__':
    main()
