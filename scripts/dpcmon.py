#!/usr/bin/env python
"""dpcmon.py

Monitor the IMM interface to the DPC.
"""
import sys
import logging
import time
import gevent
from gevent import monkey
monkey.patch_all()
import redis
from base64 import b64decode
from pydp.serialcom import Port
from pydp.imm2 import IMM
from pydp.dio import Ts4800Dio
from pydp.power import PowerSwitch
from pydp.data import DataQueue
from pydp.messaging import putmsg, line_released, line_captured
from pydp.messages import StatusMessage, SensorMessage, BatteryMessage, \
    pb_to_dict, EventMessage
import simplejson as json
import os


class TaskWatchdog(object):
    """
    Provide watchdog functionality for a Greenlet task. Restart the task
    if it stops due to an exception.
    """
    restart_delay = 3

    def __init__(self, func, *args, **kwds):
        self.func = func
        self.args = args
        self.kwds = kwds
        self.task = gevent.Greenlet(func, *args, **kwds)
        self.task.link_exception(self)
        self.task.start()

    def __call__(self, greenlet):
        logging.exception(greenlet.exception)
        gevent.sleep(self.restart_delay)
        self.task = gevent.Greenlet(self.func, *self.args, **self.kwds)
        self.task.link_exception(self)
        self.task.start()
        return self.task

    def __getattr__(self, name):
        return getattr(self.task, name)


def find_peer(imm):
    """
    Return the serial number of our peer IMM.
    """
    with line_captured(imm, delay=2, force=True):
        _, response = imm.command('disc', 5)
        e = response.find('.//Discovered')
        return e.get('SN') if e is not None else None


def data_records(msg):
    """
    Generator to construct outgoing data records from an input
    Protocol Buffer message.
    """
    if isinstance(msg, SensorMessage):
        for name, data in pb_to_dict(msg).iteritems():
            secs, usecs = divmod(data['time'], 1000000)
            del data['time']
            source = '{0}_1'.format(name)
            yield source, secs, usecs, data
    elif isinstance(msg, StatusMessage):
        source = 'profiler'
        data = pb_to_dict(msg)
        secs, usecs = divmod(data['time'], 1000000)
        del data['time']
        yield source, secs, usecs, data


def fake_profile_events(rd, pnum, state):
    """
    Publish fake profile:start and profile:end messages. This allows us
    to synchronize any subscribers if we start in the middle of a
    profile.
    """
    modes = {
        'PROFILE_UP': 'up',
        'PROFILE_DOWN': 'down',
        'STOPPED': 'stationary',
        'DOCKING': 'docking'
    }
    t = int(time.time())
    mode = modes.get(state)
    if pnum > 0:
        rd.publish('events.mmp',
                   'profile:end t={0:d} pnum={1:d}'.format(t, pnum - 1))
    rd.publish('events.mmp',
               'profile:start t={0:d} pnum={1:d} mode={2}'.format(t,
                                                                  pnum,
                                                                  mode))


def process_dpc_message(rd, line):
    """
    Process a message from the DPC.

    :param rd: Redis connection instance
    :param line: message contents
    :type line: bytes
    :return: message class instance
    """
    msg_class = {
        'S': (StatusMessage, b64decode),
        'D': (SensorMessage, b64decode),
        'B': (BatteryMessage, b64decode),
        'I': (EventMessage, bytes)
    }
    data_q = DataQueue(rd, 'archive:queue')
    msg = None
    try:
        prefix, data = line.strip().split(':', 1)
    except (TypeError, ValueError):
        prefix = 'I'
        data = line.strip()
    if prefix in msg_class:
        klass, decode = msg_class[prefix]
        try:
            msg = klass.deserialize(decode(data))
            for rec in data_records(msg):
                data_q.put(rec)
                stream_msg = {'name': rec[0],
                              't': [rec[1], rec[2]],
                              'data': rec[3]}
                channel = 'data.eng' if rec[0] == 'profiler' else 'data.sci'
                rd.publish(channel, json.dumps(stream_msg))
        except Exception as e:
            logging.exception('Cannot decode message: %r', e)
            msg = None
    return msg


def recv_messages(rd, imm, peer_id, imm_timeout=300, pnum_start=-1):
    """
    Listen on the IMM link for messages from the DPC. Returns when
    the DPC sends a 'wait' message or on timeout.

    :param rd: Redis connection
    :param imm: IMM interface
    :param peer_id: serial number of the DPC IMM
    :param imm_timeout: IMM message timeout in seconds
    :param pnum_start: expected starting profile number from DPC.
    :return: tuple of the current profile number and the DPC state.
    """
    pnum = pnum_start
    state = ''
    with line_released(imm, force=True):
        done = False
        while not done:
            t_recv, line = imm.readline(imm_timeout)
            if line is not None and len(line):
                msg = process_dpc_message(rd, str(line))
                if isinstance(msg, StatusMessage):
                    if msg.profile != pnum:
                        logging.info("Fake profile:start %d '%s'",
                                     msg.profile, msg.state)
                        fake_profile_events(rd, msg.profile, msg.state)
                        pnum = msg.profile
                elif isinstance(msg, EventMessage):
                    if msg.ev == 'profile:start':
                        pnum = int(msg.attrs.get('pnum', pnum))
                    elif msg.ev == 'wait':
                        state = msg.attrs.get('state', '')
                        done = True
                    rd.publish('events.mmp', msg.serialize())
            else:
                done = True
    return pnum, state


def send_messages(rd, imm, peer_id, cqueue='dpc:commands'):
    # List of commands which will start a new profile.
    terminators = ('go', 'dock', 'continue')
    cmd = rd.lpop(cqueue)
    while cmd is not None:
        logging.info('DPC command = %r', cmd)
        try:
            obj = json.loads(cmd)
        except ValueError:
            obj = {'id': None, 'body': cmd}
        try:
            reply = putmsg(imm, peer_id, obj['body'],
                           timeout=10, async=False,
                           force=True)
        except Exception as e:
            logging.exception('Cannot send command: %r', e)
            # Return command to the queue
            rd.lpush(cqueue, json.dumps(obj))
            break

        if reply is not None:
            try:
                logging.info('Response = %r', reply.text)
                if obj['id']:
                    evmsg = EventMessage('reply',
                                         t=int(time.time()),
                                         id=obj['id'],
                                         resp=reply.text.split()[0])
                    rd.publish('events.mmp', evmsg.serialize())
                if cmd in terminators and reply.text == 'OK':
                    break
            except AttributeError:
                logging.warning('Unexpected response: %r', reply)
        else:
            logging.warning('No response')
            # Return command to the queue
            rd.lpush(cqueue, json.dumps(obj))
            break
        cmd = rd.lpop(cqueue)
    return rd.llen(cqueue)


def monitor(rd):
    """
    Power-on and configure the IMM then start the loop to listen for
    DPC messages.
    """
    cfg = rd.hgetall('imm_cfg')
    settings = rd.hgetall('imm_settings')
    dioport = Ts4800Dio()
    sw = PowerSwitch(cfg['switch'], dioport)
    port = Port(cfg['device'], baud=int(cfg['baud']))
    sw.on()
    gevent.sleep(float(cfg.get('warmup', 0)))
    port.open()
    pnum = -1
    try:
        with IMM(port) as imm:
            imm.wakeup()
            logging.info('Configuring IMM')
            for k, v in settings.items():
                imm.set(k, int(v))
            peer_id = cfg.get('peer_sn')
            if not peer_id:
                logging.info('Checking for DPC IMM')
                peer_id = find_peer(imm)
                if not peer_id:
                    raise RuntimeError('Cannot locate peer IMM')
            logging.info('DPC IMM s/n = %s', peer_id)
            while True:
                gevent.sleep(3)
                logging.info('Monitoring IMM messages')
                rd.hset('mon', 'state', 'receiver')
                pnum, state = recv_messages(rd, imm, peer_id,
                                            imm_timeout=90,
                                            pnum_start=pnum)
                gevent.sleep(5)
                logging.info('Sending queued messages')
                rd.hset('mon', 'state', 'sender')
                send_messages(rd, imm, peer_id)
    finally:
        rd.hset('mon', 'state', 'wait')
        sw.off()


def main():
    rval = 0

    def _catch_exc(greenlet):
        logging.exception(greenlet.exception)

    if os.environ.get('LOGDEBUG'):
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    rd = redis.StrictRedis()
    task = None
    logging.info('Ready for commands')
    rd.hset('mon', 'pid', os.getpid())
    rd.hset('mon', 'state', 'wait')
    try:
        while True:
            resp = rd.blpop('mon:commands')
            if resp:
                msg = resp[1].lower()
                if msg == 'start':
                    if task is None or task.ready():
                        task = TaskWatchdog(monitor, redis.StrictRedis())
                elif msg == 'stop':
                    if task and not task.ready():
                        task.kill(timeout=4)
                        task = None
    except KeyboardInterrupt:
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Unhandled exception')
        rval = 1
    finally:
        if task and not task.ready():
            task.kill(timeout=5)
    return rval

if __name__ == '__main__':
    sys.exit(main())
