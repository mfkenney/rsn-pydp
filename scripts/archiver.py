#!/usr/bin/env python
#
import gevent
from gevent import monkey
monkey.patch_all()
import time
import os
import sys
import msgpack
import logging
import redis
from pydp.data import DataQueue


def archive_data(rd, datadir, dataq, suffix):
    """
    Greenlet task to archive the sensor data in a series of files. The files
    are named as follows::

        SENSOR_SUFFIX.mpk

    Where *SENSOR* is the sensor name and *SUFFIX* is supplied by the caller.
    Each data record is stored as a set of three `Message Pack`_ objects. The
    first is an integer timestamp in seconds since 1/1/1970 UTC, the second is
    the integer microseconds, and the third is either a binary string (raw
    data) or a map of variable names and values.

    .. _Message Pack: http://msgpack.org/

    :param rd: redis.StrictRedis instance
    :param datadir: directory name
    :param dataq: incoming data queue
    :param suffix: filename suffix
    :param publish: callable which will be passed the sensor name and
                    data record
    :type publish: callable or ``None``
    """
    # Map sensor names to files
    files = {}
    logger = logging.getLogger('archiver')
    logger.info('Archiver starting')
    try:
        while True:
            name, secs, usecs, data = dataq.get()
            if name in files:
                outf = files[name]
            else:
                pathname = os.path.join(datadir,
                                        '{0}_{1}.mpk'.format(name, suffix))
                outf = open(pathname, 'wb')
                files[name] = outf
            parts = [secs, usecs, data]
            buf = msgpack.packb(parts)
            try:
                outf.write(buf)
            except IOError as e:
                logger.critical('Cannot write data record: %r', e)
            rd.hset('sensors.last', name, buf)
    finally:
        for name, f in files.items():
            f.close()
        logger.info('Archiver exiting')
        return datadir


def split_message(msg):
    """
    Split a message from the *events.mmp* channel into an event string
    and a dictionary of attributes.

    >>> event, attrs = split_message('profile:start t=123456789 pnum=42 mode=up')
    >>> event
    'profile:start'
    >>> attrs['pnum']
    '42'
    """
    try:
        event, args = msg.split(' ', 1)
    except ValueError:
        event = msg
        attrs = {}
    else:
        attrs = dict([s.split('=', 1) for s in args.split()])
    return event, attrs


def start_archiver(rd, topdir, t_start=None, seq=0, prefix='dataset_'):
    if t_start is None:
        t_start = time.time()
    suffix = '_{0:d}'.format(seq)
    tstamp = time.strftime('%Y%m%dT%H%M%S', time.gmtime(t_start))
    datadir = os.path.join(topdir,
                           prefix + tstamp + suffix)
    if not os.path.isdir(datadir):
        os.makedirs(datadir)
    msg = 'archive:open dir={0}'.format(datadir)
    rd.publish('events.archive', msg)
    queue = DataQueue(rd, 'archive:queue')
    logging.info('Starting archiver thread')
    return gevent.spawn(archive_data, rd, datadir, queue, tstamp + suffix)


def stop_archiver(rd, task):
    logging.info('Stopping archiver thread')
    task.kill(timeout=5)
    datadir = task.value or ''
    rd.publish('events.archive', 'archive:close dir={0}'.format(datadir))
    rd.delete('archive:queue')


def mainloop(rd, topdir):
    """
    Listen for *profile:start* messages on the *events.mmp* Redis
    channel and start/stop archiver threads.
    """
    pubsub = rd.pubsub()
    pubsub.subscribe('events.mmp')
    reader = None
    inprofile = False
    logging.info('Waiting for a message')
    try:
        for msg in pubsub.listen():
            if msg['type'] == 'message':
                event, attrs = split_message(msg['data'])
                if event == 'profile:start':
                    if inprofile:
                        stop_archiver(rd, reader)
                    logging.info('Starting profile: %r', attrs)
                    reader = start_archiver(rd, topdir,
                                            seq=int(attrs.get('pnum', 0)),
                                            t_start=int(attrs.get('t', 0)))
                    inprofile = True
    finally:
        if inprofile:
            logging.info('Profile interrupted')
            if reader:
                stop_archiver(rd, reader)


def main():
    if os.environ.get('LOGDEBUG'):
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    rd = redis.StrictRedis()
    mainloop(rd, sys.argv[1])


if __name__ == '__main__':
    rval = 0
    try:
        main()
    except KeyboardInterrupt:
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Unhandled exception')
        rval = 1
    sys.exit(rval)
