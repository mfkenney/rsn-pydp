#!/usr/bin/env python
"""
Net-snmp pass-through interface for Deep Profiler status information. This
program is executed by ``snmpd`` and supports the protocol discussed on the
`snmpd manual page`_ .

.. _snmpd manual page: http://www.net-snmp.org/docs/man/snmpd.conf.html#lbBB

The diagram below illustrates the MIB subtree for the status information::

    +--rsnDp(1013)
       +-- -R-- String    version(1)
       |        Textual Convention: DisplayString
       |        Size: 0..255
       |
       +--dock(2)
       |  |
       |  +-- -R-- Gauge     v12Current(1)
       |  +-- -R-- Gauge     ambientTemp(2)
       |  +-- -R-- Gauge     heatsinkTemp(3)
       |  +-- -R-- Gauge     relHumidity(4)
       |  +-- -R-- Integer32 ipsStatus(5)
       |
       +--profiler(3)
          |
          +-- -R-- Counter   profile(1)
          +-- -R-- Gauge     depth(2)
          +-- -R-- String    mode(3)
          |        Textual Convention: DisplayString
          |        Size: 0..255
          +-- -R-- Integer32 statusTime(4)
          +-- -R-- Gauge     batCapacity(5)

"""
from pyasn1.type import univ
from pydp.dio import Ts4800LcdDio
from pydp.adc import TsAdc
from functools import partial
import redis
import msgpack
import sys

# Interface version
INTF_VERSION = '1.1'

OID_ROOT = univ.ObjectIdentifier('1.3.6.1.4.1.8072.9999.9999')
OID_RSNDP = OID_ROOT + univ.ObjectIdentifier('1013')
OID_DOCK = OID_RSNDP + univ.ObjectIdentifier('2')
OID_PROFILER = OID_RSNDP + univ.ObjectIdentifier('3')
OID_SELECTOR = univ.ObjectIdentifier('0')


def next_line(f=sys.stdin):
    return f.readline().strip()


def output(*args):
    for line in args:
        sys.stdout.write(line + '\n')
        sys.stdout.flush()


def profiler_status(key='sensors.last', name='dpc_1'):
    """
    Generator to return the latest DPC status record.
    """
    rd = redis.StrictRedis()
    while True:
        value = rd.hget(key, name)
        if value:
            yield msgpack.unpackb(value)
        else:
            yield None


def tovolts(c):
    """
    Convert ADC counts to volts.
    """
    return c * 10.24 / 32768.


def ips_status(ioport):
    """
    Return the IPS status bits as an Integer32
    """
    return 'integer', int(ioport) & 0xff


def v12_current(adc):
    """
    Return the 12v current as an Integer32.
    """
    x = tovolts(adc[6])
    return 'gauge', int(x * 1000)


def humidity(adc):
    """
    Return the humidity as an Integer32.
    """
    x = tovolts(adc[5])
    return 'gauge', int(x * 48.38 - 23.82)


def temperature(adc, channel):
    """
    Return the internal temperature as an Integer32.
    """
    x = tovolts(adc[channel])
    return 'gauge', int(1000. * (x * 36.71 - 6.02))


def profiler_variable(vname, source):
    """
    Return a variable from the profiler status record.
    """
    rtypes = {
        'time': 'integer',
        'profile': 'counter',
        'state': 'string',
        'pressure': 'gauge',
        'energy': 'gauge'
    }
    rval = 0
    try:
        secs, usecs, data = source.next()
        if vname == 'time':
            rval = secs
        else:
            rval = data[vname]
    except (TypeError, KeyError):
        pass
    return rtypes[vname], rval


def version():
    return 'string', INTF_VERSION


def load_table():
    adc = TsAdc(channels=(3, 4, 5, 6))
    iop = Ts4800LcdDio()
    psource = profiler_status()
    table = {
        univ.ObjectIdentifier('1.0'): version,
        univ.ObjectIdentifier('2.1.0'): partial(v12_current, adc),
        univ.ObjectIdentifier('2.2.0'): partial(temperature, adc, 4),
        univ.ObjectIdentifier('2.3.0'): partial(temperature, adc, 3),
        univ.ObjectIdentifier('2.4.0'): partial(humidity, adc),
        univ.ObjectIdentifier('2.5.0'): partial(ips_status, iop),
        univ.ObjectIdentifier('3.1.0'): partial(profiler_variable,
                                                'profile', psource),
        univ.ObjectIdentifier('3.2.0'): partial(profiler_variable,
                                                'pressure', psource),
        univ.ObjectIdentifier('3.3.0'): partial(profiler_variable,
                                                'state', psource),
        univ.ObjectIdentifier('3.4.0'): partial(profiler_variable,
                                                'time', psource),
        univ.ObjectIdentifier('3.5.0'): partial(profiler_variable,
                                                'energy', psource)
    }
    return table


def main():
    table = load_table()
    n_trim = len(OID_RSNDP)
    while True:
        req = next_line()
        if not req:
            break
        if req == 'PING':
            output('PONG')
        elif req == 'get':
            oid = univ.ObjectIdentifier(next_line())
            try:
                func = table[oid[n_trim:]]
                dtype, value = func()
                output(str(oid), dtype, str(value))
            except KeyError:
                output('NONE')
        elif req == 'set':
            next_line()
            next_line()
            output('not-writable')
        elif req == 'getnext':
            next_line()
            output('NONE')


if __name__ == '__main__':
    main()
