#!/usr/bin/env python
#
# MMP Manager process
#
import sys
import gevent
import gevent.queue
from gevent import monkey
monkey.patch_all()
import redis
import logging
import msgpack
import time
import string
import os
import signal
import atexit
from base64 import b64encode, b64decode
import gevent_subprocess as subprocess
from functools import partial
from pydp.imm2 import IMM, IMMError
from pydp.dio import Ts4200LcdDio
from pydp.serialcom import Port
from pydp.power import PowerSwitch
from pydp.messaging import line_captured, line_released
from pydp import timer
from pydp.messages import SensorMessage, CtdMessage, OptodeMessage,\
    FlntuMessage, FlcdMessage, StatusMessage, DbMessage, AcmMessage,\
    ChargingMessage
from pydp.adc import Ts4200Adc


SYS_REBOOT = 'sudo /sbin/shutdown -r now'


class TaskWatchdog(object):
    """
    Provide watchdog functionality for a Greenlet task. Restart the task
    if it stops due to an exception.
    """
    restart_delay = 3

    def __init__(self, func, *args, **kwds):
        self.func = func
        self.args = args
        self.kwds = kwds
        self.task = gevent.Greenlet(func, *args, **kwds)
        self.task.link_exception(self)
        self.task.start()

    def __call__(self, greenlet):
        logging.exception(greenlet.exception)
        gevent.sleep(self.restart_delay)
        self.task = gevent.Greenlet(self.func, *self.args, **self.kwds)
        self.task.link_exception(self)
        self.task.start()
        return self.task

    def __getattr__(self, name):
        return getattr(self.task, name)


def power_summary(rd, key='battery_status'):
    """
    Return a summary of the current battery status as a dictionary
    with the following keys::

        time:: sample time-stamp
        voltage:: average battery voltage
        current:: total current
        energy:: remaining energy capacity
        rel_charge:: relative charge of the battery pack
    """
    return rd.hgetall(key)


def get_rel_charge(rd):
    """
    Return a tuple of the relative charge of the battery pack
    and the time since the last battery sample.
    """
    try:
        d = power_summary(rd)
        rc = int(d['rel_charge'])
        t = int(d['t'])
    except Exception:
        rc, t = 0, 0
    if rc > 100:
        rc = -1
    return rc, int(time.time()) - t


def msg_timestamp(secs, usecs):
    return long(secs * 1000000 + usecs)


def profiler_monitor(redis_connect, out_queue, channel='data.sensors.mmp_1'):
    """
    Monitor a Redis pub/sub channel for MMP data messages. Combine this data
    with battery status information to form a Status Message and put this
    message in the outgoing IMM message queue.
    """
    def _tovolts(c):
        return c * 10.24 / 32768.
    states = {
        'up': 'PROFILE_UP',
        'down': 'PROFILE_DOWN',
        'stationary': 'STOPPED',
        'docking': 'DOCKING'
    }
    adc = Ts4200Adc(channels=(3, 4, 5, 6))
    rd = redis_connect()
    pubsub = rd.pubsub()
    pubsub.subscribe(channel)
    for msg in pubsub.listen():
        if msg['type'] == 'message':
            secs, usecs, data = msgpack.unpackb(msg['data'])
            bdata = power_summary(rd)
            humidity = _tovolts(adc[3]) * 48.38 - 23.82
            temps = [_tovolts(adc[i]) * 36.71 - 6.02 for i in (4, 5, 6)]
            sm = StatusMessage(time=msg_timestamp(secs, usecs),
                               pressure=int(data['pressure'] * 1000),
                               state=states[data['mode']],
                               profile=data['pnum'],
                               voltage=int(bdata['voltage']),
                               current=int(bdata['current']),
                               motor_current=int(data['current']),
                               rel_charge=int(bdata['rel_charge']),
                               energy=int(bdata['energy']),
                               humidity=int(humidity * 10),
                               itemp=[int(t * 10) for t in temps])
            out_queue.put((b'S', b64encode(sm.serialize())))


def charging_monitor(redis_connect, out_queue, max_charge_key,
                     channel='data.power'):
    """
    Monitor a Redis pub/sub channel for battery status messages to track the
    progress of the battery charging operation. Returns when the relative
    battery charge exceeds *max_charge*.
    """
    rd = redis_connect()
    pubsub = rd.pubsub()
    pubsub.subscribe(channel)
    for msg in pubsub.listen():
        if msg['type'] == 'message':
            secs, usecs, data = msgpack.unpackb(msg['data'])
            current = []
            csum = 0.
            esum = 0.
            n = 0
            for rec in data:
                current.append(int(rec['current'] * 1000))
                esum += rec['energy']
                if 0 <= rec['rel_charge'] <= 100:
                    csum += rec['rel_charge']
                    n += 1
            rel_charge = csum / n
            cm = ChargingMessage(time=msg_timestamp(secs, usecs),
                                 current=current,
                                 rel_charge=int(rel_charge),
                                 energy=int(esum * 1000))
            power = rd.hgetall('power')
            max_charge = int(power.get(max_charge_key, 95))
            if out_queue:
                out_queue.put((b'C', b64encode(cm.serialize())))
            if max_charge <= rel_charge <= 100:
                return rel_charge


def build_sensor_message(rd, sensors, key='sensors.last'):
    """
    Read the most recent sample from the listed sensors and
    combine them into a single :class:`pydp.messages.SensorMessage`.
    """
    messages = {}
    for sensor in sensors:
        name, idx = sensor.split('_')
        # Unpack the Message Pack encoded timestamp and
        # data record. Non-existant samples are ignored.
        try:
            klass = (globals())[name.capitalize() + 'Message']
            secs, usecs, data = msgpack.unpackb(rd.hget(key, sensor))
        except (KeyError, ValueError, TypeError):
            pass
        else:
            t = msg_timestamp(secs, usecs)
            messages[name] = klass(time=t, **data)
    return SensorMessage(**messages)


def sensor_data(redis_connect, out_queue, interval=10, key='sensors.last',
                sensors=None):
    """
    Add periodic sensor data messages to the outgoing message queue.
    """
    rd = redis_connect()
    logger = logging.getLogger('sensors')
    sensors = sensors or []
    for t in timer(interval, fsleep=gevent.sleep):
        try:
            msg = build_sensor_message(rd, sensors, key=key)
        except Exception:
            logger.exception('Cannot create message')
        else:
            out_queue.put((b'D', b64encode(msg.serialize())))


def db_update(rd, key, value):
    """
    Update a string, hash, or list value in the Redis database.

    >>> rd = redis.StrictRedis(db=2)
    >>> rd.flushdb()
    True
    >>> db_update(rd, ('foo',), ['bar'])
    >>> rd.get('foo')
    'bar'
    >>> db_update(rd, ('myhash', 'mykey'), ['myvalue'])
    >>> rd.hget('myhash', 'mykey')
    'myvalue'
    >>> db_update(rd, ('mylist',), ['v1', 'v2', 'v3'])
    >>> db_update(rd, ('+mylist',), ['v4', 'v5'])
    >>> rd.llen('mylist')
    5
    >>> rd.lrange('mylist', 0, -1)
    ['v1', 'v2', 'v3', 'v4', 'v5']
    >>> db_update(rd, ('+mylist',), ['v6'])
    >>> rd.lrange('mylist', 0, -1)
    ['v1', 'v2', 'v3', 'v4', 'v5', 'v6']
    >>> db_update(rd, ('mylist', '2'), ['42'])
    >>> rd.lindex('mylist', 2)
    '42'
    >>> db_update(rd, ('foo',), ['baz'])
    >>> rd.get('foo')
    'baz'
    >>> rd.flushdb()
    True
    """
    if len(key) == 2:
        if key[1] in string.digits:
            rd.lset(key[0], int(key[1]), value[0])
        else:
            rd.hset(key[0], key[1], value[0])
    elif key[0].startswith('+'):
        # Append to a list
        k = key[0][1:]
        for v in value:
            rd.rpush(k, v)
    elif len(value) > 1:
        # New list
        k = key[0]
        rd.delete(k)
        for v in value:
            rd.rpush(k, v)
    else:
        rd.set(key[0], value[0])


def db_query(rd, key):
    """
    Lookup the value of a key from the Redis database.

    >>> rd = redis.StrictRedis(db=2)
    >>> rd.flushdb()
    True
    >>> rd.set('foo', 'bar')
    True
    >>> db_query(rd, ('foo',))
    ['bar']
    >>> db_query(rd, ('foo', '0'))
    []
    >>> rd.rpush('mylist', '1')
    1L
    >>> rd.rpush('mylist', '2')
    2L
    >>> db_query(rd, ('mylist', '1'))
    ['2']
    >>> db_query(rd, ('mylist',))
    ['1', '2']
    >>> rd.hset('myhash', 'mykey', 'myval')
    1L
    >>> db_query(rd, ('myhash', 'mykey'))
    ['myval']
    >>> rd.flushdb()
    True
    """
    value = []
    try:
        if len(key) == 2:
            if key[1] in string.digits:
                value = [rd.lindex(key[0], int(key[1]))]
            else:
                value = [rd.hget(key[0], key[1])]
        else:
            dtype = rd.type(key[0])
            if dtype == 'list':
                value = rd.lrange(key[0], 0, -1)
            elif dtype == 'string':
                value = [rd.get(key[0])]
    except redis.ResponseError:
        value = []
    return value


def message_handler(rd, text):
    """
    Process an incoming message from the DSC
    """
    reply = None
    if text.startswith('set '):
        # Database update
        _, contents = text.strip().split(' ', 1)
        msg = DbMessage.deserialize(b64decode(str(contents))).todict()
        db_update(rd, msg['key'].split('.'), msg['value'])
        reply = 'OK'
    elif text.startswith('get '):
        # Database query
        _, key = text.strip().split(' ', 1)
        value = db_query(rd, key.split('.'))
        msg = DbMessage(key=key, value=value)
        reply = 'E:' + b64encode(msg.serialize())
    elif text.startswith('go'):
        # Start the profiler operation
        rd.rpush('mmp:commands', 'start')
        reply = 'OK'
    elif text.startswith('dock'):
        rd.rpush('mmp:messages', 'CTLCHG')
        reply = 'OK'
    elif text.startswith('continue'):
        rd.rpush('mmp:messages', 'CTLCNT')
        reply = 'OK'
    elif text.startswith('use_schedule '):
        _, contents = text.strip().split(' ', 1)
        rd.rpush('mmp:messages', 'XMTSCH ' + contents)
        reply = 'OK'
    elif text.startswith('stop'):
        # Stop the profiler operation
        rd.rpush('mmp:commands', 'stop')
        reply = 'OK'
    elif text.startswith('reboot'):
        logging.critical('System reboot requested')
        try:
            cmd, secs = text.strip().split()
        except (TypeError, ValueError):
            secs = 10
        gevent.spawn_later(int(secs), subprocess.call, SYS_REBOOT.split())
        reply = 'OK'
    return reply


def imm_monitor(imm, timeout):
    with line_released(imm, force=True):
        while True:
            t_recv, line = imm.readline(timeout)
            if line is None:
                break
            elif len(line):
                text = str(line).strip()
                # Occassionally, we get echoed-back contents of the
                # last sent message followed by two \r's, followed
                # by the text from the DSC
                yield t_recv, text.split('\r\r')[-1]


def message_receiver(imm, dispatcher, timeout=None):
    """
    Task to receive messages from the DSC.

    :param imm: IMM instance
    :param dispatcher: callable to process messages
    :param timeout: maximum number of seconds to wait, ``None`` means
                    wait forever.
    """
    logger = logging.getLogger('imm_recv')
    logger.info('Starting')
    for t_recv, line in imm_monitor(imm, timeout):
        logger.info('RECV %r', line)
        try:
            reply = dispatcher(line)
        except Exception:
            logger.exception('Bad command: (%r)', line)
            reply = 'ERROR: bad command'
        if reply is not None:
            logger.info('Sending reply: %r', reply)
            try:
                imm.send(reply, 5)
            except IMMError:
                logger.exception('IMM host response error')
    logger.info('Timeout waiting for IMM message')


def message_sender(imm, in_queue, peer_sn, dispatcher):
    """
    Task to route messages from the Redis message queue to the IMM
    """
    logger = logging.getLogger('imm_send')
    with line_captured(imm, force=True):
        logger.info('Starting')
        while True:
            try:
                mtype, contents = in_queue.get(timeout=90)
            except gevent.queue.Empty:
                # Send some harmless command to keep the IMM from
                # going to sleep.
                try:
                    imm.command('getlinestatus', 4)
                except Exception:
                    logger.exception('IMM I/O error')
            else:
                if mtype == 'C' and peer_sn:
                    msg = b'#S{0}:{1}:{2}'.format(peer_sn, mtype, contents)
                    timeout = 15
                else:
                    msg = b'#G0:{0}:{1}'.format(mtype, contents)
                    timeout = 5
                try:
                    try:
                        _, resp = imm.command(msg, timeout)
                    except Exception:
                        logger.exception('IMM I/O error')
                    else:
                        if mtype == 'C' and peer_sn:
                            try:
                                elem = resp[0]
                                if elem and elem.text:
                                    dispatcher(elem.text.strip())
                            except Exception:
                                logger.exception('Malformed response: %r',
                                                 resp)
                finally:
                    in_queue.task_done()


def split_message(msg):
    """
    Split a message from the *events.mmp* channel into an event string
    and a dictionary of attributes.

    >>> event, attrs = split_message('profile:start t=123456789 pnum=42 mode=up')
    >>> event
    'profile:start'
    >>> attrs['pnum']
    '42'
    """
    try:
        event, args = msg.split(' ', 1)
    except ValueError:
        event = msg
        attrs = {}
    else:
        attrs = dict([s.split('=', 1) for s in args.split()])
    return event, attrs


def queue_events(rd, channel, queue):
    """
    Queue event messages from the specified channel.
    """
    try:
        pubsub = rd.pubsub()
        pubsub.subscribe('events.mmp')
        for msg in pubsub.listen():
            if msg['type'] == 'message':
                try:
                    event, attrs = split_message(msg['data'])
                except Exception:
                    logging.exception('Cannot parse message: %r', msg['data'])
                else:
                    queue.put((event, attrs))
    finally:
        queue.put(StopIteration)


def charging_done(rd, task):
    """
    Queue a message to request that the MPC leave the dock.
    """
    if not isinstance(task.value, gevent.GreenletExit):
        logging.info('Charging complete (%r)', task.value)
        if not 'CTLCNT' in rd.lrange('mmp:messages', 0, -1):
            rd.rpush('mmp:messages', 'CTLCNT')


def mainloop(rd, imm, peer_sn=None):
    """
    Listen for *profile:start* and *profile:stop* messages on the
    *events.mmp* Redis channel.
    """
    event_queue = gevent.queue.Queue()
    event_task = gevent.spawn(queue_events, rd, 'events.mmp', event_queue)
    is_docked = False
    in_profile = False
    sender, chg_mon = None, None
    tasks = []
    # Start the IMM receiver task
    receiver = TaskWatchdog(message_receiver,
                            imm,
                            partial(message_handler, rd))
    sensors = ['ctd_1', 'optode_1', 'flntu_1', 'flcd_1']
    msg_queue = gevent.queue.JoinableQueue()
    try:
        for event, attrs in event_queue:
            if event == 'profile:start':
                if in_profile:
                    logging.warning('Profile already started')
                    continue
                logging.info('Profile start')
                rd.hset('events', 'profile:start', int(time.time()))
                in_profile = True
                is_docked = False
                if receiver:
                    receiver.kill(timeout=10)
                    logging.info('Message receiver shutdown')
                    receiver = None
                rel_charge, _ = get_rel_charge(rd)
                mode = attrs.get('mode', 'unknown')
                # Start the task to send messages to the DSC via
                # the IMM
                if sender is None:
                    sender = TaskWatchdog(message_sender,
                                          imm, msg_queue,
                                          peer_sn,
                                          partial(message_handler, rd))
                out_msg = 'pstart t={0} pnum={1} bat={2:d} mode={3}'.format(
                    attrs.get('t'), attrs.get('pnum'), rel_charge, mode
                )
                # Add a profile start message to the outgoing queue
                msg_queue.put((b'I', out_msg))
                if mode != 'docking':
                    # Kill the charging monitor if it's running.
                    if chg_mon and not chg_mon.ready():
                        logging.info('Stop charging monitor')
                        chg_mon.kill(timeout=5)
                    # Start a task to add sensor data messages to the
                    # outgoing queue.
                    tasks.append(gevent.spawn(sensor_data,
                                              redis.StrictRedis,
                                              msg_queue,
                                              interval=10,
                                              sensors=sensors))
                    # Send the ACM data separately since it has a large
                    # data record
                    tasks.append(gevent.spawn(sensor_data,
                                              redis.StrictRedis,
                                              msg_queue,
                                              interval=21,
                                              sensors=['acm_1']))

                # Send MPC status messages
                tasks.append(gevent.spawn(profiler_monitor,
                                          redis.StrictRedis,
                                          msg_queue))
            elif event == 'profile:end':
                if not in_profile:
                    logging.warning('Profile not started')
                    continue
                logging.info('Profile end')
                rd.hset('events', 'profile:end', int(time.time()))
                in_profile = False
                # Queue a message to set the MPC clock. We put the message
                # at the head of the list so it will be sent first.
                rd.lpush('mmp:messages', 'CTLCLK')
                rel_charge, age = get_rel_charge(rd)
                status = attrs.get('status', '-1')
                # Shutdown any running tasks
                for task in tasks:
                    task.kill(timeout=4)
                tasks = []

                if sender:
                    out_msg = 'pend t={0} pnum={1} bat={2:d} stat={3}'.format(
                        attrs['t'], attrs['pnum'], rel_charge, status
                    )
                    # Add a profile end message to the queue and wait
                    # until it is sent before halting the sender.
                    msg_queue.put((b'I', out_msg))
                    if not msg_queue.empty():
                        msg_queue.join()
                    sender.kill(timeout=10)
                    logging.info('Message sender shutdown')
                    sender = None

                # If we are docked, insure that the message-sender is
                # running and start the charging monitor. Otherwise,
                # exit the loop.
                if is_docked:
                    if sender is None:
                        sender = TaskWatchdog(message_sender,
                                              imm,
                                              msg_queue,
                                              peer_sn,
                                              partial(message_handler, rd))
                    if chg_mon is None or chg_mon.ready():
                        chg_mon = gevent.spawn(charging_monitor,
                                               redis.StrictRedis,
                                               msg_queue,
                                               'max_charge')
                        chg_mon.link_value(partial(charging_done, rd))
                else:
                    break
            elif event == 'stalled':
                logging.info('Profiler stalled')
                if sender:
                    rel_charge, _ = get_rel_charge(rd)
                    out_msg = 'stalled t={0} pr={1} iter={2} bat={3:d}'.format(
                        attrs['t'], attrs['pr'], attrs['iter'], rel_charge
                    )
                    msg_queue.put((b'I', out_msg))
            elif event == 'docked':
                logging.info('Profiler docked')
                rd.hset('events', 'docked', int(time.time()))
                is_docked = True
                if sender:
                    rel_charge, _ = get_rel_charge(rd)
                    out_msg = 'docked t={0} pr={1} bat={2:d}'.format(
                        attrs['t'], attrs['pr'], rel_charge
                    )
                    # Send it twice to increase the chances that it gets through
                    msg_queue.put((b'I', out_msg))
                    msg_queue.put((b'I', out_msg))
                else:
                    logging.warning('Unexpected message-sender shutdown')
            else:
                if sender:
                    out_msg = [event] + ['{0}={1}'.format(k, v)
                                         for k, v in attrs.items()]
                    msg_queue.put((b'I', ' '.join(out_msg)))
                else:
                    logging.warning('Unexpected message: %r', event)
    finally:
        logging.info('Halting tasks')
        # Shutdown any running tasks
        for task in tasks:
            task.kill(timeout=4)
        if sender:
            sender.kill(timeout=10)
        event_task.kill(timeout=4)
        if receiver is not None:
            receiver.kill(timeout=10)
        if chg_mon and not chg_mon.ready():
            chg_mon.kill(timeout=4)


def cleanup(sw):
    logging.info('Powering down IMM')
    sw.off()


def catch_signal(*args):
    sys.exit(0)


def main():
    if os.environ.get('LOGDEBUG'):
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    for s in (signal.SIGTERM, signal.SIGQUIT):
        gevent.signal(s, catch_signal)
    rd = redis.StrictRedis()
    cfg = rd.hgetall('imm_cfg')
    settings = rd.hgetall('imm_settings')
    dioport = Ts4200LcdDio()
    sw = PowerSwitch(cfg['switch'], dioport)
    port = Port(cfg['device'], baud=int(cfg['baud']))
    sw.on()
    atexit.register(cleanup, sw)
    gevent.sleep(float(cfg.get('warmup', 0)))
    port.open()
    with IMM(port) as imm:
        imm.wakeup()
        for k, v in settings.items():
            imm.set(k, int(v))
            imm.sleep()
            while True:
                mainloop(rd, imm, peer_sn=cfg.get('peer_sn'))

if __name__ == '__main__':
    rval = 0
    try:
        main()
    except Exception:
        logging.exception('Unhandled exception')
        rval = 1
    sys.exit(rval)
