#!/usr/bin/env python
#
# DPC Battery Monitor process
#
import sys
import smbus
import time
import redis
import msgpack
from decimal import Decimal
from pydp import sync_timer as timer
from pydp.battery import Battery, Mux, median_value


def encode_decimal(obj):
    if isinstance(obj, Decimal):
        return float(obj)
    return obj


def timestamp(t=None):
    t = t or time.time()
    secs = int(t)
    return (secs, int((t - secs) * 1000000))


# Data display conversion functions
def cvt_voltage(mv):
    return Decimal(mv) / Decimal(1000)


def cvt_current(ma):
    # The two-byte value returned for the current readings must be
    # interpreted as a signed integer.
    if ma & 0x8000:
        ma -= 65536
    return Decimal(ma) / Decimal(1000)


def cvt_capacity(tmwh):
    return Decimal(tmwh) / Decimal(100)


def cvt_temp(k):
    return Decimal(k) / Decimal(10) - Decimal('273.15')


VARIABLES = [
    ('s/n', 'SerialNumber', None),
    ('voltage', 'Voltage', cvt_voltage),
    ('current', 'Current', cvt_current),
    ('avg_current', 'AverageCurrent', cvt_current),
    ('chg_current', 'ChargingCurrent', cvt_current),
    ('temperature', 'Temperature', cvt_temp),
    ('max_error', 'MaxError', None),
    ('rel_charge', 'RelativeStateOfCharge', None),
    ('abs_charge', 'AbsoluteStateOfCharge', None),
    ('energy', 'RemainingCapacity', cvt_capacity),
    ('full_charge', 'FullChargeCapacity', cvt_capacity),
    ('time_remaining', 'RunTimeToEmpty', None),
    ('avg_time_remaining', 'AverageTimeToEmpty', None),
    ('avg_time_to_full', 'AverageTimeToFull', None),
    ('status', 'BatteryStatus', hex),
    ('pack_status', 'PackStatus', hex),
    ('cycles', 'CycleCount', None)
]


def collect(b, bvars, n=5):
    """
    Collect the data for a Battery.
    """
    rec = [('address', b.address)]
    # Re-initialize the battery's mode setting on a
    # periodic basis.
    if (time.time() - b.t_init) > 45:
        b._initialize()
    for key, method, cvt in bvars:
        func = getattr(b, method)
        try:
            value = median_value(func, n=n)
            if cvt:
                rec.append((key, cvt(value)))
            else:
                rec.append((key, value))
        except IOError:
            rec.append((key, None))
    return rec


def get_subdict(d, keys):
    """
    Return a subset of a dictionary if and only if all of
    the values are non-None.
    """
    d_sub = {}
    for k in keys:
        if k in d and d[k] is not None:
            d_sub[k] = d[k]
        else:
            return None
    return d_sub


def sample_all(t_start, rd, batteries, bvars):
    """
    Sample all batteries and store the result in a Redis data-store
    """
    summary_vars = ('voltage', 'avg_current', 'energy', 'rel_charge',
                    'temperature')
    records = []
    t0 = timestamp(t=t_start)
    vsum = 0.
    isum = 0.
    esum = 0.
    csum = 0.
    tsum = 0.
    n = 0
    for b in batteries:
        try:
            rec = dict(collect(b, bvars))
        except IOError:
            sys.stderr.write(
                'Cannot read battery at 0x{:04x}\n'.format(
                    b.address))
        else:
            sub_rec = get_subdict(rec, summary_vars)
            if sub_rec:
                vsum += float(rec['voltage'])
                isum += float(rec['avg_current'])
                esum += float(rec['energy'])
                csum += float(rec['rel_charge'])
                tsum += float(rec['temperature'])
                n += 1
            records.append(rec)
    buf = msgpack.packb([t0[0], t0[1], records], default=encode_decimal)
    rd.set('batteries.last', buf)
    rd.publish('data.power', buf)
    db_key = 'battery_status'
    if n > 0:
        pipe = rd.pipeline(transaction=False)
        pipe.hset(db_key, 't', str(t0[0]))
        pipe.hset(db_key, 'count', str(n))
        pipe.hset(db_key, 'temp', str(int(tsum * 1000 / n)))
        pipe.hset(db_key, 'voltage', str(int(vsum * 1000 / n)))
        pipe.hset(db_key, 'current', str(int(isum * 1000)))
        pipe.hset(db_key, 'energy', str(int(esum * 1000)))
        pipe.hset(db_key, 'rel_charge', str(int(csum / n)))
        pipe.execute()


def main():
    rd = redis.StrictRedis()
    # Fetch the list of battery addresses
    addrs = [divmod(int(a), 256) for a in rd.lrange('batteries', 0, -1)]
    # Fetch the run-time configuration
    cfg = rd.hgetall('batteries_cfg')

    bus = smbus.SMBus(0)
    # Bit of a hack here. Create an object for each battery mux in the
    # system to ensure that only one mux is active at any given time.
    # See Mux.__new__ for details.
    Mux(bus, 0x70)
    Mux(bus, 0x77)
    varnames = cfg['variables'].split()
    interval = int(cfg.get('interval', '60'))

    try:
        for tick in timer(interval):
            batteries = []
            for mux_addr, chan in addrs:
                b = Battery(bus, mux_addr, chan)
                try:
                    b._initialize()
                    batteries.append(b)
                except IOError:
                    sys.stderr.write(
                        'Cannot read battery at ({0:x}, {1})\n'.format(
                            mux_addr, chan))
            bvars = [v for v in VARIABLES if v[0] in varnames]
            sample_all(tick, rd, batteries, bvars)
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    main()
