#!/usr/bin/env python
"""
Monitor various DSC sensors and publish to a Redis channel.
"""
import sys
import redis
import simplejson as json
import argparse
from pydp.dio import Ts4800LcdDio
from pydp.adc import TsAdc
from pydp import timer
from functools import partial


def tovolts(c):
    """
    Convert ADC counts to volts.
    """
    return c * 10.24 / 32768.


def ips_status(ioport):
    """
    Return the IPS status bits as an integer
    """
    return int(ioport) & 0xff


def v12_current(adc):
    """
    Return the 12v current as an integer.
    """
    x = tovolts(adc[6])
    return int(x * 1000)


def humidity(adc):
    """
    Return the humidity as an integer.
    """
    x = tovolts(adc[5])
    return int(x * 48.38 - 23.82)


def temperature(adc, channel):
    """
    Return the internal temperature as an integer.
    """
    x = tovolts(adc[channel])
    return int(1000. * (x * 36.71 - 6.02))


def data_source(interval):
    adc = TsAdc(channels=(3, 4, 5, 6))
    iop = Ts4800LcdDio()
    table = [
        ('v12', partial(v12_current, adc)),
        ('humidity', partial(humidity, adc)),
        ('T_ambient', partial(temperature, adc, 4)),
        ('T_heatsink', partial(temperature, adc, 3)),
        ('ips_status', partial(ips_status, iop))
    ]

    for t in timer(interval):
        rec = [(name, f()) for name, f in table]
        yield t, rec


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('interval', help='sample interval in seconds',
                        nargs='?',
                        type=int,
                        default=10)
    parser.add_argument('--channel', help='Redis channel for messages',
                        default='data.eng')
    args = parser.parse_args()
    rd = redis.StrictRedis()
    try:
        for t, rec in data_source(args.interval):
            secs = int(t)
            usecs = int((t - secs) * 1000000)
            rd.publish(args.channel,
                       json.dumps({'name': 'dock',
                                   't': [secs, usecs],
                                   'data': dict(rec)}))
    except KeyboardInterrupt:
        sys.stderr.write('Exiting ...\n')


if __name__ == '__main__':
    main()
