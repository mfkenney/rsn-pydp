#!/usr/bin/env python
#
import sys
import zmq
import simplejson as json
import time
import argparse
from pprint import pprint


def show_message(mtype, contents, outf=sys.stdout):
    t = contents['t'][0] + contents['t'][1] * 1.0e-6
    # Display IPS_STATUS value in hex
    if mtype == 'DATA' and contents['name'] == 'dock':
        contents['data']['ips_status'] = hex(contents['data']['ips_status'])
    outf.write(time.strftime('[%Y-%m-%d %H:%M:%S]',
                             time.gmtime(t)))
    outf.write(' {0} \n'.format(mtype))
    pprint(contents, stream=outf)


def monitor(*socks):
    poller = zmq.Poller()
    for sock in socks:
        poller.register(sock, zmq.POLLIN)
    while True:
        ready = dict(poller.poll())
        for sock in ready:
            mtype, contents = sock.recv_multipart()
            show_message(mtype, json.loads(contents))


def main():
    """
    Subscribe to Deep Profiler data streams.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('publishers', nargs='+')
    args = parser.parse_args()

    ctx = zmq.Context()
    socks = []
    for endpoint in args.publishers:
        sock = ctx.socket(zmq.SUB)
        sock.connect(endpoint)
        sock.setsockopt(zmq.SUBSCRIBE, b'')
        socks.append(sock)
    monitor(*socks)


if __name__ == '__main__':
    main()
