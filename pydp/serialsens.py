#!/usr/bin/env python
#
import serial
import os
import gevent
import gevent.socket
from gevent.queue import Queue
import time
from contextlib import contextmanager
from functools import partial


class TimeoutError(Exception):
    def __init__(self, command=''):
        self.command = command
        
    def __str__(self):
        if self.command:
            return 'Timeout sending command %r' % self.command
        else:
            return 'Timeout reading sensor'

  
class SensorError(Exception):
    def __init__(self, cmd):
        self.cmd = cmd
    def __str__(self):
        return 'Invalid command: %r' % self.cmd


class BaseSensor(object):
    """
    Common Sensor interface.
    """
    def __init__(self, device, baud=9600, **kwds):
        """
        Initialize the serial interface. *device* is a string naming the serial
        interface (e.g. ``/dev/ttyS0``) and *baud* is the integer baud rate.
        """
        self.sessionlog = None
        self.serial = serial.Serial()
        self.serial.port = device
        self.serial.baudrate = baud
        self.serial.timeout = 0
        
    def __repr__(self):
        return '%s(%r, baud=%r)' % (
                self.__class__.__name__,
                self.serial.port, self.serial.baudrate)
        
    def open(self, sessionlog=None):
        """
        Open the serial interface. If *sessionlog* is not ``None``, it must be a
        writeable, file-like object.
        """
        self.sessionlog = sessionlog
        self.serial.open()
        self.serial.nonblocking()
        if self.sessionlog:
            self.sessionlog.write('<session sensor="%s" dev="%s">\n' % (
                    self.__class__.__name__,
                    self.serial.port.encode('utf-8')))
        
    def close(self):
        """
        Close the serial interface.
        """
        self.serial.close()
        if self.sessionlog:
            self.sessionlog.write('</session>\n')
            self.sessionlog.flush()
       
    def send_break(self):
        """
        Send a serial BREAK "signal".
        """
        self.serial.sendBreak()
        if self.sessionlog:
            self.sessionlog.write('<break t="%.6f" />' % (time.time(),))
        
    @property
    def fd(self):
        """
        Return the OS-level file-descriptor for the serial interface.
        """
        return self.serial.fileno()


class BaseRecordParser(object):
    """
    Base class for parsers which parse the output of serial sensors which stream
    fixed format records rather than use a request-response protocol.
    
    Characters are passed to the parser via the :meth:`feed` method and timestamped
    data records are extracted by iterating over the parser instance.
    """
    def reset(self):
        self.records = Queue()
        self.count = 0
     
    def consume(self, c):
        raise NotImplemented()
    
    def feed(self, c):
        self.consume(c)
        
    def __len__(self):
        return self.count
    
    def __iter__(self):
        self.records.put(StopIteration)
        return self
    
    def next(self):
        rval = self.records.get()
        if rval is StopIteration:
            raise rval
        self.count -= 1
        return rval
    
    def close(self):
        return self.count


class FramedRecordParser(BaseRecordParser):
    """
    Parse data records framed by start and end bytes. Each data record is
    stored in the :class:`~gevent.queue.Queue` as a :func:`bytearray`.
    """
    def __init__(self, start, end=None):
        """
        Create a new parser instance. *start* is the starting character, a single
        element string. If *end* is ``None``, the end character is set equal to the
        start character.
        """
        self.start = start
        self.end = end or start
        if not (isinstance(self.start, bytes) or isinstance(self.start, bytearray)):
            raise TypeError('Byte-string expected')
        if not (isinstance(self.end, bytes) or isinstance(self.end, bytearray)):
            raise TypeError('Byte-string expected')
        self.reset()
        
    def reset(self):
        """
        Reset the parser to its initial state where it discards all input until
        it sees :attr:`start`.
        """
        super(FramedRecordParser, self).reset()
        self.consume = self._look_for_start
        self.timestamp = None
        self.recbuf = bytearray()
        
    def _look_for_start(self, c):
        if c == self.start:
            self.consume = self._look_for_end
            
    def _look_for_end(self, c):
        if c == self.end:
            # Handle the case where start-char == end-char
            if self.end != self.start:
                self.consume = self._look_for_start
            self.records.put((self.timestamp, self.recbuf))
            self.count += 1
            self.recbuf = bytearray()
            self.timestamp = None
        else:
            if self.timestamp is None:
                self.timestamp = time.time()
            self.recbuf.append(c)
            
    def close(self):
        """
        Flush any partial records and return the number of records queued.
        """
        if self.recbuf:
            self.records.put((self.timestamp, self.recbuf))
        return self.count


class DelimitedRecordParser(BaseRecordParser):
    """
    Parse data records delimited by a sequence of one or more bytes. Each data record
    is stored in the :class:`~gevent.queue.Queue` as a :func:`bytearray`.
    """
    def __init__(self, delim):
        """
        Create a new parser instance. *delim* is a sequence of one or more bytes (a byte
        string).
        """
        if not (isinstance(delim, bytearray) or isinstance(delim, bytes)):
            raise TypeError('Byte-string expected')
        self._delim = delim
        self._match = len(delim)
        self.reset()
        
    @property
    def delim(self):
        return self._delim
    
    @delim.setter
    def delim(self, value):
        self._delim = value
        self._match = len(value)
        
    def reset(self):
        """
        Reset the parser to its initial state.
        """
        super(DelimitedRecordParser, self).reset()
        self.recbuf = bytearray()
        self.timestamp = None
        
    def consume(self, c):
        if self.timestamp is None:
            self.timestamp = time.time()
        self.recbuf.append(c)
        if self.recbuf[-self._match:] == self._delim:
            self.records.put((self.timestamp, self.recbuf[:-self._match]))
            self.count += 1
            self.recbuf = bytearray()
            self.timestamp = None
            
    def close(self):
        """
        Flush any partial records and return the number of records queued.
        """
        if self.recbuf:
            self.records.put((self.timestamp, self.recbuf))
        return self.count

 
class SimpleSensor(BaseSensor):
    """
    Basic serial sensor interface. Send a command, get a response.
    """
    eol = b''
    prompt = b'>'
    error = None
    
    def __init__(self, *args, **kwds):
        super(SimpleSensor, self).__init__(*args, **kwds)
        if 'parser' in kwds:
            self.parser = kwds['parser']
        else:
            self.parser = DelimitedRecordParser(self.prompt)
    
    def send(self, command, timeout):
        """
        Send a command string to the attached sensor.
        
        *timeout* is the maximum number of seconds to wait for the serial interface
        to be ready. If the timeout expires, raise the :class:`TimeoutError` exception.
        """
        fd = self.fd
        self.serial.flushInput()
        if command is not None:
            buf = serial.to_bytes(command) + self.eol
        else:
            buf = ''
            self.send_break()
        if self.sessionlog:
            self.sessionlog.write(
                    '<send t="%.6f">%r</send>\n' % (time.time(), buf))
        total = len(buf)
        n = 0
        while n < total:
            gevent.socket.wait_write(fd, timeout=timeout,
                                     timeout_exc=TimeoutError(command))
            n += os.write(fd, buf[n:])
    
    def recv(self, timeout):
        """
        Read the output from the sensor until the next prompt.
        
        *timeout* is the maximum number of seconds to wait for the serial interface
        to be ready. If the timeout expires, raise the :class:`TimeoutError` exception,
        otherwise return a generator which will iterate over all of the records currently
        buffered. Each record is a tuple containing a timestamp (in seconds since the epoch)
        and a :func:`bytearray` of the output from the sensor.
        
        Typical usage:
        
            >>> for t, rec in s.recv(timeout):
            ...    pass
            >>> # t and rec contain the most recent data
        """
        fd = self.fd
        self.parser.reset()
        gevent.socket.wait_read(fd, timeout=timeout,
                                timeout_exc=TimeoutError())
        while True:
            if self.on_input():
                break
            gevent.socket.wait_read(fd, timeout=timeout,
                                    timeout_exc=TimeoutError())
        return self.results()
    
    def _read_stream(self, timeout, outq):
        """
        Task to run as a Greenlet to continuously read records from the sensor
        and write them to a :class:`Queue`.
        """
        fd = self.fd
        self.parser.reset()
        gevent.socket.wait_read(fd, timeout=timeout,
                                timeout_exc=TimeoutError())
        while True:
            if self.on_input():
                for rec in self.parser:
                    outq.put(rec)
            gevent.socket.wait_read(fd, timeout=timeout,
                                    timeout_exc=TimeoutError())
            
    def start_stream(self, timeout, q):
        """
        Start a task in a :class:`gevent.Greenlet` to read streaming data records
        and write them to the supplied :class:`gevent.queue.Queue`.
        """
        def _propagate_exception(q, greenlet):
            q.put((0, greenlet.exception))
        task = gevent.spawn(self._read_stream, timeout, q)
        task.link_exception(partial(_propagate_exception, q))
        return task
    
    def stop_stream(self, task, timeout=None):
        """
        Stop the stream reader task.
        """
        if task is not None:
            task.kill(timeout=timeout)
        
    @contextmanager
    def recv_stream(self, timeout, eor='\r\n'):
        """
        Switch the sensor interface into a streaming context where it continuously
        outputs timestamped data records to a :class:`Queue`.
        
        Typical usage:
        
            with s.recv_stream(timeout) as dataq:
                while some_condition():
                    tstamp, rec = dataq.get()
                    if isinstance(rec, TimeoutError):
                        break
        
        """
        defdelim = self.parser.delim
        self.parser.delim = eor
        q = Queue()
        task = None
        try:
            task = self.start_stream(timeout, q)
            yield q
        finally:
            self.stop_stream(task, timeout=timeout*2)
        self.parser.delim = defdelim
        
    def command(self, cmd, timeout):
        """
        Send a command and return the timestamped response.
        
        A call to this method is equivalent to a call to :meth:`send` followed by a call
        to :meth:`recv`.
        """
        t = 0
        resp = None
        self.send(cmd, timeout)
        for t, resp in self.recv(timeout):
            pass
        if self.error and self.error in resp:
            raise SensorError(cmd)
        return t, resp

    def results(self):
        """
        Iterator to return data records from the parser.
        """
        for rec in self.parser:
            yield rec
            
    def on_input(self):
        """
        Handle an input-ready event on the serial interface.
        
        Read as many characters as possible from the interface and return the number of
        data records available.
        """
        fd = self.serial.fileno()
        data = os.read(fd, 1024)
        for c in data:
            self.parser.feed(c)
        if self.sessionlog:
            self.sessionlog.write(
                '<recv t="%.6f">%r</recv>\n' % (time.time(), data))
        return len(self.parser) > 0

