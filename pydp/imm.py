#!/usr/bin/env python
#
# Interface to a Seabird Inductive Modem Module (IMM)
#
import serial
import os
import gevent
import gevent.socket
import time
from xml.etree import ElementTree as etree
from collections import deque
from string import printable


class BuildStanza(etree.TreeBuilder):
    """
    Extend :class:`~xml.etree.TreeBuilder` to track the XML tree depth so we can
    parse a stream of XML stanzas. Each stanza is at a depth of 1 under the top-level
    ``<stream>`` element. Also, add a *timestamp* attribute to each element.
    """
    depth = 0
    stanza_depth = 1
    def __init__(self, stanza_cb, text_cb, *args, **kwds):
        self.stanza_cb = stanza_cb
        self.text_cb = text_cb
        self.timestamps = deque()
        etree.TreeBuilder.__init__(self, *args, **kwds)
        
    def start(self, tag, attrs):
        self.depth += 1
        self.timestamps.append(time.time())
        return etree.TreeBuilder.start(self, tag, attrs)

    def data(self, data):
        etree.TreeBuilder.data(self, data)
        if self.depth == self.stanza_depth:
            self.text_cb(data)
            
    def end(self, tag):
        elem = etree.TreeBuilder.end(self, tag)
        elem.set('timestamp', '%.6f' % self.timestamps.pop())
        self.depth -= 1
        if self.depth == self.stanza_depth:
            self.stanza_cb(elem)
        return elem


class TimeoutError(Exception):
    def __init__(self, context=None):
        self.context = context
        
    def __str__(self):
        return 'TimeoutError(%r)' % self.context

class IMMError(Exception):
    def __init__(self, etype, msg):
        self.etype = etype
        self.msg = msg
        
    def __str__(self):
        return 'IMMError(%r, %r)' % (self.etype, self.msg)
        

class IMM(object):
    """
    Manage serial communications with a Seabird Inductive Modem Module (IMM).
    """
    eol = b'\r\n'
    end_tags = ('poweroff',)
    prompt = 'IMM>'
    prompt_len = 4
    
    def __init__(self, device, baud=9600, sessionlog=None):
        self.parser = None
        self.sessionlog = sessionlog
        self.serial = serial.Serial()
        self.serial.port = device
        self.serial.baudrate = baud
        self.serial.timeout = 0
        self.stanzas = deque()
        self.textbuf = bytearray()
        self.settings = {}
        self.awake = False
        
    def open(self):
        """
        Open the serial connection and initialize the XML parser.
        """
        self.serial.open()
        self.serial.nonblocking()
        self.parser = etree.XMLParser(target=BuildStanza(self._store_stanza,
                                                         self._store_text))
        self.parser.feed('<stream>')

    def wakeup(self):
        """
        Try to wakeup the device from sleep mode.
        
        First send a carriage return and wait for a response. If the wait times-out, the
        device was not asleep so we must send a linefeed to get back to the prompt.
        """
        self.serial.flushInput()
        os.write(self.fd, b'\r')
        try:
            self.recv(3)
        except TimeoutError:
            os.write(self.fd, b'\n')
            self.recv(3)
        self.awake = True

    def sleep(self):
        """
        Place the device in sleep mode. When the IMM is in sleep mode, a peer
        can send data directly to the host.
        """
        self.send('pwroff', 2)
        try:
            self.recv(4)
            self.awake = False
        except IMMError:
            self.awake = True
        return not self.awake
    shutdown = sleep
    
    @property
    def fd(self):
        return self.serial.fileno()
        
    def _store_stanza(self, elem):
        self.stanzas.append(elem)
        self.textbuf = bytearray()
    
    def _store_text(self, text):
        self.textbuf.extend(text)
        
    def _load_settings(self, force=False):
        if force or (not self.settings):
            results = self.command(b'getcd', 3)
            settings = results[0].getchildren()[0]
            self.settings = dict([(k.lower(), v) for k, v in settings.items()])
            
    def get(self, name):
        """
        Return the value of an IMM configuration setting.
        """
        self._load_settings()
        return self.settings[name.lower()]
     
    def set(self, name, value):
        """
        Update an IMM configuration setting.
        """
        self._load_settings()
        name = name.lower()
        if name not in self.settings:
            raise AttributeError
        value = str(value)
        cmd = b'set%s=%s' % (name, value)
        self.command(cmd, 3)
        self.settings[name] = value
        
    def results(self):
        """
        Returns a generator which iterates over the results of the most recent command
        and then clears the results buffer.
        """
        err = None
        for elem in self.stanzas:
            if elem.tag.lower() == 'error':
                err = IMMError(elem.get('type'), elem.get('msg'))
            for subs in elem.getiterator():
                if subs.tag.lower() == 'error':
                    err = IMMError(subs.get('type'), subs.get('msg'))
            yield elem
        self.stanzas.clear()
        if self.awake and not self.sessionlog:
            # If we're not logging, recreate the parser to limit memory consumption.
            self.parser.feed('</stream>')
            self.parser.close()
            self.parser = etree.XMLParser(target=BuildStanza(self._store_stanza,
                                                         self._store_text))
            self.parser.feed('<stream>')
        if err is not None:
            raise err
        
    def close(self):
        """
        Close the serial connection and XML parser. If session logging was requested, write the
        saved session to a file.
        """
        self.parser.feed('</stream>')
        root = self.parser.close()
        if self.sessionlog:
            self.sessionlog.write(etree.tostring(root))
            self.sessionlog.flush()
        self.parser = None
        self.stanzas.clear()
        self.serial.close()
    
    def parser_reset(self):
        """
        Close and recreate the parser. This method should be called periodically during
        a long running session to prevent runaway memory consumption.
        """
        self.close()
        self.open()
        
    def send(self, command, timeout, chunksize=4, delay=0.01):
        """
        Send a command string to the IMM.
        
        *timeout* is the maximum number of seconds to wait for the serial interface
        to be ready. If the timeout expires, raise the :class:`TimeoutError` exception.
        
        *chunksize* and *delay* are used to throttle the character send rate so as not
        to overflow the input buffer on the device. The default values should be
        sufficient.
        """
        fd = self.serial.fileno()
        self.serial.flushInput()
        buf = serial.to_bytes(command) + self.eol
        total = len(buf)
        start, end = 0, chunksize
        n = 0
        while n < total:
            gevent.socket.wait_write(fd, timeout=timeout, timeout_exc=TimeoutError(command))
            n += os.write(fd, buf[start:end])
            time.sleep(delay)
            start += chunksize
            end += chunksize

    def recv(self, timeout):
        """
        Read the output of the IMM until the <Executed/> element is seen. The return value
        is a list of XML stanzas. Each stanza is an :class:`xml.etree.Element` instance.
        *timeout* is the maximum number of seconds to wait for a result. If the timeout
        expires, raise a :class:`TimeoutError` exception. If an error is returned by the
        device, raise an :class:`IMMError` exception.
        """
        fd = self.fd
        gevent.socket.wait_read(fd, timeout=timeout, timeout_exc=TimeoutError('recv'))
        while True:
            status = self.on_input()
            if status == True:
                break
            gevent.socket.wait_read(fd, timeout=timeout, timeout_exc=TimeoutError('recv'))
        return list(self.results())

    def command(self, command, timeout, **kwds):
        """
        Send a command to the IMM and return the response. Equivalent to calling :meth:`send`
        followed by :meth:`recv`
        """
        self.send(command, timeout, **kwds)
        return self.recv(timeout)
        
    def readline(self, timeout):
        """
        Return a single line of data sent from a remote IMM.
        """
        if self.awake:
            self.sleep()
        fd = self.fd
        linebuf = bytearray()
        try:
            gevent.socket.wait_read(fd, timeout=timeout, timeout_exc=TimeoutError('readline'))
            while True:
                data = os.read(fd, 1024)
                for c in data:
                    linebuf.append(c)
                    if c == b'\n':
                        return linebuf
                gevent.socket.wait_read(fd, timeout=timeout, timeout_exc=TimeoutError('readline'))
        except TimeoutError:
            return linebuf
            
    def on_input(self, *args, **kwds):
        fd = self.serial.fileno()
        data = os.read(fd, 1024)
        rval = False
        # Feed all of the input into the XML parser until we see
        # the prompt or an "end tag". Note that parser.feed expects
        # a byte-string as input (i.e. an encoded string, not unicode).
        n = len(self.stanzas)
        for c in data:
            if c in printable:
                self.parser.feed(c)
            else:
                self.parser.feed(r'\x%02x' % ord(c))
            # Check for the prompt
            if self.textbuf[-self.prompt_len:] == self.prompt:
                self.textbuf = bytearray()
                rval = True
            # Short circuit the loop if we haven't added a new XML stanza
            if len(self.stanzas) == n:
                continue
            n = len(self.stanzas)
            try:
                elem = self.stanzas[-1]
                tag = elem.tag.lower()
                if tag in self.end_tags:
                    rval = True
            except IndexError:
                pass
        return rval

if __name__ == '__main__':
    import sys
    try:
        devname = sys.argv[1]
    except IndexError:
        sys.stderr.write('Usage: %s serialdev\n' % sys.argv[0])
        sys.exit(1)
    imm = IMM(devname, sessionlog=sys.stdout)
    imm.open()
    imm.wakeup()
    value = int(imm.get('tmodem4'))
    if value < 200:
        imm.set('tmodem4', 200)
    imm.shutdown()
    imm.close()
