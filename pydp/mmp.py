#!/usr/bin/env python
#
# Interface to the McLane Mooring Profiler (MMP)
#

CRC16_POLY = 0x1021


def update_crc16(crc, c):
    """
    Accumulate the 16-bit XMODEM (CCITT) CRC. *crc* is the current value
    of the CRC (16-bit integer) and *c* is the next data byte (8-bit integer).
    The return value is the new value of the CRC.

    >>> # data is an array of bytes
    >>> crc = reduce(update_crc, data, 0)
    """
    crc = crc ^ (c << 8)
    for i in range(8):
        if (crc & 0x8000):
            crc = (crc << 1) ^ CRC16_POLY
        else:
            crc = crc << 1
        crc = crc & 0xffff
    return crc


class CRCError(Exception):
    def __init__(self, context=None):
        self.context = context

    def __str__(self):
        return 'CRCError(%r)' % self.context


class InvalidState(Exception):
    def __init__(self, state):
        self.state = state

    def __str__(self):
        return 'InvalidState(%r)' % self.state


class MMPProtocol(object):
    """
    Manage the serial communications protocol with the MMP.

    This class serves as a parser to which you supply a dictionary of event
    handlers (callables) and to which you feed the input one character at a
    time. The :meth:`feed` method will return a byte-string to send to the MMP
    or ``None`` if there is nothing to output.

    Example usage::

        p = MMPProtocol(event_handlers)
        for c in port.read(1):
            output = p.feed(c)
            if output is not None:
                port.write(output)

    *event_handlers* is a dictionary mapping MMP commands (in lower case) to
    callables. Each handler is called with the MMP command as the first
    argument followed by the command parameters. The MMPDIR and MMPFIL are the
    exception. The MMPDIR handler is passed the directory listing (as an array
    of strings) as its second argument while the MMPFIL handler is passed the
    file name and file contents (as a binary array). A handler must return
    ``None`` or a byte string which will be sent to the MMP.
    """
    eol = b'\n'
    acks = {'MMPOBS': b'ACKOBS\r\n',
            'MMPDCK': b'ACKDCK\r\n'}

    def __init__(self, events=None):
        self._state = 'readline'
        self.recbuf = bytearray()
        self.events = events or {}

    def feed(self, c):
        f = getattr(self, 'state_' + self._state)
        if not f:
            raise InvalidState(self._state)
        return f(c)

    def cmd_default(self, cmd, *args):
        ev = self.events.get(str(cmd.lower()))
        if ev:
            ev(cmd, *args)
        return self.acks.get(cmd)

    def state_readline(self, c):
        self.recbuf.append(c)
        rval = None
        if c == self.eol:
            args = self.recbuf.strip().split()
            cmd = args[0]
            self.recbuf = bytearray()
            f = getattr(self, str('enter_' + cmd.lower()), self.cmd_default)
            rval = f(str(cmd), *(args[1:]))
        return rval

    def enter_mmpeod(self, cmd, crc):
        if self._crc != int(crc):
            raise CRCError({'command': 'MMPDIR',
                            'calculated': self._crc,
                            'expected': int(crc)})
        return b'ACKEOD\r\n'

    def enter_mmpeof(self, cmd, crc):
        if self._crc != int(crc):
            return b'NAKEOF\r\n'
        return b'ACKEOF\r\n'

    def enter_mmpdir(self, cmd, count):
        self._crc = 0
        self._file_count = int(count)
        if self._file_count > 0:
            self._state = 'mmpdir'
        else:
            self._state = 'readline'
        return None

    def state_mmpdir(self, c):
        self.recbuf.append(c)
        self._crc = update_crc16(self._crc, ord(c))
        rval = None
        if c == b'\r':
            self._file_count -= 1
            if self._file_count <= 0:
                rval = self.exit_mmpdir(self.recbuf)
        return rval

    def exit_mmpdir(self, listing):
        self._state = 'readline'
        self.recbuf = bytearray()
        ev = self.events.get('mmpdir')
        if ev:
            ev('MMPDIR', listing.strip().split('\r'))
        return None

    def enter_mmpfil(self, cmd, name, size):
        self._crc = 0
        self._file_name = name
        self._file_size = int(size)
        if self._file_size > 0:
            self._state = 'mmpfil'
        else:
            self._state = 'readline'
        return None

    def state_mmpfil(self, c):
        self.recbuf.append(c)
        self._crc = update_crc16(self._crc, ord(c))
        self._file_size -= 1
        rval = None
        if self._file_size <= 0:
            rval = self.exit_mmpfil(self._file_name, self.recbuf)
        return rval

    def exit_mmpfil(self, name, contents):
        self._state = 'readline'
        self.recbuf = bytearray()
        ev = self.events.get('mmpfil')
        if ev:
            ev('MMPFIL', name, contents)
        return None
