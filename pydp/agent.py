#!/usr/bin/env python
#
import gevent
import time
from numpy import dtype
import simplejson as json

class SensorAgent(object):
    """
    Manage the sampling and data storage for a :class:`sensor.SerialSensor`. The
    sampled data records are stored in a PyTables HDF5 file.

    Typical usage::

      fileh = tables.openFile('example.hd5', mode='w')
      group = fileh.createGroup('/', 'profile')
      agents = []
      for sensor_obj, interval, is_streaming in sensor_list:
          agent = SensorAgent(sensor_obj, fileh, group, streaming=is_streaming)
          agent.start(interval)
          agents.append(agent)
      # ...
      for agent in agents:
          agent.stop()
      fileh.close()

    Any exceptions that occur during sampling (i.e. between the calls to
    :meth:`start` and :meth:`stop`) will be raised when :meth:`stop` is
    called.
    """
    def __init__(self, sensor, fileh, group, streaming=False):
        """
        Initialize a SensorAgent instance.

        :param sensor: sensor to sample
        :type sensor: :class:`SerialSensor`
        :param fileh: HDF5 file handle
        :type fileh: :class:`tables.File`
        :param group: parent group for the new table
        :type group: :class:`tables.Group`
        """
        self.sensor = sensor
        m = self.sensor.get_metadata()
        table_desc = [('time', 'f8')]
        for name in m['fields']:
            table_desc.append((name, m['dtypes'][name]))
        self.table = fileh.createTable(group,
                                       sensor.name.replace('/', '_'),
                                       dtype(table_desc),
                                       'Data table for %s' % self.sensor.name)
        m['units']['time'] = 'seconds and microseconds since 1/1/1970 UTC'
        self.table.attrs.metadata = json.dumps(m)
        self.streaming = streaming
        self.task = None
        self.sensor.start()

    def __del__(self):
        self.sensor.stop()

    def start(self, interval):
        """
        Start the asynchronous sensor sampling task. This method returns as soon
        as the task is started.

        :param interval: sampling interval in seconds (ignored for streaming
                         sensors).
        :type interval: float
        """
        self.sensor.on_event('connect').get()
        self.sensor.on_event('sample').get()
        self.is_done = False
        if self.streaming:
            self.task = gevent.spawn(self._run)
        else:
            self.task = gevent.spawn(self._sample, interval)

    def stop(self, timeout=5):
        """
        Stop the sensor sampling task and return a count of the number of data
        records stored.

        :param timeout: number of seconds to wait for task to finish
        :type timeout: float
        :raises: any exception that occurred during sampling
        :return: data record count.
        """
        self.is_done = True
        self.task.join(timeout=timeout)
        if not self.task.ready():
            self.task.kill(timeout=timeout)
        self.sensor.on_event('pause').get()
        self.sensor.on_event('disconnect').get()
        return self.task.get(timeout=timeout)

    def _run(self):
        """
        :class:`gevent.Greenlet` task to sample data from a streaming sensor
        and write the values to the HDF5 file.

        :return: number of data records stored
        """
        assert self.sensor.state == 'ready'
        dataq = self.sensor.on_event('run').get()
        rec = self.table.row
        count = 0
        try:
            while not self.is_done:
                t, data = dataq.get()
                rec['time'] = t
                for name, value in data:
                    rec[name] = value
                rec.append()
                count += 1
                self.table.flush()
        finally:
            self.task.value = count
            self.table.flush()
        return count

    def _sample(self, interval):
        """
        :class:`gevent.Greenlet` task to sample data from a non-streaming sensor
        and write the values to the HDF5 file.

        :param interval: sampling interval in seconds
        :return: number of data records stored
        """
        assert self.sensor.state == 'ready'
        rec = self.table.row
        count = 0
        t0 = time.time()
        try:
            while not self.is_done:
                t1 = time.time()
                t, data = self.sensor.on_event('run').get()
                rec['time'] = t
                for name, value in data:
                    rec[name] = value
                rec.append()
                count += 1
                self.table.flush()
                t0 += interval
                if t0 < t1:
                    t0 = t1
                gevent.sleep(t0 - t1)
        finally:
            self.task.value = count
            self.table.flush()
        return count