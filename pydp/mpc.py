#!/usr/bin/env python
#
"""
.. module:: mpc
   :synopsis: interface to the McLane Profiler Controller
"""
import time
import gevent_subprocess as subprocess
import string
import os
import fcntl

CRC16_POLY = 0x1021


def update_crc16(crc, c):
    """
    Accumulate the 16-bit XMODEM (CCITT) CRC. *crc* is the current value
    of the CRC (16-bit integer) and *c* is the next data byte (8-bit integer).
    The return value is the new value of the CRC.

    >>> # data is an array of bytes
    >>> crc = reduce(update_crc, data, 0)
    """
    crc = crc ^ (c << 8)
    for i in range(8):
        if (crc & 0x8000):
            crc = (crc << 1) ^ CRC16_POLY
        else:
            crc = crc << 1
        crc = crc & 0xffff
    return crc


class CRCError(Exception):
    def __init__(self, context=None):
        self.context = context

    def __str__(self):
        return 'CRCError(%r)' % self.context


class MPC(object):
    """
    McLane Profiler Controller.
    """
    strace_cmd = 'strace -ttt -e trace=read,write -o /tmp/xmtsch.trace'
    xmodem_send = 'sx -b -k {filename}'
    strip_chars = string.whitespace + '\x00'

    def __init__(self, port, xmodem_debug=False):
        """
        Instance initializer.

        :param port: serial port interface
        :type port: pydp.serialcom.Port
        """
        assert port.isopen()
        self.port = port
        self.pnum = -1
        if xmodem_debug:
            self.xmodem_send = self.strace_cmd + ' ' + self.xmodem_send

    def readline(self, timeout=None, eol='\n'):
        """
        Read the output from the MPC up to the next linefeed

        :param timeout: read timeout in seconds or ``None`` to block.
        :rtype: string or ``None``
        """
        text = []
        done = False
        t = None
        while not done:
            c = self.port.read(1, timeout=timeout)
            if not c:
                return 0, None
            if t is None:
                t = time.time()
            if c == eol:
                done = True
            else:
                text.append(c)
        return t, ''.join(text)

    def sendmsg(self, msg, timeout=None):
        """
        Send a message to the MPC.

        :param msg: message tuple, all elements will be converted
                    to strings.
        :param timeout: serial port timeout in seconds
        """
        if msg[0] == 'SENDFILE':
            cmd = self.xmodem_send.format(filename=msg[1])
            # We need to switch the serial port to blocking mode
            # for the XMODEM file transfer.
            flags = fcntl.fcntl(self.port.fd, fcntl.F_GETFL)
            fcntl.fcntl(self.port.fd, fcntl.F_SETFL, flags & ~os.O_NONBLOCK)
            subprocess.call(cmd.split(), stdin=self.port.fd,
                            stdout=self.port.fd)
            fcntl.fcntl(self.port.fd, fcntl.F_SETFL, flags)
        elif msg[0] == 'CTLCLK':
            self.port.write('CTLCLK {0:d}\r\n'.format(int(time.time())))
        else:
            line = ' '.join([str(e) for e in msg])
            self.port.write(line + '\r\n', timeout=timeout)

    def recvmsg(self, timeout=None):
        """
        Receive a message from the MPC.

        :param timeout: serial port timeout in seconds or ``None`` to block.
        :return: (timestamp, list of strings)
        """
        t, line = self.readline(timeout=timeout)
        if line:
            line = line.strip(self.strip_chars)
            try:
                cmd, rest = line.split(' ', 1)
            except ValueError:
                cmd = line
                rest = None
            return t, [cmd] + (rest.split(', ') if rest else [])
        else:
            return None, None

    def __getattr__(self, name):
        """
        Expose MPC messages as class methods::

          self.ackprf(0, timeout=2) --> self.sendmsg(('ACKPRF', 0), timeout=2)

        """
        def _sender(*args, **kwds):
            self.sendmsg((name.upper(),) + args, timeout=kwds.get('timeout'))
        return _sender
