#!/usr/bin/env python
"""
.. module:: parsers
   :synopsis: parsers for sensor data records
"""
import time
from collections import deque, namedtuple

Timestamp = namedtuple('Timestamp', 'secs usecs')


class Parser(object):
    """
    Implement an iterative parser using a Pyparsing grammar.

    Arbitrarily sized blocks of input characters are sent to the
    parser using :meth:`feed`. Each complete set of matched tokens, in
    the form of a :class:`pyparsing.ParseResults` instance, is
    appended to a queue along with a timestamp. The queue contents can
    be read by iterating over the Parser instance.
    """
    def __init__(self, grammar):
        self.grammar = grammar
        self.buf = []
        self.q = deque()
        self.ts = 0

    def __len__(self):
        return len(self.q)

    def __iter__(self):
        return self

    def next(self):
        """
        Return the next result from the token queue. Each element
        is a tuple containing a timestamp and a list of tokens.
        """
        try:
            return self.q.popleft()
        except IndexError:
            raise StopIteration

    def feed(self, chars):
        """
        Add more characters to the parser.
        """
        if self.ts == 0:
            self.ts = time.time()
        g = self.grammar
        self.buf.append(chars)
        s = ''.join(self.buf)
        match = next(g.scanString(s), None)
        while match:
            tokens, start, end = match
            rest = s[end:]
            ts = Timestamp._make(divmod(int(self.ts*1000000), 1000000))
            self.q.append((ts, tokens))
            self.buf = [rest]
            match = next(g.scanString(rest), None)
            if not rest:
                self.ts = 0
        return len(self)


class MultimodeParser(Parser):
    """
    Iterative parser that can switch among multiple grammars based
    on *mode*.
    """
    def __init__(self, grammar, mode=None):
        self._mode = mode
        self.grammars = {}
        super(MultimodeParser, self).__init__(grammar)

    def add_grammar(self, mode, grammar):
        self.grammars[mode] = grammar

    @property
    def grammar(self):
        return self.grammars[self._mode]

    @grammar.setter
    def grammar(self, value):
        self.grammars[self._mode] = value

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        if not value in self.grammars:
            raise ValueError('Unknown parsing mode')
        self._mode = value
