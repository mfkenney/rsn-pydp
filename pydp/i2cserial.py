#!/usr/bin/env python
"""
.. module:: i2cserial
   :synopsis: interface to the BL233B RS232 - I2C adapter
"""
import serial
from binascii import a2b_hex, b2a_hex
from struct import unpack, pack


class SMBusSerial(object):
    def __init__(self, dev, baud=57600, bus=1):
        """
        Initialize a class instance.

        :param dev: serial device name.
        :param baud: serial baud rate.
        """
        self.port = serial.Serial(dev, baudrate=baud, timeout=3)
        self.bus = bus
        if not (0 < bus <= 4):
            raise ValueError('Invalid bus number')

    def _getline(self):
        line = self.port.readline()
        if line:
            line = line.rstrip()
        return line

    def read_byte(self, addr):
        """
        Read a byte from an SMBus address.

        :param addr: device address
        """
        addr = addr << 1
        self.port.write(':G{:d} S{:02X}01P\n'.format(self.bus, addr | 0x01))
        resp = self._getline()
        if resp:
            return int(resp, 16)
        else:
            return -1

    def write_byte(self, addr, val):
        """
        Write a byte to an SMBus address.

        :param addr: device address
        :param val: value to write
        """
        addr = addr << 1
        self.port.write(':G{:d} S{:02X}{:02X}P\n'.format(self.bus, addr, val))

    def read_word_data(self, addr, cmd):
        """
        I2C Read Word Data transaction.

        :param addr: device address
        :param cmd: command word
        :return: response word (unsigned integer) or -1 on error
        """
        addr = addr << 1
        cmd = b2a_hex(pack('<H', cmd)).upper()
        self.port.write(':G{:d} S{:02X}{} R02P\n'.format(
            self.bus, addr, cmd))
        resp = self._getline()
        if resp:
            # Response is sent LSB first
            val = int(resp[0:2], 16) + 256*int(resp[2:4], 16)
            return val
        else:
            return -1

    def write_word_data(self, addr, cmd, val):
        """
        I2C Write Word Data transaction.

        :param addr: device address
        :param cmd: command word
        :param val: 16-bit data word sent LSB first
        """
        addr = addr << 1
        cmd = b2a_hex(pack('<H', cmd)).upper()
        data = b2a_hex(pack('<H', val)).upper()
        self.port.write(':G{:d} S{:02X}{}{}P\n'.format(
            self.bus, addr, cmd, data))

    def read_i2c_block_data(self, addr, cmd):
        """
        I2C Read Block Data transaction.

        :param addr: device address
        :param cmd: command word
        :return: response block as a string.
        """
        addr = addr << 1
        cmd = b2a_hex(pack('<H', cmd)).upper()
        self.port.write(':G{:d} S{:02X}{} R00 P\n'.format(
            self.bus, addr, cmd))
        resp = self._getline()
        if resp:
            return str(a2b_hex(resp))
        else:
            return ''
