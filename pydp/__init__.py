#!/usr/bin/env python
#
import time

__version__ = '2.0'


def timer(interval, fsleep=time.sleep):
    """
    Timer tick generator. Can be used to iterate at a fixed rate.
    Returns a series of time values.

    :param interval: time between ticks in seconds
    :param fsleep: sleep function
    """
    t0 = time.time()
    while True:
        t1 = time.time()
        yield t1
        if interval > 0:
            t0 += interval
            if t0 < t1:
                t0 = t1
            fsleep(t0 - t1)
        else:
            break


def sync_timer(interval, fsleep=time.sleep):
    """
    Version of :func:`timer` which is synchronous with the time-of-day. Each
    tick occurs at a multiple of *interval* seconds.

    :param interval: time between ticks in seconds
    :param fsleep: sleep function
    """
    t0 = time.time()
    if interval > 0:
        q, r = divmod(int(t0), interval)
        t_next = (q + 1) * interval
        fsleep(t_next - time.time())
        t0 = t_next
    while True:
        t1 = time.time()
        yield t1
        if interval > 0:
            t0 += interval
            if t0 < t1:
                t0 = t1
            fsleep(t0 - t1)
        else:
            break
