#!/usr/bin/env python
#
"""
.. module:: serialcom
   :platform: Unix
   :synopsis: Gevent compatible serial port interface.
"""
import gevent
import serial
import os
import time
import logging
from gevent.select import select
import xml.sax.saxutils as su
from gevent.queue import Queue, Empty, Full
from pydp.scanners import coroutine
from collections import deque


class Port(object):
    """
    Serial port interface. Enhances a ``serial.Serial`` instance to work with
    gevent.
    """

    #: Valid flow-control settings
    flow_control = ('xonxoff', 'rtscts', 'dsrdtr')

    def __init__(self, device, baud=9600, flow=None):
        """
        Initialize a class instance.

        :param device: serial device filename (e.g. ``/dev/ttyS0``)
        :param baud: baud rate
        :type baud: integer
        :param flow: flow-control specification string or ``None``
                     (see :attr:`flow_control`)
        """
        #: Optional file to log the serial port I/O
        self.sessionlog = None
        #: Serial port interface
        self.serial = serial.Serial()
        #: File descriptor for serial port interface
        self.fd = None
        self.serial.port = device
        self.name = device.split('/')[-1]
        self.serial.baudrate = baud
        self.serial.timeout = 0
        if flow in self.flow_control:
            setattr(self.serial, flow, True)

    def __repr__(self):
        return '%s(%r, baud=%r)' % (
            self.__class__.__name__,
            self.serial.port, self.serial.baudrate)

    def open(self, sessionlog=None, blocking=False):
        """
        Open the serial interface and start a communication session.

        :param sessionlog: file to log all communication.
        """
        self.sessionlog = sessionlog
        self.serial.open()
        if not blocking:
            self.serial.nonblocking()
        self.fd = self.serial.fileno()
        if self.sessionlog:
            self.sessionlog.write('<session class="%s" dev="%s">\n' % (
                self.__class__.__name__,
                self.serial.port.encode('utf-8')))

    def isopen(self):
        """
        :return: ``True`` if the port is open.
        """
        return self.fd is not None

    def close(self):
        """
        Close the serial interface and communication session.
        """
        if self.isopen():
            self.serial.close()
            self.fd = None
            if self.sessionlog:
                self.sessionlog.write('</session>\n')
                self.sessionlog.flush()

    def read(self, nbytes, timeout=None):
        """
        Read data from the serial port until *nbytes* are received or the
        *timeout* expires. A *timeout* value of ``None`` results in a
        blocking read.

        :param nbytes: maximum number of bytes to read
        :param timeout: serial port timeout in seconds
        :return: byte string of length <= *nbytes*
        """
        if self.fd is None:
            raise IOError('Serial port not open')
        n = 0
        buf = []
        while n < nbytes:
            rlist, _, _ = select([self.fd], [], [], timeout=timeout)
            if not rlist:
                break
            s = os.read(self.fd, nbytes - n)
            if not s:
                break
            n += len(s)
            buf.append(s)
            if self.sessionlog:
                self.sessionlog.write(
                    '<recv t="%.6f">%r</recv>\n' % (time.time(),
                                                    su.escape(s)))
        return ''.join(buf)

    def write(self, s, timeout=None):
        """
        Write a sequence of bytes to the serial port

        :param s: string or byte sequence
        :param timeout: serial port timeout in seconds
        :return: sequence of unwritten bytes, will be empty
                 if the operation was successful.
        """
        if self.fd is None:
            raise IOError('Serial port is not open')
        s = serial.to_bytes(s)
        while s:
            _, wlist, _ = select([], [self.fd], [], timeout=timeout)
            if not wlist:
                break
            n = os.write(self.fd, s)
            if n <= 0:
                break
            if self.sessionlog:
                self.sessionlog.write(
                    '<send t="%.6f">%s</send>\n' % (time.time(),
                                                    su.escape(s[:n])))
            s = s[n:]
        return s


class Reader(object):
    """
    Class to manage reading from a serial sensor. This class uses a Greenlet to
    read the stream of bytes from a sensor and one or more :mod:`pydp.scanners`
    to turn that stream of bytes into a stream of records which are buffered in
    a :class:`gevent.queue.Queue`.

    If multiple scanners are used, the *mode* attribute is used to select the
    one that is currently active.
    """
    def __init__(self, port, qmax=None, logger=None, size=1, timeout=None):
        assert port.isopen()
        self.port = port
        self.qmax = qmax
        self.mode, self.queue, self.task = None, None, None
        self.scanners = {}
        self.logger = logger or logging.getLogger('rdr-' + port.name)
        self.size = size
        self.timeout = timeout

    def __repr__(self):
        return '{0}({1!r})'.format(self.__class__.__name__,
                                   self.port)

    def add_scanner(self, scanner, mode=None, scanner_args=()):
        """
        Add an input scanner. A scanner is a coroutine which turns the
        stream of bytes from the sensor into a stream of records. Each
        scanner is associated with a *mode* of operation.

        :param scanner: scanner function.
        :param mode: scanner identifier or ``None`` for default
        """
        self.scanners[mode] = scanner(self._enqueue(), *scanner_args)

    def isactive(self):
        return (self.task is not None)

    def activate(self):
        """
        Start the reader task.
        """
        assert self.task is None
        self.queue = Queue(maxsize=self.qmax)
        self.task = gevent.spawn(self._reader)

    def deactivate(self):
        """
        Stop the reader task.
        """
        assert self.task is not None
        self.task.kill(timeout=3)
        self.queue, self.task = None, None

    @coroutine
    def _enqueue(self):
        while True:
            timestamp, data = (yield)
            try:
                self.queue.put((timestamp, data))
            except Full:
                pass

    def _reader(self):
        self.logger.debug('Reader task starting')
        try:
            while True:
                buf = self.port.read(self.size)
                scanner = self.scanners.get(self.mode)
                if scanner:
                    try:
                        for c in buf:
                            scanner.send(c)
                    except Exception:
                        self.logger.exception('Error parsing %r', buf)
        finally:
            self.logger.debug('Reader task exiting')

    def drain_queue(self):
        """
        Empty the input queue.
        """
        assert self.queue is not None
        self.deactivate()
        self.activate()

    def __call__(self, timeout=None, block=True):
        """
        Receive a response from the sensor. The return value is a tuple
        containing a timestamp in seconds along with the response string.
        If the timeout expires, a tuple of (``None``, ``None``) is returned.

        :param timeout: maximum number of seconds to wait
        :param block: wait for input if ``True``, *timeout* is ignored
                      if ``False``.
        :return: (*timestamp*, *response*) or (``None``, ``None``)
        """
        assert self.task is not None
        try:
            t, response = self.queue.get(timeout=timeout, block=block)
            self.logger.debug('RECV: %r', response)
            return t, response
        except Empty:
            return None, None


class Writer(object):
    """
    Class to manage writing to a serial sensor.
    """
    def __init__(self, port, logger=None):
        assert port.isopen()
        self.port = port
        self.logger = logger or logging.getLogger('wtr-' + port.name)

    def __repr__(self):
        return '{0}({1!r})'.format(self.__class__.__name__,
                                   self.port)

    def __call__(self, cmd, timeout=None, chunksize=None, delay=0):
        """
        Send a command string to the sensor. To accommodate devices with small
        input buffers, the *chunksize* and *delay* parameters can be used to
        send the input in small segments with a short delay in-between.

        :param cmd: command string
        :param timeout: maximum number of seconds to wait
        :param chunksize: number of characters to send at a time
        :type chunksize: int or None
        :param delay: delay in seconds between chunks
        :return: boolean status
        """
        self.logger.debug('SEND: %r', cmd)
        total = len(cmd)
        nsplit = chunksize or total
        start, end, n = 0, nsplit, 0
        while n < total:
            leftover = self.port.write(cmd[start:end], timeout=timeout)
            if leftover:
                break
            gevent.sleep(delay)
            start += nsplit
            end += nsplit
            n += nsplit
        return n == total


class SyncReader(object):
    """
    Class to manage reading from a serial sensor. This is a synchronous version
    of :class:`Reader`. No background Greenlet is used to read the input. The
    input is only read when the instance is called. As with :class:`Reader`,
    one or more scanners are used to turn the input stream of bytes into a
    sequence of records.

    If multiple scanners are used, the *mode* attribute is used to select the
    one that is currently active.
    """
    def __init__(self, port, qmax=None, logger=None, size=1024, timeout=None):
        assert port.isopen()
        self.port = port
        self.mode = None
        self.queue = deque()
        self.scanners = {}
        self.logger = logger or logging.getLogger('rdr-' + port.name)
        self.size = size
        self.timeout = timeout

    def __repr__(self):
        return '{0}({1!r})'.format(self.__class__.__name__,
                                   self.port)

    def add_scanner(self, scanner, mode=None, scanner_args=()):
        """
        Add an input scanner. A scanner is a coroutine which turns the
        stream of bytes from the sensor into a stream of records. Each
        scanner is associated with a *mode* of operation.

        :param scanner: scanner function.
        :param mode: scanner identifier or ``None`` for default
        """
        if mode in self.scanners:
            self.scanners[mode].close()
        self.scanners[mode] = scanner(self._enqueue(), *scanner_args)

    def isactive(self):
        return True

    def activate(self):
        """
        Start the reader task.
        """
        pass

    def deactivate(self):
        """
        Stop the reader task.
        """
        pass

    @coroutine
    def _enqueue(self):
        while True:
            timestamp, data = (yield)
            try:
                self.queue.append((timestamp, data))
            except Full:
                pass

    def drain_queue(self):
        """
        Empty the input queue.
        """
        self.queue = deque()

    def __call__(self, timeout=None, **kwds):
        """
        Receive a response from the sensor. The return value is a tuple
        containing a timestamp in seconds along with the response string.
        If the timeout expires, a tuple of (``None``, ``None``) is returned.

        :param timeout: maximum number of seconds to wait
        :return: (*timestamp*, *response*) or (``None``, ``None``)
        """
        scanner = self.scanners.get(self.mode)
        with gevent.Timeout(timeout, False):
            while len(self.queue) == 0:
                rlist, _, _ = select([self.port.fd], [], [], timeout=timeout)
                if rlist:
                    buf = os.read(self.port.fd, self.size)
                    self.logger.debug('RECV: %r', buf)
                    for c in buf:
                        scanner.send(c)
        try:
            return self.queue.popleft()
        except IndexError:
            return None, None
