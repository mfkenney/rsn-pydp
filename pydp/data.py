#!/usr/bin/env python
#
"""
.. :module: data
    :synopsis: deep-profiler sensor data management
"""
import gevent
from gevent import monkey
monkey.patch_all()
import msgpack
import logging
from gevent.queue import Empty
from gevent.event import AsyncResult
from pydp import timer
from collections import namedtuple
from redis import RedisError


SampleCtl = namedtuple('SampleCtl', 'interval timeout prep_args keep_raw')


class DataQueue(object):
    """
    Implement a data queue using a Redis list
    """
    def __init__(self, rd, key, maxlen=0):
        self.rd = rd
        self.key = key
        self.maxlen = maxlen
        self.logger = logging.getLogger('dq')

    def __len__(self):
        return self.llen(self.key)

    def put(self, entry):
        try:
            self.rd.lpush(self.key, msgpack.packb(entry))
        except RedisError as e:
            self.logger.critical('Cannot add to queue: %r', e)
        if self.maxlen > 0:
            self.rd.ltrim(self.key, 0, self.maxlen - 1)

    def get(self, timeout=None, block=True):
        if block:
            entry = self.rd.brpop(self.key, timeout=timeout)
        else:
            entry = self.rd.rpop(self.key)
        if not entry:
            raise Empty
        return msgpack.unpackb(entry[1])


def sample_sensor(sens, name, out_queue, ctl):
    """
    Greenlet task to sample a sensor. Data records are written
    to a queue along with a timestamp and the sensor name. This
    task runs forever and must be killed by the caller.

    :param sens: sensor object
    :type sens: pydp.sensors.Sensor
    :param name: sensor name
    :type name: string
    :param ctl: sensor sampling parameters
    :type ctl: SampleCtl
    :return: tuple containing a count of the number of read attempts
             and the number of errors.
    """
    logger = logging.getLogger('sample:' + name)
    reads, errors = 0, 0
    logger.info('Sampling task starting')
    sens.prepare(**ctl.prep_args)
    try:
        if ctl.interval == 0:
            pass
            while True:
                sens.sample()
                reads += 1
                t, rawdata = sens.recv(ctl.timeout)
                if t:
                    secs = int(t)
                    usecs = int((t - secs) * 1000000)
                    if ctl.keep_raw:
                        out_queue.put((name + '-raw', secs, usecs, rawdata))
                    out_queue.put((name, secs, usecs,
                                  sens.format_data(rawdata)))
                else:
                    errors += 1
        else:
            for tick in timer(ctl.interval, fsleep=gevent.sleep):
                sens.sample()
                reads += 1
                t, rawdata = sens.recv(ctl.timeout)
                if t:
                    secs = int(t)
                    usecs = int((t - secs) * 1000000)
                    if ctl.keep_raw:
                        out_queue.put((name + '-raw', secs, usecs, rawdata))
                    out_queue.put((name, secs, usecs,
                                  sens.format_data(rawdata)))
                else:
                    errors += 1
    finally:
        sens.cleanup()
        logger.info('Sampling task exiting')
        return reads, errors


def send_commands(sens, commands, timeout=None):
    """
    Greenlet task to send one or more commands to a sensor. The sensor
    responses are returned as a list.

    :param sens: sensor object
    :param commands: list of command strings
    :param timeout: sampling timeout in seconds
    """
    responses = []
    for cmd in commands:
        responses.append(sens.command(cmd, timeout))
    return responses


class SensorResult(AsyncResult):
    def __init__(self, sens):
        AsyncResult.__init__(self)
        self.sens = sens

    def __call__(self, *args):
        self.sens.deactivate()
        return AsyncResult.__call__(self, *args)


class DataManager(object):
    """
    Manage the sampling and data storage of one or more Sensors. Each sensor
    is sampled by a separate task which queues the time-stamped data records
    for the archiver
    """
    data_queue = 'archive:queue'

    def __init__(self, rd):
        """
        :param rd: Redis server for data distribution
        :type rd: redis.StrictRedis
        """
        self._sensors = {}
        self.tasks = {}
        self.logger = logging.getLogger('datamgr')
        self.rd = rd

    def __getitem__(self, name):
        return (self._sensors[name])[0]

    def add_sensor(self, name, sens, interval, timeout=None,
                   prep_args={}, keep_raw=False):
        """
        Add a new sensor.

        :param name: sensor name (must be unique)
        :param sens: Sensor object instance
        :type sens: :class:`pydp.sensors.Sensor`
        :param interval: sampling interval in seconds (set to 0 for streaming
                         sensors)
        :param timeout: sensor read timeout in seconds
        """
        ctl = SampleCtl(interval=interval, timeout=timeout,
                        prep_args=prep_args, keep_raw=keep_raw)
        self._sensors[name] = (sens, ctl)

    def remove_sensor(self, name):
        """
        Remove a sensor.
        """
        del self._sensors[name]

    def sensors(self):
        """
        List of sensor names.
        """
        return self._sensors.keys()

    def send_commands(self, name, commands):
        """
        Send one or more commands to a sensor asynchronously.

        :param name: sensor name
        :param commands: list of command strings
        :rtype: :class:`gevent.event.AsyncResult`
        """
        sens, ctl = self._sensors[name]
        result = SensorResult(sens)
        gevent.spawn(send_commands, sens, commands, ctl.timeout).link(result)
        return result

    def start(self, **kwds):
        """
        Start the sampling process. This results in a new data-set directory
        being added to the archive.

        :param seq: sequence number for this session
        :param t: session start time in seconds
        """
        queue = DataQueue(self.rd, self.data_queue, maxlen=250)
        for name, entry in self._sensors.items():
            sens, ctl = entry
            if hasattr(sens, 'variables'):
                self.rd.hset(name + '_metadata', 'variables',
                             msgpack.packb(getattr(sens, 'variables')))
            # If an error occurs when trying to activate the sensor
            # drop it from the task list.
            try:
                sens.activate()
            except Exception:
                pass
            else:
                self.tasks[name] = gevent.spawn(
                    sample_sensor, sens, name, queue, ctl)

    def stop(self):
        """
        Stop the sampling process.
        """
        for name, task in self.tasks.items():
            self.logger.info('Stopping %s task', name)
            if not task.ready():
                task.kill(timeout=5)
            if task.exception is not None:
                self.logger.critical(
                    '%s sampling error (%r)', name, task.exception)
            else:
                try:
                    reads, errors = task.value
                except ValueError:
                    self.logger.critical(
                        '%s sampling error (%r)', name, task.value)
                    reads, errors = 0, 1
                self.rd.hincrby('sensor:errors', name, amount=errors)
                self.rd.hincrby('sensor:reads', name, amount=reads)
            self[name].deactivate()
