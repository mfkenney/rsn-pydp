#!/usr/bin/env python
#
# Manage power switches.
#
from weakref import WeakValueDictionary
import logging
import time


class FakeDio(object):
    """
    Fake DIO port for testing the PowerSwitch class
    """
    def __init__(self):
        self.inreg = 0
        self.outreg = 0
        self.outputs = 0

    def set(self, mask):
        mask &= self.outputs
        self.outreg |= mask

    def clear(self, mask):
        mask &= self.outputs
        self.outreg &= ~mask

    def test(self, line):
        mask = 1 << line
        if mask & self.outputs:
            val = self.outreg
        else:
            val = self.inreg
        return (val & mask) == mask


class _Switch(object):
    """
    Class to use Python's built-in reference counting to manage the
    state of a power switch. Creating a new object instance turns on
    the switch and deleting the instance turns it off.
    """
    _table = WeakValueDictionary()

    def __new__(cls, idx, port):
        sw = _Switch._table.get(idx)
        if not sw:
            sw = object.__new__(cls)
            sw.name = 'sw-{0}'.format(idx)
            sw.idx = int(idx)
            sw.t_on = time.time()
            sw.port = port
            logging.info('Power on %s', sw.name)
            port.set(1 << sw.idx)
            _Switch._table[sw.idx] = sw
        return sw

    def __del__(self):
        logging.info('Power off %s', self.name)
        self.port.clear(1 << self.idx)


class PowerSwitch(object):
    """
    Class to manage a power switch.

    >>> import logging
    >>> logging.basicConfig(level=logging.INFO)
    >>> sw1 = PowerSwitch(1, FakeDio())
    >>> sw2 = PowerSwitch(1, FakeDio())
    >>> sw1.on()
    INFO:root:Power on sw-1
    >>> sw2.on()
    >>> sw1.off()
    >>> sw2.off()
    INFO:root:Power off sw-1
    """
    def __init__(self, idx, port):
        self.idx = idx
        self.refs = []
        self.port = port

    def on(self):
        self.refs.append(_Switch(self.idx, self.port))

    def off(self):
        if self.refs:
            self.refs.pop()
        else:
            # Bit of a hack here so we can force a
            # switch off before PowerSwitch.on has
            # been called.
            self.port.clear(1 << self.idx)

    def timer(self):
        """
        Return the switch on time in seconds or zero if the
        switch is off.
        """
        try:
            return time.time() - self.refs[-1].t_on
        except IndexError:
            return 0
