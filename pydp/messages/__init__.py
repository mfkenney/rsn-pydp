#
# pydp.messaging package
#
from google.protobuf.descriptor import FieldDescriptor
from profiler_pb2 import Status, Battery, ChargingState, SensorData, \
    DbEntry, Profile
from dock_pb2 import ProfilerStatus, IpsStatus, DockStatus, Alert
from sensors_pb2 import CtdData, OptodeData, AcmData, FlntuData, FlcdData
from ast import literal_eval
from urllib import quote, unquote


def pb_from_dict(pb, d):
    """
    Set Protocol Buffer Message field values from a dictionary.

    :param pb: Protocol Buffer Message instance
    :param d: dictionary
    """
    for k, v in d.items():
        try:
            setattr(pb, k, v)
        except TypeError:
            if type(v) == type(''):
                # This might be an enum value expressed as a string ...
                setattr(pb, k, getattr(pb, v))
            elif isinstance(v, dict):
                pb_from_dict(getattr(pb, k), v)
            else:
                raise
        except AttributeError:
            if isinstance(v, WrappedMessage):
                pb_from_dict(getattr(pb, k), v.todict())
            elif isinstance(v, dict):
                pb_from_dict(getattr(pb, k), v)
            else:
                # Assigning to a repeated field
                obj = getattr(pb, k)
                obj.extend(v)


def pb_to_dict(pb):
    """
    Create a dictionary from a Protocol Buffer Message

    :param pb: Protocol Buffer Message instance
    :return: dictionary
    """
    d = dict()
    for desc, value in pb.ListFields():
        if desc.enum_type is not None:
            d[desc.name] = desc.enum_type.values_by_number[value].name
        elif desc.message_type is not None:
            if desc.label == FieldDescriptor.LABEL_REPEATED:
                L = []
                for pb in value:
                    L.append(pb_to_dict(pb))
                d[desc.name] = tuple(L)
            else:
                d[desc.name] = pb_to_dict(value)
        else:
            # This is a bit of a hack to detect if a value is an instance
            # of Google's internal container class that is used for
            # repeated values. We want to convert the returned list to a
            # tuple. If the container class had simply subclassed 'list'
            # we could use 'isinstance(value, list)' ...
            if hasattr(value, '_values'):
                d[desc.name] = tuple(value._values)
            else:
                d[desc.name] = value
    return d


def args_to_dict(args, do_eval=True):
    """
    Convert a command argument list of the form NAME=VALUE
    into a dictionary. VALUE must not contain any whitespace,
    spaces must be url-encoded.

    >>> d = args_to_dict('foo=bar baz=1,2,3').items()
    >>> d.sort()
    >>> d
    [('baz', [1, 2, 3]), ('foo', 'bar')]
    """
    def maybe_eval(s):
        if do_eval:
            try:
                return literal_eval(s)
            except (ValueError, SyntaxError):
                return s
        else:
            return s
    params = {}
    for name, val in [arg.split('=') for arg in args.split()]:
        if ',' in val:
            params[name] = [maybe_eval(unquote(v)) for v in val.split(',')]
        else:
            params[name] = maybe_eval(unquote(val))
    return params


_registry = {}


def deserialize(pbname, data):
    """
    Deserialize a block of message data to a WrappedMessage class instance.

    :param pbname: the Protocol Buffer message name
    :param data: message data string
    :return: new WrappedMessage instance
    """
    cls = _registry[pbname]
    return cls.deserialize(data)


class MessageMetaClass(type):
    def __init__(cls, name, bases, kwds):
        super(MessageMetaClass, cls).__init__(name, bases, kwds)
        key = kwds.get('__pbclass__')
        if key:
            _registry[key.__name__] = cls


class WrappedMessage(object):
    """
    Wrap a Protocol Buffer class in something a bit more Pythonic
    """
    __metaclass__ = MessageMetaClass
    __pbclass__ = None

    def __init__(self, **kwds):
        self.msg = self.__pbclass__()
        pb_from_dict(self.msg, kwds)

    def __getattr__(self, name):
        return getattr(self.msg, name)

    def _asdict(self):
        return pb_to_dict(self.msg)
    todict = _asdict

    def serialize(self):
        return self.msg.SerializeToString()

    @property
    def pbname(self):
        return self.__pbclass__.__name__

    @classmethod
    def deserialize(cls, data):
        obj = cls()
        obj.msg.ParseFromString(data)
        return obj


class StatusMessage(WrappedMessage):
    """
    Class the represent a status message sent by the Profiler.

    >>> t = 1332775669629072L
    >>> m1 = StatusMessage(time=t, pressure=1234560, profile=1, state='PROFILE_UP', voltage=13600)
    >>> m1.todict()
    {'profile': 1, 'pressure': 1234560, 'state': 'PROFILE_UP', 'voltage': 13600, 'time': 1332775669629072L}
    >>> d = m1.todict()
    >>> m2 = StatusMessage(**d)
    >>> m1.serialize() == m2.serialize()
    True
    >>> m3 = StatusMessage.deserialize(m2.serialize())
    >>> m1.serialize() == m3.serialize()
    True
    >>> m3 = deserialize('Status', m2.serialize())
    >>> m1.serialize() == m3.serialize()
    True
    """
    __pbclass__ = Status


class BatteryMessage(WrappedMessage):
    __pbclass__ = Battery


class ChargingMessage(WrappedMessage):
    __pbclass__ = ChargingState


class SensorMessage(WrappedMessage):
    __pbclass__ = SensorData


class CtdMessage(WrappedMessage):
    __pbclass__ = CtdData


class OptodeMessage(WrappedMessage):
    __pbclass__ = OptodeData


class FlntuMessage(WrappedMessage):
    __pbclass__ = FlntuData


class FlcdMessage(WrappedMessage):
    __pbclass__ = FlcdData


class AcmMessage(WrappedMessage):
    __pbclass__ = AcmData


class ProfilerMessage(WrappedMessage):
    """
    Class to encapsulate a StatusMessage along with a timestamp.

    >>> t = 1332775669629072L
    >>> m1 = StatusMessage(time=t, pressure=1234560, profile=1, state='PROFILE_UP', voltage=13600)
    >>> ps = ProfilerMessage(time=t, status=m1)
    >>> m1.pressure == ps.status.pressure
    True
    >>> m1.state == ps.status.state
    True
    >>> ps.status.voltage == 13600
    True
    """
    __pbclass__ = ProfilerStatus


class IpsMessage(WrappedMessage):
    __pbclass__ = IpsStatus


class AlertMessage(WrappedMessage):
    __pbclass__ = Alert


class DockMessage(WrappedMessage):
    __pbclass__ = DockStatus

    def add_alert(self, alert):
        pb = self.msg.alerts.add()
        if isinstance(alert, WrappedMessage):
            pb_from_dict(pb, alert.todict())
        elif isinstance(alert, dict):
            pb_from_dict(pb, alert)


class DbMessage(WrappedMessage):
    __pbclass__ = DbEntry


class ProfileMessage(WrappedMessage):
    __pbclass__ = Profile


class EventMessage(object):
    """
    Class to handle event messages from the DPC using a similar
    API to the ProtocolBuffer data messages.

    >>> msg = EventMessage.deserialize('profile:start t=123456789 pnum=42 mode=up')
    >>> msg.ev
    'profile:start'
    >>> msg.attrs['pnum']
    42
    >>> msg.pnum
    42
    """
    def __init__(self, ev, **attrs):
        self.ev = ev
        self.attrs = attrs

    def __getattr__(self, name):
        try:
            return self.attrs[name]
        except KeyError:
            raise AttributeError

    @classmethod
    def deserialize(cls, text):
        try:
            event, args = text.split(' ', 1)
        except ValueError:
            event = text
            attrs = {}
        else:
            attrs = args_to_dict(args)
        return cls(event, **attrs)

    def serialize(self):
        def stringify(x):
            if isinstance(x, list):
                return ','.join([quote(bytes(e)) for e in x])
            else:
                return quote(bytes(x))
        L = [self.ev]
        L.extend(['='.join([k, stringify(v)]) for k, v in self.attrs.items()])
        return ' '.join(L)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
