//
// Protocol Buffer definitions
//
import "sensors.proto";
package profiler;
option py_generic_services = false;

// Status message sent periodically by the Profiler
message Status {
    // time in microseconds since 1/1/1970 UTC
    required fixed64 time = 1;
    // profile index number (zero based)
    required fixed32 profile = 2;
    required StateType state = 3;
    // in-situ pressure in decibars * 1000
    required int32 pressure = 4;
    // average voltage of all batteries in mV
    optional int32 voltage = 5;
    // total current from all batteries in mA
    optional int32 current = 6;
    // internal temperature(s) in degC * 10
    repeated int32 itemp = 7 [packed=true];
    // internal humidity in %rh * 10
    optional int32 humidity = 8;
    // total remaining battery capacity in mWh
    optional int32 energy = 9;
    // MMP motor current in mA
    optional int32 motor_current = 10;
    // relative charge of the battery pack in percent
    optional int32 rel_charge = 11;
    // optional voltage from Vmon circuit in millivolts
    optional int32 vmon = 12;

    enum StateType {
        STOPPED = 0;
        PROFILE_UP = 1;
        PROFILE_DOWN = 2;
        DOCKING = 3;
        DOCKED = 4;
        CHARGING = 5;
        STATIONARY = 6;
    }
}

// More detailed battery information
message Battery {
    // time in microseconds since 1/1/1970 UTC
    required fixed64 time = 1;
    // number of batteries sampled
    required int32 count = 2;
    // average battery temperature in degC * 10
    required int32 temp = 3;
    // average battery voltage in mV
    required int32 voltage = 4;
    // total battery current in mA
    required int32 current = 5;
    // remaining battery capacity in mWh
    required int32 energy = 6;
    // relative charge of the battery pack in percent
    required int32 rel_charge = 7;
}

// These messages are sent when the Profiler is charging
message ChargingState {
    // time in microseconds since 1/1/1970 UTC
    required fixed64 time = 1;
    // input current to each battery in mA
    repeated sint32 current = 2;
    // relative charge of the battery pack in percent
    optional int32 rel_charge = 3;
    // remaining capacity of the battery pack in mWh
    optional int32 energy = 4;
}

// Data from sensors, sent occasionally
message SensorData {
    optional sensors.CtdData ctd = 1;
    optional sensors.OptodeData optode = 2;
    optional sensors.AcmData acm = 3;
    optional sensors.FlntuData flntu = 4;
    optional sensors.FlcdData flcd = 5;
}

// This message is used to query or update an entry in the
// configuration database on the profiler.
message DbEntry {
    required string key = 1;
    repeated string value = 2;
}

// This message is used to describe a profile. Profile descriptions
// are stored in a queue on the DPC.
message Profile {
    // Profile type
    required ProfileType dir = 1;
    // Target depth in decibars
    required int32 depth = 2;
    // Time limit in seconds
    required int32 maxtime = 3;
    // List of disabled sensors
    repeated string exclude = 4;

    enum ProfileType {
        DIVE = 0;
        RISE = 1;
        STATIONARY = 2;
        DOCKING = 3;
    }

    enum Queueing {
        REPLACE = 0;  // replace the contents of the queue
        APPEND = 1;   // add to the tail of the queue
        PREPEND = 2;  // add to the head of the queue
    }
    // How should this description be queued?
    optional Queueing queueing = 5 [default = APPEND];
    // How many times should the description be requeued?
    optional uint32 requeue = 6 [default = 0];
    // Start profile on the next multiple of tstart seconds
    optional int32 tstart = 7 [default = 0];
    // Name of the pattern that this profile is part of.
    optional string pattern = 8;
}
