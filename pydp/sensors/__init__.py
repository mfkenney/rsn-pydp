#
# Sensor driver modules.
#


class SensorError(Exception):
    """
    Raised when an error response is received from a Sensor
    """
    def __init__(self, cmd):
        self.cmd = cmd

    def __str__(self):
        return 'Invalid command: %r' % self.cmd


class Sensor(object):
    """
    Abstract base class for all serial sensors.
    """
    def __init__(self, reader, writer):
        """
        Instance initializer.

        :param reader: serial input interface
        :type reader: pydp.serialcom.Reader
        :param writer: serial output interface
        :type writer: pydp.serialcom.Writer
        """
        self._reader = reader
        self._writer = writer

    @property
    def mode(self):
        return self._reader.mode

    @mode.setter
    def mode(self, mode):
        self._reader.mode = mode

    def activate(self):
        self._reader.activate()

    def deactivate(self):
        self._reader.deactivate()

    def send(self, cmd, timeout=None, chunksize=None, delay=0):
        """
        Send a command string to the sensor. To accommodate devices with small
        input buffers, the *chunksize* and *delay* parameters can be used to
        send the input in small segments with a short delay in-between.

        :param cmd: command string
        :param timeout: maximum number of seconds to wait
        :param chunksize: number of characters to send at a time
        :type chunksize: int or None
        :param delay: delay in seconds between chunks
        :return: boolean status
        """
        return self._writer(cmd, timeout=timeout,
                            chunksize=chunksize, delay=delay)

    def recv(self, timeout=None, block=True):
        """
        Receive a response from the sensor. The return value is a tuple
        containing a timestamp in seconds along with the response string.
        If the timeout expires, a tuple of (``None``, ``None``) is returned.

        :param timeout: maximum number of seconds to wait
        :param block: wait for input if ``True``, *timeout* is ignored
                      if ``False``.
        :return: (*timestamp*, *response*) or (``None``, ``None``)
        """
        return self._reader(timeout=timeout, block=block)

    def command(self, cmd, timeout, **kwds):
        """
        Send a command and return the response.

        :param cmd: command string
        :param timeout: maximum number of seconds to wait
        :param kwds: passed to :meth:`Sensor.send`
        :return: see :meth:`Sensor.recv`
        :raises: :class:`EnvironmentError`
        """
        self.send(cmd, timeout=timeout, **kwds)
        t, response = self.recv(timeout=timeout)
        if t is None:
            raise EnvironmentError((-1, 'No response from %r' % self))
        return t, response

    def prepare(self, *args, **kwds):
        """
        Prepare the device for sampling.
        """
        pass

    def sample(self):
        """
        Acquire a data sample. This method can remain a no-op if the instrument
        streams its data samples.
        """
        pass

    def cleanup(self, *args, **kwds):
        """
        End the sampling process.
        """
        pass

    def format_data(self, rawdata):
        """
        Format the raw data record from the device. This method should
        be overridden in the subclass.
        """
        return rawdata
