#!/usr/bin/env python
#
"""
.. :module: sensors.wetlab
    :synopsis: interface to Wetlabs sensors
"""
from pydp.sensors import Sensor
from pydp.scanners import acs_scanner, delimiter_scanner
from pydp.serialcom import SyncReader, Writer


class Wetlabs(object):
    """
    Interface to a generic Wetlabs device
    """
    def __getattr__(self, name):
        return getattr(self._sens, name)

    def __iter__(self):
        return self

    def next(self):
        t, result = self.recv(timeout=self.auto_timeout)
        if t is not None:
            return t, result
        raise StopIteration

    def __enter__(self):
        self._sens.activate()
        self.prepare()
        return self

    def __exit__(self, *args):
        self.cleanup()
        self._sens.deactivate()
        return False


class Fluorometer(Wetlabs):
    """
    Interface to a Wetlabs generic Fluorometer
    """
    eol = b'\r\n'
    wakeup_delay = 0.25
    auto_timeout = 5

    def __init__(self, port):
        """

        :param port: serial port interface
        :type port: pydp.serialcom.Port
        """
        rdr = SyncReader(port)
        rdr.add_scanner(delimiter_scanner, scanner_args=('\r\n',))
        self._sens = Sensor(rdr, Writer(port))


class Flntu(Fluorometer):
    """
    Interface to a Wetlabs FLNTURTD.
    """
    variables = ('chlaflo', 'ntuflo')

    def format_data(self, rawdata):
        """
        Return a formatted version of the output from :meth:`sample`.
        """
        f = rawdata.strip().split('\t')
        return dict(zip(self.variables, (int(f[3]), int(f[5]))))


class Flcd(Fluorometer):
    """
    Interface to a Wetlabs FLCDRTD.
    """
    variables = ('cdomflo',)

    def format_data(self, rawdata):
        """
        Return a formatted version of the output from :meth:`sample`.
        """
        f = rawdata.strip().split('\t')
        return dict(zip(self.variables, (int(f[3]),)))


class Acs(Wetlabs):
    """
    Interface to a Wetlabs AC-S.
    """
    auto_timeout = 3

    def __init__(self, port):
        """

        :param port: serial port interface
        :type port: pydp.serialcom.Port
        """
        rdr = SyncReader(port)
        rdr.add_scanner(acs_scanner)
        self._sens = Sensor(rdr, Writer(port))

    def format_data(self, rawdata):
        return bytes(rawdata)
