#!/usr/bin/env python
#
"""
.. :module: sensors.fsi
    :synopsis: interface to FSI sensors
"""
import gevent
from pydp.sensors import Sensor
from pydp.scanners import delimiter_scanner
from pydp.serialcom import SyncReader, Writer


class AcmPlus(object):
    """
    Interface to an FSI ACM-PLUS acoustic current meter. The sensor
    must be preconfigured for continuous sampling at power-up with
    the following data variable enabled:

    * TX
    * TY
    * HX
    * HY
    * HZ
    * VPATH
    """
    eol = b'\r\n'
    auto_timeout = 5
    variables = ('tx', 'ty',
                 'hx', 'hy', 'hz',
                 'va', 'vb', 'vc', 'vd')

    def __init__(self, port):
        """

        :param port: serial port interface
        :type port: pydp.serialcom.Port
        """
        rdr = SyncReader(port)
        rdr.add_scanner(delimiter_scanner, scanner_args=('\r\n',))
        self._sens = Sensor(rdr, Writer(port))

    def __getattr__(self, name):
        return getattr(self._sens, name)

    def __iter__(self):
        return self

    def next(self):
        t, result = self.recv(timeout=self.auto_timeout)
        if t is not None:
            return t, result
        raise StopIteration

    def prepare(self):
        """
        Prepare the device for sampling.
        """
        # The device displays several lines of status information
        # at power-up. We need to flush this from the input buffer.
        _, data = self._sens.recv(timeout=4)
        while data is not None and len(data) < 40:
            _, data = self._sens.recv(timeout=1)

    def cleanup(self):
        """
        Exit sampling mode.
        """
        self._sens.send('S', 2)

    def __enter__(self):
        self._sens.activate()
        # Give the reader task a chance to accumulate the start-up
        # messages.
        gevent.sleep(0.5)
        self.prepare()
        return self

    def __exit__(self, *args):
        self.cleanup()
        self._sens.deactivate()
        return False

    def format_data(self, rawdata):
        """
        Return a formatted version of the output from :meth:`sample`.
        """
        fields = [float(f.strip()) for f in rawdata.strip().split(',')]
        return dict(zip(self.variables, fields))
