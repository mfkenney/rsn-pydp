#!/usr/bin/env python
#
"""
.. :module: sensors.aanderaa
    :synopsis: interface to Aanderaa Optodes
"""
from pydp.sensors import Sensor
from pydp.scanners import delimiter_scanner
from pydp.serialcom import SyncReader, Writer


class Optode(object):
    """
    Interface to an Aanderaa Optode. Before using this class, the
    sensor should be configured to output Temperature and Calibrated
    Phase at 1hz as follows::

        set mode(smart sensor terminal)
        set interval(1)
        set enable text(no)
        set passkey(1)
        set enable rawdata(yes)
        set enable temperature(yes)
        save

    """
    eol = b'\r\n'
    error = b'*'
    auto_timeout = 3
    variables = ('t', 'doconcs')

    def __init__(self, port):
        """

        :param port: serial port interface
        :type port: pydp.serialcom.Port
        """
        rdr = SyncReader(port)
        rdr.add_scanner(delimiter_scanner, scanner_args=('\r\n',))
        self._sens = Sensor(rdr, Writer(port))

    def __getattr__(self, name):
        return getattr(self._sens, name)

    def __iter__(self):
        return self

    def next(self):
        t, result = self.recv(timeout=self.auto_timeout)
        if t is not None:
            return t, result
        raise StopIteration

    def format_data(self, rawdata):
        """
        Return a formatted version of the output from :meth:`sample`.
        """
        f = rawdata.strip().split('\t')
        return dict(zip(self.variables, (float(f[3]), float(f[4]))))

    def __enter__(self):
        self._sens.activate()
        self.prepare()
        return self

    def __exit__(self, *args):
        self.cleanup()
        self._sens.deactivate()
        return False
