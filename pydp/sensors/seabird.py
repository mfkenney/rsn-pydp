#!/usr/bin/env python
#
"""
.. :module: sensors.seabird
    :synopsis: interface to Seabird CTDs
"""
import gevent
from pydp.sensors import Sensor, SensorError
from pydp.scanners import delimiter_scanner
from pydp.serialcom import Reader, Writer


class Sbe52mp(object):
    """
    Interface to a Seabird 52-MP CTD
    """
    eol = b'\r'
    error = b'?CMD'
    wakeup_delay = 0.25
    auto_timeout = 3
    #: Data Product Names
    variables = ('condwat', 'tempwat', 'preswat', 'tempctd')

    def __init__(self, port):
        """

        :param port: serial port interface
        :type port: pydp.serialcom.Port
        """
        rdr = Reader(port)
        rdr.add_scanner(delimiter_scanner,
                        mode='polled',
                        scanner_args=('S>',))
        rdr.add_scanner(delimiter_scanner,
                        mode='auto',
                        scanner_args=('\r\n',))
        rdr.mode = 'polled'
        self._sens = Sensor(rdr, Writer(port))

    def __getattr__(self, name):
        return getattr(self._sens, name)

    def command(self, cmd, timeout, **kwds):
        """
        Send a command to the instrument and return the response. The
        classes *eol* attribute is appended to each command.

        See :meth:`pydp.sensors.Sensor.command`.
        """
        t, response = self._sens.command(cmd + self.eol, timeout, **kwds)
        if self.error in response:
            raise SensorError(cmd)
        return t, response

    def __iter__(self):
        if self._sens.mode == 'auto':
            return self
        raise ValueError('Sensor is not in auto-sampling mode')

    def next(self):
        if self._sens.mode == 'auto':
            t, result = self.recv(timeout=self.auto_timeout)
            if t is not None:
                return t, result
        raise StopIteration

    def prepare(self, dps_level='L0'):
        """
        Start sampling.
        """
        self.wakeup()
        self.set('pcutoff', '-1')
        if dps_level in ('raw', 'L0'):
            self.set('outputctdoraw', 'y')
        else:
            self.set('outputctdo', 'y')
        self.set('overwritemem', 'y')
        self.command('startprofile', 10)
        self._sens.mode = 'auto'

    def cleanup(self):
        """
        Stop sampling.
        """
        self._sens.mode = 'polled'
        self._sens.send('stopprofile', 3)

    def __enter__(self):
        self._sens.activate()
        self.prepare()
        return self

    def __exit__(self, *args):
        try:
            self.cleanup()
        except EnvironmentError:
            pass
        self._sens.deactivate()
        return False

    def wakeup(self):
        """
        Try to wake-up the device from sleep mode.

        First send a carriage-return and wait for a response. If the wait
        times-out, the device was not asleep so we must send a linefeed to
        get back to the prompt.
        """
        assert self._sens.mode == 'polled'
        try:
            self._sens.command(b'\r', 4)
        except EnvironmentError:
            self._sens.command(b'\n', 4)
        gevent.sleep(self.wakeup_delay)

    def set(self, name, value):
        """
        Change a configuration setting.

        :param name: configuration parameter
        :param value: new setting
        """
        self.command('{0}={1}'.format(name, value), 3)

    def format_data(self, rawdata):
        """
        Return a formatted version of the output from :meth:`sample`.
        """
        fields = [float(f.strip()) for f in rawdata.strip().split(',')]
        if len(fields) == 5:
            # raw data values
            return dict(zip(self.variables, fields[0:4]))
        else:
            return dict(zip(self.variables[0:3], fields[0:3]))
