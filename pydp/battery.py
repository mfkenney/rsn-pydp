#!/usr/bin/env python
#
import struct
from functools import partial
import time


def block_to_string(block):
    """
    Convert an I2C data block to a string

    :param block: list of 1-byte integers
    :return: string value
    """
    # The byte values encode a Pascal-style string where the first
    # integer is the string length
    vals = struct.unpack('32p', struct.pack('32B', *block))
    return vals[0]


class Messages:
    """
    Command code values as specified by the Smart Battery Data
    Specification document.
    """
    REMAINING_CAPACITY_ALARM = 0x01
    REMAINING_TIME_ALARM = 0x02
    BATTERY_MODE = 0x03
    TEMPERATURE = 0x08
    VOLTAGE = 0x09
    CURRENT = 0x0a
    AVERAGE_CURRENT = 0x0b
    MAX_ERROR = 0x0c
    RELATIVE_STATE_OF_CHARGE = 0x0d
    ABSOLUTE_STATE_OF_CHARGE = 0x0e
    REMAINING_CAPACITY = 0x0f
    FULL_CHARGE_CAPACITY = 0x10
    RUN_TIME_TO_EMPTY = 0x11
    AVERAGE_TIME_TO_EMPTY = 0x12
    AVERAGE_TIME_TO_FULL = 0x13
    CHARGING_CURRENT = 0x14
    CHARGING_VOLTAGE = 0x15
    BATTERY_STATUS = 0x16
    CYCLE_COUNT = 0x17
    DESIGN_CAPACITY = 0x18
    DESIGN_VOLTAGE = 0x19
    SPECIFICATION_INFO = 0x1a
    MANUFACTURE_DATE = 0x1b
    SERIAL_NUMBER = 0x1c
    PACK_STATUS = 0x2f    # UltraLife proprietary command

class BlockMessages:
    """
    Commands which return blocks of data rather than words
    """
    MANUFACTURER_NAME = 0x20
    DEVICE_NAME = 0x21
    DEVICE_CHEMISTRY = 0x22


class SpecificationInfo:
    """
    Bit-fields for the SpecificationInfo command. Each value is a bitmask
    and a right-shift.
    """
    REVISION = 0x000f, 0
    VERSION = 0x00f0, 4
    VSCALE = 0x0f00, 8
    IPSCALE = 0xf000, 12


class BatteryStatus:
    """
    Bit-fields for the BatteryStatus command.
    """
    OVER_CHARGED_ALARM = 1 << 15
    TERMINATE_CHARGE_ALARM = 1 << 14
    OVER_TEMP_ALARM = 1 << 12
    TERMINATE_DISCHARGE_ALARM = 1 << 11
    REMAINING_CAPACITY_ALARM = 1 << 9
    REMAINING_TIME_ALARM = 1 << 8
    INITIALIZED = 1 << 7
    DISCHARGING = 1 << 6
    FULLY_CHARGED = 1 << 5
    FULLY_DISCHARGED = 1 << 4


class BatteryMode:
    """
    Bit-fields for the BatteryMode command.
    """
    INTERNAL_CHARGE_CONTROLLER = 1 << 0
    PRIMARY_BATTERY_SUPPORT = 1 << 1
    CONDITION_FLAG = 1 << 7
    CHARGE_CONTROLLER_ENABLED = 1 << 8
    ALARM_MODE = 1 << 13
    CHARGER_MODE = 1 << 14
    CAPACITY_MODE = 1 << 15

# Default I2C address for Smart Batteries
DEFAULT_ADDRESS = 0x0b


class SMBBattery(object):
    """
    Class to represent a so-called Smart Battery with an SMBus interface.
    """
    # Mask of writeable bits in mode register
    mode_mask = 0xe300

    def __new__(cls, bus, address=DEFAULT_ADDRESS):
        """
        Create a new object instance.

        :param bus: SMBus object
        :param address: bus address of battery controller
        :return: a new SMBBattery instance
        """
        obj = object.__new__(cls)
        # Create a method for each Message and BlockMessage code. The
        # method name is a Camel-Case version of the attribute name.
        for msg in dir(Messages):
            if not msg.startswith('_'):
                name = ''.join([e.capitalize() for e in msg.split('_')])
                code = getattr(Messages, msg)
                f = partial(obj.rw_word, code)
                f.__name__ = name
                f.__doc__ = 'Send a %s message to the battery' % name
                setattr(obj, name, f)
        for msg in dir(BlockMessages):
            if not msg.startswith('_'):
                name = ''.join([e.capitalize() for e in msg.split('_')])
                code = getattr(BlockMessages, msg)
                f = partial(obj.readblock, code)
                f.__name__ = name
                f.__doc__ = 'Send a %s message to the battery' % name
                setattr(obj, name, f)
        obj.bus = bus
        obj.addr = address
        obj.t_init = 0
        return obj

    def _initialize(self):
        """
        Battery setup method
        """
        mode = self.BatteryMode()
        # Disable alarm and charging broadcasts and set capacity units
        # to 10mWh
        mode = mode | BatteryMode.ALARM_MODE | BatteryMode.CHARGER_MODE
        mode |= BatteryMode.CAPACITY_MODE
        self.t_init = time.time()
        self.BatteryMode(mode & self.mode_mask)

    @property
    def mode(self):
        return self.BatteryMode()

    @property
    def status(self):
        return self.BatteryStatus()

    @property
    def sn(self):
        """
        :return: battery serial number
        """
        return self.SerialNumber()

    @property
    def date(self):
        """
        :return: date of manufacture as (year, month, day)
        """
        val = self.ManufactureDate()
        day = val & 0x1f
        mon = (val >> 5) & 0x0f
        y = (val >> 9) & 0x7f
        return y + 1980, mon, day

    @property
    def type(self):
        """
        :return: battery make, model, chemistry
        """
        make = block_to_string(self.ManufacturerName()).strip()
        model = block_to_string(self.DeviceName()).strip()
        chem = block_to_string(self.DeviceChemistry()).strip()
        return make, model, chem

    def rw_word(self, msg, value=None):
        """
        Send an SMBus message to the battery to read or write a word

        :param msg: message code word
        :param value: word value to write
        :return: battery controller response word
        """
        if value is not None:
            try:
                rval = self.bus.write_word_data(self.addr, msg, value)
            except IOError:
                rval = self.bus.write_word_data(self.addr, msg, value)
        else:
            try:
                rval = self.bus.read_word_data(self.addr, msg)
            except IOError:
                rval = self.bus.read_word_data(self.addr, msg)
        return rval

    def readblock(self, msg):
        """
        Send an SMBus message to read a block of data.

        :param msg: message code word
        :return: block of data as an array of 1-byte integers
        """
        try:
            return self.bus.read_i2c_block_data(self.addr, msg)
        except IOError:
            return self.bus.read_i2c_block_data(self.addr, msg)


class Mux(object):
    """
    Class to represent an PCA9547D mux used to individually switch each
    Smart Battery onto the I2C bus. This class maintains a registry of all
    muxes on the bus to ensure that only one mux has a channel enabled at
    any given time.
    """
    _devs = {}
    channels = 8
    enable_mask = 0x08

    def __new__(cls, bus, addr):
        obj = Mux._devs.get(addr)
        if not obj:
            obj = object.__new__(cls)
            obj.bus = bus
            obj.addr = addr
            Mux._devs[addr] = obj
        return obj

    def clear(self):
        """
        Disable all mux channels.
        """
        self.bus.write_byte(self.addr, 0)

    def set(self, channel):
        """
        Select and enable the specified channel
        """
        assert 0 <= channel < self.channels
        # Clear any other muxes on the bus
        for addr, mux in Mux._devs.items():
            if addr != self.addr:
                mux.clear()
        self.bus.write_byte(self.addr, self.enable_mask | channel)

    def get(self):
        """
        Return the current mux channel setting
        """
        return self.bus.read_byte(self.addr) & ~self.enable_mask


class Battery(SMBBattery):
    """
    Class to represent a Smart Battery accessed through an I2C bus mux
    """
    def __new__(cls, bus, mux_address, mux_channel):
        """
        Create an object instance.

        :param bus: :class:`smbus.SMBus` object
        :param mux_address: bus address of the mux
        :param mux_channel: mux channel to select this battery
        """
        obj = SMBBattery.__new__(cls, bus)
        obj.mux = Mux(bus, mux_address)
        obj.mux_channel = mux_channel
        obj.address = (mux_address << 8) | mux_channel
        return obj

    def _select(self):
        """
        Make this battery addressable on the bus.
        """
        self.mux.set(self.mux_channel)
        val = self.mux.get()
        assert val == self.mux_channel

    def rw_word(self, msg, value=None):
        """
        Send an SMBus message to the battery to read or write a word

        :param msg: message code word
        :param value: word value to write
        :return: battery controller response word
        """
        self._select()
        return SMBBattery.rw_word(self, msg, value)

    def readblock(self, msg):
        """
        Send an SMBus message to read a block of data.

        :param msg: message code word
        :return: block of data as an array of 1-byte integers
        """
        self._select()
        return SMBBattery.readblock(self, msg)


def median_value(func, n=5):
    """
    Helper function to clean-up noisy readings from a Battery. Runs the
    supplied :class:`Battery` method ``n`` times and returns the median
    value.

    :param func: Battery method
    :param n: number of samples
    :return: median of ``n`` samples.
    """
    assert n > 2
    values = []
    i = 0
    while i < n:
        try:
            values.append(func())
            i += 1
        except IOError:
            pass
    values.sort()
    return values[n // 2]


if __name__ == '__main__':
    import smbus
    bus = smbus.SMBus(0)
    batteries = [
        (0x70, 0),
        (0x70, 1),
        (0x77, 0),
        (0x77, 1)
    ]
    b = []
    for mux, chan in batteries:
        bat = Battery(bus, mux, chan)
        bat._initialize()
        b.append(bat)

    for _ in range(30):
        vals = []
        try:
            for i in range(4):
                x = median_value(b[i].RemainingCapacity)
                vals.append(x)
                time.sleep(0.1)
            print vals
        except IOError:
            print 'Read error'
        except AssertionError:
            print 'Bad mux setting'
