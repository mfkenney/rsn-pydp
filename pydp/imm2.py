#!/usr/bin/env python
#
"""
.. module:: imm2
   :synopsis: interface to a Seabird Inductive Modem Module
"""
from pydp.scanners import imm_scanner, delimiter_scanner
from pydp.serialcom import SyncReader, Writer
import gevent
import logging


class IMMError(Exception):
    def __init__(self, etype, msg):
        self.etype = etype
        self.msg = msg

    def __str__(self):
        return 'IMMError(%r, %r)' % (self.etype, self.msg)


class IMM(object):
    """
    Class to interface with a Seabird Inductive Modem Module (IMM)
    """
    eol = b'\r\n'
    #: Maximum size of data-record queue, ``None`` is unlimited.
    qmax = None
    #: Number of seconds to pause after wake-up
    wakeup_delay = 0.5

    def __init__(self, port):
        """
        Instance initializer.

        :param port: serial port interface
        :type port: pydp.serialcom.Port
        """
        self._reader = SyncReader(port)
        self._reader.add_scanner(imm_scanner, mode='host',
                                 scanner_args=('\nIMM>',))
        self._reader.add_scanner(delimiter_scanner, mode='im',
                                 scanner_args=(b'\r\n',))
        self._reader.mode = 'host'
        self._writer = Writer(port)
        self.settings = dict()

    @property
    def mode(self):
        return self._reader.mode

    @mode.setter
    def mode(self, mode):
        self._reader.mode = mode

    def activate(self):
        self._reader.activate()

    def deactivate(self):
        self._reader.deactivate()

    def __enter__(self):
        self.activate()
        return self

    def __exit__(self, *args):
        self.deactivate()

    def _load_settings(self, force=False):
        if force or (not self.settings):
            t, results = self.command(b'getcd', 8)
            settings = results[0].getchildren()[0]
            self.settings = dict([(k.lower(), v) for k, v in settings.items()])
            self.settings['s/n'] = results[0].get('SerialNumber')

    def send(self, cmd, timeout=None, chunksize=4, delay=0.01):
        """
        Send a command string to the IMM.

        :param cmd: command string
        :param timeout: maximum number of seconds to wait
        :param chunksize: number of characters to send at a time
        :param delay: delay in seconds between chunks
        :return: boolean status
        """
        return self._writer(cmd + self.eol, timeout=timeout,
                            chunksize=chunksize, delay=delay)

    def recv(self, timeout=None):
        """
        Return the latest response from either the IMM (when in host mode) or
        from the remote system (when in im mode). The response is returned as
        a tuple of (<timestamp>, <value>). The <value> is either an ElementTree
        .Element instance (host mode) or a string (im mode).

        :param timeout: number of seconds to wait
        :return: (timestamp, value) or ``None``, ``None`` on timeout
        :rtype: tuple
        """
        t, data = self._reader(timeout=timeout)
        if self.mode == 'host':
            if data is not None:
                for elem in data:
                    if elem.tag.lower() == 'error':
                        raise IMMError(elem.get('type'), elem.get('msg'))
            else:
                self._reader.add_scanner(imm_scanner, mode='host',
                                         scanner_args=('\nIMM>',))
        return t, data

    def wakeup(self):
        """
        Try to wake-up the device from sleep mode.

        First send a carriage return and wait for a response. If the wait
        times-out, the device was not asleep so we must send a linefeed to
        get back to the prompt.
        """
        self.mode = 'host'
        self._reader.drain_queue()
        self._writer(b'\r', timeout=2)
        t, resp = self._reader(timeout=3)
        if resp is None:
            self._writer(b'\n', timeout=2)
            t, resp = self._reader(timeout=3)
            if resp is None:
                raise EnvironmentError((-1, 'No response from IMM'))
        self._reader.drain_queue()
        gevent.sleep(self.wakeup_delay)

    def sleep(self):
        """
        Place the device in sleep mode. When the IMM is in sleep mode, a peer
        can send data directly to the host.
        """
        self.send(b'pwroff', timeout=2)
        t, result = self.recv(timeout=4)
        if t is not None:
            self.mode = 'im'
            self._reader.drain_queue()
        else:
            self._reader.add_scanner(imm_scanner, mode='host',
                                     scanner_args=('\nIMM>',))
            raise EnvironmentError((-1, 'No response from IMM'))

    @property
    def awake(self):
        return self.mode == 'host'

    def command(self, cmd, timeout, **kwds):
        """
        Send a command and return the response.

        :param cmd: command string
        :param timeout: maximum number of seconds to wait
        :return: see :meth:`IMM.recv`
        """
        self.send(cmd, timeout=timeout, **kwds)
        t, response = self.recv(timeout=timeout)
        if t is None:
            raise EnvironmentError((-1, 'No response from IMM'))
        return t, response

    def get(self, name):
        """
        Return the value of an IMM configuration setting.

        :param name: case-insensitive parameter name
        """
        self._load_settings()
        return self.settings[name.lower()]

    def set(self, name, value):
        """
        Update an IMM configuration setting.

        :param name: case-insensitive parameter name.
        :param value: new value
        """
        self._load_settings()
        name = name.lower()
        if name not in self.settings:
            raise AttributeError
        value = str(value)
        cmd = b'set%s=%s' % (name, value)
        self.command(cmd, 3)
        self.settings[name] = value

    def readline(self, timeout):
        """
        Read a single line of data sent from a remote IMM.

        :param timeout: maximum number of seconds to wait
        :return: (timestamp, text) or (``None``, ``None``)
        :rtype: tuple
        """
        if self.awake:
            self.sleep()
        return self.recv(timeout=timeout)

    def drain_queue(self):
        """
        Empty the input queue.
        """
        self._reader.drain_queue()


if __name__ == '__main__':
    import sys
    from pydp.serialcom import Port
    logging.basicConfig(level=logging.DEBUG)
    p = Port(sys.argv[1], baud=9600)
    with open('sessionlog.txt', 'w') as f:
        p.open(sessionlog=f)
        with IMM(p) as imm:
            imm.wakeup()
            value = int(imm.get('tmodem4'))
            if value < 200:
                imm.set('tmodem4', 200)
            imm.set('enablehostwakeupcr', 0)
            print imm.settings
            imm.sleep()
            print 'Waiting for message ... '
            _, msg = imm.readline(None)
            while not msg:
                _, msg = imm.readline(None)
            print repr(msg)
        p.close()
