#!/usr/bin/env python
#
"""
.. module:: adc
   :synopsis: interface to the ADC on Technologic Systems CPU boards.
"""
from dio import peekpoke


class TsAdc(object):
    """
    Access the FPGA interface to the MCP3428 ADC on the TS-81X0
    baseboards.
    """
    base = 0xb0016000
    cfgreg = 0x0
    maskreg = 0x02
    datareg = 0x04
    bits = {
        12: 0 << 2,
        14: 1 << 2,
        16: 2 << 2
    }
    gain = {
        0: 0,
        2: 1,
        4: 2,
        8: 3
    }

    def __init__(self, channels=None, bits=16, gain=0):
        """
        :param channels: list of channels to enable (1-6). ``None`` selects
                         all channels.
        :param bits: sample resolution; 16, 14, or 12 bits
        :param gain: input gain; 0, 2, 4, or 8
        """
        self.channels = channels or range(1, 7)
        if not bits in self.bits:
            raise ValueError('Invalid ADC resolution')
        if not gain in self.gain:
            raise ValueError('Invalid ADC gain setting')
        # Set bit 4 for TS-8100 baseboard
        cfg = 0xad10 | self.bits[bits] | self.gain[gain]
        peekpoke(16, self.base + self.cfgreg, cfg)
        mask = 0
        for c in self.channels:
            mask |= (1 << (c - 1))
        peekpoke(16, self.base + self.maskreg, mask)

    def __getitem__(self, chan):
        if chan in self.channels:
            return peekpoke(16, self.base + self.datareg + 2 * (chan - 1))
        raise IndexError('Invalid ADC channel')


class Ts4800Adc(TsAdc):
    pass


class Ts4200Adc(TsAdc):
    base = 0x30000080
