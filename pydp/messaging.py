#!/usr/bin/env python
#
# IMM-based messaging protocol between the Dock and Profiler.
#
from contextlib import contextmanager
import gevent

MAX_MSG_LEN = 100
CRC32_POLY = 0xedb88320


class CrcError(Exception):
    """
    Raised when a message CRC check fails.
    """
    def __init__(self, context=None):
        self.context = context

    def __str__(self):
        return 'CrcError(%r)' % self.context


class MessageTooLong(Exception):
    """
    Raised when a message exceeds :data:`MAX_MSG_LEN` characters.
    """
    pass


def update_crc32(crc, c):
    """
    Accumulate the 32-bit CRC. *crc* is the current value of the CRC
    (32-bit integer) and *c* is the next data byte (8-bit integer).
    The return value is the new value of the CRC.

    >>> # data is an array of bytes
    >>> crc = reduce(update_crc32, data, 0xffffffffL)
    """
    crc = crc ^ c
    for i in range(8):
        if crc & 0x01:
            crc = (crc >> 1) ^ CRC32_POLY
        else:
            crc = crc >> 1
        crc = crc & 0xffffffff
    return crc


@contextmanager
def line_captured(imm, timeout=4, delay=1.5, force=False, **kwds):
    """
    Access the IMM with the line captured. This allows sending commands
    to a remote modem or host.

    :param imm: interface to IMM
    :type imm: imm2.IMM
    :param timeout: maximum number of seconds to wait for a response
    :param delay: number of seconds to wait after capturing the line
    :param force: if true, capture the line even if it is in-use.
    :raise: :class:`imm2.IMMError`
    """
    do_release = False
    if not imm.awake:
        try:
            imm.wakeup()
        except Exception:
            imm.wakeup()
        gevent.sleep(delay)
        do_release = True
    if force:
        imm.command('fcl', timeout, **kwds)
    else:
        imm.command('captureline', timeout, **kwds)
    gevent.sleep(delay)
    try:
        yield imm
    finally:
        if do_release:
            imm.command('releaseline', timeout, **kwds)
            gevent.sleep(delay)
            imm.sleep()


@contextmanager
def line_released(imm, timeout=4, delay=1.5, force=False, **kwds):
    """
    Access the IMM with the line released and the device in a low-power
    sleep state. This allows listening for messages from a remote host.

    :param imm: interface to IMM
    :type imm: imm2.IMM
    :param timeout: maximum number of seconds to wait for a response
    :param delay: number of seconds to wait after releasing the line
    """
    do_capture = False
    if imm.awake:
        try:
            imm.command('releaseline', timeout, **kwds)
        except Exception:
            imm.command('releaseline', timeout, **kwds)
        gevent.sleep(delay)
        imm.sleep()
        do_capture = True
    try:
        yield imm
    finally:
        if do_capture:
            imm.wakeup()
            gevent.sleep(delay)
            if force:
                imm.command('fcl', timeout, **kwds)
            else:
                imm.command('captureline', timeout, **kwds)


def putmsg(imm, peer_sn, msg, timeout=3, delay=1.5, async=True, force=False):
    """
    Send a message to a peer IMM with serial number *peer_sn*. Delivery is
    normally asynchronous, the message is stored in the peer's Sample Memory.

    *msg* is the byte string to send to the peer. *delay* is the number of
    seconds to wait between capturing the line and sending the message. If
    *async* is ``False``, send the message directly to the peer.

    Returns the XML ``<RemoteReply>`` stanza from the peer IMM.
    """
    reply = None
    if len(msg) > MAX_MSG_LEN:
        raise MessageTooLong()
    if not peer_sn:
        # Test mode. Store in the local sample memory
        imm.command('sampleaddline:' + msg, timeout)
    else:
        with line_captured(imm, timeout=timeout, delay=delay, force=force):
            if async:
                cmd = '!S%s:sampleaddline:%s' % (peer_sn, msg)
            else:
                cmd = '#S%s:%s' % (peer_sn, msg)
            _, results = imm.command(cmd, timeout)
            reply = results[0]
    return reply


def getmsg(imm, timeout=3):
    """
    Generator to iterate over all incoming messages stored in the local IMM
    Sample Memory.
    """
    _, result = imm.command('samplegetlist', timeout)
    samples = result[0]
    assert samples.tag == 'SampleList'
    # The samples are listed in descending order of ID but we must always
    # remove the oldest sample first so we re-sort into ascending order.
    L = [(int(e.get('ID'), 16), e) for e in samples.findall('Sample')]
    L.sort()
    # For each message, retrieve it from the sample-memory, verify the CRC,
    # erase the sample, and return the contents.
    for s_id, e in L:
        _, result = imm.command('samplegetdata:%x' % s_id, timeout)
        msg = result[0].text
        crc = reduce(update_crc32, [ord(c) for c in msg], 0xffffffffL)
        if crc != int(result[0].get('CRC', 0), 16):
            raise CrcError(context={'sample_id': s_id})
        imm.command('sampleerase:%x' % s_id, timeout)
        yield msg
