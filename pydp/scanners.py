#!/usr/bin/env python
#
"""
.. module:: scanners
   :synopsis: scanners for sensor data records
"""
import time
from xml.etree import ElementTree as ET
from string import printable
from functools import wraps
import struct


class ImmParser(ET.TreeBuilder):
    """
    Class to build an XML tree from the output of a
    Seabird Inductive Modem Module (IMM).
    """
    #: List of tags which signal the end of a transmission
    endtags = ('PowerOff',)

    def __init__(self, cb_done, *args, **kwds):
        #: Callback function executed when transmission ends.
        self.cb_done = cb_done
        ET.TreeBuilder.__init__(self, *args, **kwds)

    def end(self, tag):
        elem = ET.TreeBuilder.end(self, tag)
        if elem.tag in self.endtags:
            self.cb_done()


def coroutine(func):
    """
    Decorator to simplify the creation of coroutines.
    """
    @wraps(func)
    def start(*args, **kwargs):
        cr = func(*args, **kwargs)
        cr.next()
        return cr
    return start


@coroutine
def frame_scanner(target, start, end):
    """
    Coroutine to scan data records framed by a START and END character.

    :param target: coroutine to accept data records
    :param start: start character
    :type start: one-element string
    :param end: end character
    :type end: one-element string
    """
    while True:
        c = (yield)
        if c == start:
            contents = bytearray()
            t = time.time()
            c = (yield)
            while c != end:
                contents.append(c)
                c = (yield)
            target.send((t, contents))


@coroutine
def delimiter_scanner(target, delim):
    """
    Coroutine to scan data records ended by a delimiter

    :param target: coroutine to accept data records
    :param delim: delimiter string
    """
    n = len(delim)
    t = None
    while True:
        contents = bytearray()
        c = (yield)
        if t is None:
            t = time.time()
        contents.append(c)
        while contents[-n:] != delim:
            c = (yield)
            contents.append(c)
        target.send((t, contents[:-n]))
        t = None


@coroutine
def imm_scanner(target, delim='IMM>'):
    """
    Coroutine to scan responses from a Seabird Inductive Modem Module.

    :param target: coroutine to accept the responses
    :param delim: response delimiter.
    """
    def _response_done():
        imm_scanner.done = True

    def feedxml(p, c):
        if c in printable:
            p.feed(c)
        else:
            p.feed(r'\x{0:02x}'.format(ord(c)))
    n = len(delim)
    t = None
    while True:
        contents = bytearray()
        imm_scanner.done = False
        parser = ET.XMLParser(target=ImmParser(_response_done))
        parser.feed('<output>')
        c = (yield)
        if t is None:
            t = time.time()
        contents.append(c)
        feedxml(parser, c)
        while contents[-n:] != delim:
            c = (yield)
            contents.append(c)
            feedxml(parser, c)
            if imm_scanner.done:
                break
        parser.feed('</output>')
        target.send((t, parser.close()))
        t = None


@coroutine
def aadi_scanner(target):
    """
    Coroutine to scan responses from sensors using the AADI Smart Sensor
    Protocol (e.g. Optodes)

    :param target: coroutine to accept the responses
    """
    t = None
    while True:
        contents = bytearray()
        c = (yield)
        if t is None:
            t = time.time()
        if c in ('#', '*'):
            while c != '\n':
                contents.append(c)
                c = (yield)
            target.send((t, contents))
            t = None
        else:
            contents.append(c)


@coroutine
def acs_scanner(target):
    """
    Coroutine to scan data records from a Wetlabs Spectral Absorption and
    Attenutation Meter (AC-S). The packet format is documented in Appendix A
    of the sensor User's Guide.
    """
    pktstart = b'\xff\x00\xff\x00'
    n_match = len(pktstart)
    # Data packet ends with a 2-byte checksum and 1 pad byte
    trailer_len = 3
    state = 'findstart'
    contents = bytearray()
    while True:
        c = (yield)
        contents.append(c)
        if state == 'findstart':
            if contents[-n_match:] == pktstart:
                t = time.time()
                contents = bytearray()
                state = 'readlen'
        elif state == 'readlen':
            if len(contents) == 2:
                (n,) = struct.unpack('>h', bytes(contents))
                # The packet length includes the packet start sequence (which
                # has already been read) but does not include the 2-byte
                # checksum and 1 pad byte
                remaining = n - (n_match + 2) + trailer_len
                # Reset the buffer
                contents = bytearray()
                if remaining > 0:
                    state = 'readpacket'
                else:
                    state = 'findstart'
        elif state == 'readpacket':
            remaining -= 1
            if remaining == 0:
                target.send((t, contents[:-trailer_len]))
                contents = bytearray()
                state = 'findstart'
