ZeroMQ Protocols
=================

Profiler Interface
-------------------

Ports and Connections
~~~~~~~~~~~~~~~~~~~~~~

The server opens the following two ports.

  * A COMMAND port (0MQ ROUTER socket) at ipc:///home/rsn/ipc/profiler
  * A PUBLISHER port (0MQ PUB socket) at tcp://*:5500

A client should open at least one connection.

  * A SUBSCRIBER connection (0MQ SUB socket) to tcp://*:5500

A client may open an additional connection:

  * A COMMAND connection (0MQ DEALER socket) to ipc:///home/rsn/ipc/profiler

The COMMAND socket uses ROUTER-DEALER rather than REQ-REP because the server will
send more than a single response and these responses are delivered asynchronously.

Server to Client Updates
~~~~~~~~~~~~~~~~~~~~~~~~~

DATA messages are broadcast on the publisher socket when data is received from the
Profiler over the IMM::

    Frame 0: "DATA"
    Frame 1: data source as a 0MQ string
    Frame 2: Message Pack encoded data record

The *data source* string is either ``sensor``:*sensor_name* for sensor data
records, ``status`` for profiler status, ``battery`` for battery status, or
``charging`` for battery charging status. The *data record* is a three-element
`Message Pack`_ encoded array:

======== ======================= =============================================
Index    Message Pack Type       Description
======== ======================= =============================================
0        int                     data timestamp in seconds since 1/1/1970 UTC
1        int                     microsecond part of the timestamp
2        map                     data sample
======== ======================= =============================================

A Message Pack *map* associates variable names (strings) with values (integer or
floating point numbers). This makes the data record (somewhat) self-describing.

EVENT messages are broadcast on the publisher socket when an event message is
received from the Profiler over the IMM::

    Frame 0: "EVENT"
    Frame 1: Event type as a 0MQ string
    Frame 2: Event details as a 0MQ string

REPLY messages are broadcast on the publisher socket when the Profiler sends a
response to a command::

    Frame 0: "REPLY"
    Frame 1: UUID, 16 bytes
    Frame 2: Profiler response as a 0MQ string

The UUID is mandatory and matches the UUID sent in response to a Client command (see
below).

ERROR messages are broadcast on the publisher socket when a delivery error occurs
for a Profiler command::

    Frame 0: "ERROR"
    Frame 1: UUID, 16 bytes
    Frame 2: error details as a 0MQ string

.. _Message Pack: http://msgpack.org/

Client to Server Commands
~~~~~~~~~~~~~~~~~~~~~~~~~

The Client sends this message to queue a command for later delivery to the
Profiler (the Profiler only accepts commands at the end of a profile)::

    Frame 0: "QUEUE"
    Frame 1: Profiler command as 0MQ string

The Server responds immediately with the following message::

    Frame 0: "QUEUED"
    Frame 1: message-id (UUID), 16 bytes
    Frame 2: queue-length, 4 bytes in network byte order

After the command has been delivered to the Profiler, the Server will send either a
REPLY or an ERROR message::

    Frame 0: "REPLY"
    Frame 1: message-id (UUID), 16 bytes
    Frame 2: Profiler response as a 0MQ string

    Frame 0: "ERROR"
    Frame 1: message-id (UUID), 16 bytes
    Frame 2: error description as 0MQ string


Dock Interface
---------------

Ports and Connections
~~~~~~~~~~~~~~~~~~~~~~

The server opens a 0MQ REP socket at tcp://*:5600. The client opens a REQ socket and
connects to this endpoint.

Data Flow
~~~~~~~~~

The client sends the following command to the server::

    Frame 0: "STATUS?"

The server responds with the following message::

    Frame 0: "DockStatus"
    Frame 1: serialized DockStatus Protocol Buffer message


