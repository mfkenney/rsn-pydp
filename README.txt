Deep Profiler Software
======================

This package contains modules and scripts for the RSN Deep Profiler Project.

Installation
-------------

1. Install prerequisites:

    pip install -r requirements.txt
    
2. Install package:

    python setup.py sdist
    pip install dist/Pydp-$VERSION.tar.gz
    