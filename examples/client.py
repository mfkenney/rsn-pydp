#!/usr/bin/env python
#
"""
client.py [endpoint]
"""
from gevent_zeromq import zmq
import gevent
import gevent.socket
import sys
import curses
from decimal import Decimal
from datetime import datetime
from collections import namedtuple
from pydp.messages import deserialize

Field = namedtuple('Field', 'name convert units')

prec = Decimal('0.001')
div1000 = lambda x: (type(x) == type([])) and x[0]/Decimal(1000) or x/Decimal(1000)
timefmt = lambda x: datetime.utcfromtimestamp(x[0]).isoformat(sep=' ')
MESSAGE = [
    Field('time', timefmt, None),
    Field('profile', None, None),
    Field('state', None, None),
    Field('pressure', lambda x: x.quantize(prec), 'dbars'),
    Field('voltage', lambda x: x[0].quantize(prec), 'volts'),
    Field('current', lambda x: x[0].quantize(prec), 'amps')
]

class FieldDisplay(object):
    def __init__(self, row, col, width, field):
        self.win = curses.newwin(1, width, row, col)
        self.field = field
        self.width = width
        
    def update(self, value=None):
        self.win.addstr(0, 0, self.field.name, curses.A_BOLD)
        self.win.clrtoeol()
        if value is not None:
            if self.field.convert:
                try:
                    value = self.field.convert(value)
                except Exception:
                    value = 'n/a'
            display = '%s %6s' % (value, self.field.units or '')
            self.win.addstr(0, self.width-len(display)-1, display, curses.A_NORMAL)
        self.win.noutrefresh()
   
def run(stdscr, msgdesc, socket, title='Profiler Status Display'):
    stdscr.clear()
    max_row, max_col = stdscr.getmaxyx()
    stdscr.addstr(1, (max_col - len(title))/2, title)
    text = 'HELP:  q=quit'
    stdscr.addstr(max_row-1, 0, text, curses.A_DIM)
    
    widgets = {}
    row = 4
    for field in msgdesc:
        w = FieldDisplay(row, 8, 60, field)
        w.update()
        widgets[field.name] = w
        row += 1
    stdscr.noutrefresh()
    curses.doupdate()
    curses.curs_set(0)
    
    task = gevent.spawn(monitor, socket, widgets)
    
    stdscr.nodelay(1)
    while True:
        gevent.socket.wait_read(sys.stdin.fileno())
        c = stdscr.getch()
        if chr(c) in 'Qq':
            break
    task.kill()
    stdscr.nodelay(0)

def unpack_message(pm):
    """
    Unpack the StatusMessage to a dictionary
    """
    m_div = Decimal(1000)
    d_div = Decimal(10)
    d = pm.todict()['status']
    t = d['time']
    d['time'] = list(divmod(t, 1000000))
    d['pressure'] = Decimal(d['pressure'])/m_div
    if 'voltage' in d:
        d['voltage'] = [Decimal(v)/m_div for v in d['voltage']]
    if 'current' in d:
        d['current'] = [Decimal(c)/m_div for c in d['current']]
    if 'itemp' in d:
        d['itemp'] = [Decimal(t)/d_div for t in d['itemp']]
    if 'humidity' in d:
        d['humidity'] = Decimal(d['humidity'])/d_div
    return d

def monitor(socket, widgets):
    """
    Gevent task to read data from a ZeroMQ socket and update the display.
    """
    while True:
        frames = socket.recv_multipart()
        tag, uuid, pbtype, data = frames
        msg = unpack_message(deserialize(pbtype, data))
        for name in widgets:
            widgets[name].update(msg[name])
        curses.doupdate()

def main():
    try:
        endpoint = sys.argv[1]
    except IndexError:
        endpoint = 'tcp://127.0.0.1:5500'
        
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect(endpoint)
    socket.setsockopt(zmq.SUBSCRIBE, b'DATA')
    curses.wrapper(run, MESSAGE, socket)
    socket.close()
    
if __name__ == '__main__':
    main()