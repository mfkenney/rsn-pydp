#!/usr/bin/env python
#
# Unpack a DPC data archive file.
#
import msgpack
import time
import sys


def format_data(t, data):
    print time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime(t))
    if isinstance(data, dict):
        for k, v in data.items():
            print '\t{0}={1}'.format(k, v)
    else:
        print '\traw={0}...({1:d} bytes)'.format(data[0:16], len(data))


def main():
    try:
        infile = open(sys.argv[1], 'rb')
    except IndexError:
        sys.stderr.write('Missing filename\n')
        sys.exit(1)

    unpacker = msgpack.Unpacker(infile)
    for secs, usecs, data in unpacker:
        format_data(secs, data)


if __name__ == '__main__':
    main()
