#!/usr/bin/env python
#
import sys
import zmq
import simplejson as json
import argparse
import sqlite3
import re


def insert_row(cursor, table, t_secs, t_usecs, data, cols):
    vals = []
    for name in cols:
        m = re.match(r'([a-z]+)_(\d+)', name, re.I)
        if m:
            vals.append(data[m.group(1)][int(m.group(2))])
        else:
            vals.append(data[name])
    tstamp = long(t_secs * 1000000) + t_usecs
    vals.append(tstamp)
    cols.append('timestamp')
    ph = ['?'] * len(vals)
    cmd = 'insert into {0} ({1}) values ({2})'.format(table,
                                                      ','.join(cols),
                                                      ','.join(ph))
    cursor.execute(cmd, tuple(vals))


def monitor(conn, *socks):
    c = conn.cursor()
    poller = zmq.Poller()
    for sock in socks:
        poller.register(sock, zmq.POLLIN)
    while True:
        ready = dict(poller.poll())
        for sock in ready:
            mtype, contents = sock.recv_multipart()
            if mtype == 'DATA':
                msg = json.loads(contents)
                secs, usecs = msg['t']
                data = msg['data']
                names = []
                for name in data.keys():
                    val = data[name]
                    if isinstance(val, tuple) or isinstance(val, list):
                        names.extend([name + '_' + str(i)
                                      for i in range(len(val))])
                    else:
                        names.append(name)
                try:
                    insert_row(c, msg['name'], secs, usecs, data, names)
                except sqlite3.IntegrityError as e:
                    sys.stderr.write(repr(e) + '\n')
                    sys.stderr.write('Skipping {0} @[{1:d}, {2:d}]\n'.format(msg['name'],
                                                                             secs, usecs))
                conn.commit()
            elif mtype == 'EVENT':
                msg = json.loads(contents)
                secs, usecs = msg['t']
                attrs = msg['attrs']
                if msg['name'] == 'profile:start':
                    c.execute('insert into profiles (start, pnum, mode) values (?,?,?)',
                              (secs, attrs['pnum'], attrs['mode']))
                    conn.commit()
                elif msg['name'] == 'profile:end':
                    try:
                        c.execute('update profiles set end=? where pnum=?',
                                  (secs, attrs['pnum']))
                    except sqlite3.IntegrityError:
                        c.execute('insert into profiles (end, pnum) values (?,?)',
                                  (secs, attrs['pnum']))
                    else:
                        conn.commit()


def main():
    """
    Subscribe to Deep Profiler data streams and log the values
    to an SQLite database.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('dbname', help='SQLite database file')
    parser.add_argument('publishers', nargs='+')
    args = parser.parse_args()

    ctx = zmq.Context()
    socks = []
    for endpoint in args.publishers:
        sock = ctx.socket(zmq.SUB)
        sock.connect(endpoint)
        sock.setsockopt(zmq.SUBSCRIBE, b'')
        socks.append(sock)
    try:
        conn = sqlite3.connect(args.dbname)
        monitor(conn, *socks)
    finally:
        conn.close()



if __name__ == '__main__':
    main()
