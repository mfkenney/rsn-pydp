#!/usr/bin/env python
#
# Monitor the temperature sensors on the DSC.
#
import sys
import time
from pydp.adc import Ts4800Adc


def tovolts(c):
    """
    Convert ADC counts to volts.
    """
    return c * 10.24 / 32768.


def main():
    t = time.time()
    adc = Ts4800Adc(channels=(3, 4, 5, 6))
    temps = [tovolts(adc[i]) * 36.71 - 6.02 for i in (3, 4)]
    fields = []
    fields.append(str(int(t)))
    fields.extend(['{0:.2f}'.format(x) for x in temps])
    sys.stdout.write(','.join(fields) + '\n')


if __name__ == '__main__':
    main()
