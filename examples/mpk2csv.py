#!/usr/bin/env python
#
"""
Convert a DPC data archive file to CSV format
"""
import msgpack
import sys
import re


def format_record(outf, t_secs, t_usecs, data, names):
    outf.write('{0:d},{1:d},'.format(t_secs, t_usecs))
    vals = []
    for name in names:
        m = re.match(r'([a-z]+)_(\d+)', name, re.I)
        if m:
            vals.append(data[m.group(1)][int(m.group(2))])
        else:
            vals.append(data[name])
    outf.write(','.join([str(v) for v in vals]) + '\n')


def main():
    try:
        infile = open(sys.argv[1], 'rb')
    except IndexError:
        infile = sys.stdin

    outfile = sys.stdout
    first = True
    unpacker = msgpack.Unpacker(infile)
    for secs, usecs, data in unpacker:
        if first:
            first = False
            if isinstance(data, dict):
                names = []
                for name in data.keys():
                    val = data[name]
                    if isinstance(val, tuple) or isinstance(val, list):
                        names.extend([name + '_' + str(i)
                                      for i in range(len(val))])
                    else:
                        names.append(name)
                names.sort()
                outfile.write(','.join(['t_secs', 't_usecs'] + names) + '\n')
            else:
                raise TypeError('Cannot output raw-data as CSV')
        format_record(outfile, secs, usecs, data, names)


if __name__ == '__main__':
    main()
