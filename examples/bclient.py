#!/usr/bin/env python
#
import redis
import msgpack
import time
import sys


def consumer(rd):
    unpacker = msgpack.Unpacker()
    fmt = '{0} {1:d} V={2:.3f} I={3:.3f} E={4:.1f} T={5:.1f}/{6:.1f}/{7:.1f} {8:.1f}%'
    pubsub = rd.pubsub()
    pubsub.subscribe('data.power')
    for msg in pubsub.listen():
        if msg['type'] == 'message':
            unpacker.feed(msg['data'])
            secs, _, data = unpacker.next()
            t = time.strftime('%Y-%m-%dT%H:%M:%S', time.gmtime(secs))
            n = len(data)
            vsum = sum([rec['voltage'] for rec in data])
            isum = sum([rec['avg_current'] for rec in data])
            esum = sum([rec['energy'] for rec in data])
            csum = sum([rec['rel_charge'] for rec in data])
            temp = [rec['temperature'] for rec in data]
            if temp:
                tmax = max(temp)
                tmin = min(temp)
                tavg = sum(temp) / n
            else:
                tmax, tmin, tavg = 0., 0., 0.

            print fmt.format(
                t, n, vsum / n, isum, esum, tmin, tmax, tavg, csum / n)


def main():
    try:
        host = sys.argv[1]
    except IndexError:
        host = 'localhost'

    rd = redis.StrictRedis(host=host)
    try:
        consumer(rd)
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
