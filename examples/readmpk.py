#!/usr/bin/env python
#
"""
Read a Deep Profiler archive into a Pandas DataFrame
"""
import msgpack
import os
import numexpr as ne
from pandas import Series, DataFrame
from datetime import datetime
from dateutil.tz import tzutc

TYPEMAP = {
    'ctd_1': 'float32',
    'acm_1': 'float32',
    'optode_1': 'float32',
    'flntu_1': 'uint16',
    'flcd_1': 'uint16'
}


def make_series(t_secs, t_usecs, data, names, dtype=None):
    """
    Convert a data-record to a pandas.Series instance.
    """
    t = t_secs + 1.0e-6 * t_usecs
    dt = datetime.utcfromtimestamp(t).replace(tzinfo=tzutc())
    vals = []
    for name in names:
        parts = name.split('.')
        if len(parts) == 2:
            vals.append(data[parts[0]][int(parts[1])])
        else:
            vals.append(data[name])
    return dt, Series(vals, index=names, dtype=dtype)


def readmpk(infile, dtype=None):
    """
    Create a pandas.DataFrame from the contents of a MessagePack encoded
    data file.
    """
    first = True
    rows = []
    index = []
    unpacker = msgpack.Unpacker(infile)
    for secs, usecs, data in unpacker:
        if first:
            first = False
            if isinstance(data, dict):
                names = []
                for name in data.keys():
                    val = data[name]
                    if isinstance(val, tuple) or isinstance(val, list):
                        names.extend([name + '.' + str(i)
                                      for i in range(len(val))])
                    else:
                        names.append(name)
                names.sort()
            else:
                raise TypeError('Cannot read raw-data archives')
        idx, row = make_series(secs, usecs, data, names, dtype=dtype)
        rows.append(row)
        index.append(idx)
    return DataFrame(rows, index=index)


def readarchive(dirname):
    """
    Read an archive directory and return a dictionary of DataFrame instances.
    """
    frames = {}
    for fname in os.listdir(dirname):
        parts = fname.split('_')
        key = '_'.join(parts[:2])
        frames[key] = readmpk(open(os.path.join(dirname, fname), 'r'),
                              dtype=TYPEMAP.get(key))
    return frames


def archive_to_h5(dirlist, store):
    """
    Read the contents of one or more data archive directories into an
    HDF5 data store.
    """
    n_ups, n_downs = 0, 0
    for adir in dirlist:
        base, tstamp, pnum = adir.split('_')
        ar = readarchive(adir)
        for name, df in ar.items():
            path = '/d{0}_{1}/{2}'.format(tstamp, pnum, name)
            store.put(path, df)
            if name == 'mmp_1':
                pnum = int(pnum)
                mode = df['mode'][0]
                entry = DataFrame({'pnum': [pnum], 'tstamp': [tstamp]},
                                  index=[pnum])
                if mode == 'up':
                    store.append('up_profiles', entry)
                elif mode == 'down':
                    store.append('down_profiles', entry)


def add_data_product(df, dp_desc):
    """
    Add a new data product to a DataFrame. The data product becomes a new
    column and is described by *dp_desc*.
    """
    local_vars = {}
    local_vars.update(dp_desc['env'])
    name = dp_desc['name']
    local_vars.update(dict([(v, df[v]) for v in df.columns.values]))
    df[name] = ne.evaluate(dp_desc[name], local_dict=local_vars)
