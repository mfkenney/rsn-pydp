#!/usr/bin/env python
#
# Send a command to the Profiler via the IMM and wait for a response.
#
import struct
import sys
import uuid
import zmq
import os

def main():
    context = zmq.Context()
    cmd_socket = context.socket(zmq.DEALER)
    endpoint = os.path.expanduser('~/ipc/imm-command')
    cmd_socket.connect('ipc://'+endpoint)
    try:
        cmd = sys.argv[1]
    except IndexError:
        cmd = b'testing 1 2 3'
    cmd_socket.send_multipart([b'QUEUE', cmd])
    op, msg_id, qlen = cmd_socket.recv_multipart()
    print 'Message ID: ', uuid.UUID(bytes=msg_id).hex
    print 'Queue length: %d' % struct.unpack('!l', qlen)
    print 'Waiting for reply ... '
    op, m_id, contents = cmd_socket.recv_multipart()
    assert m_id == msg_id
    print op, contents

if __name__ == '__main__':
    main()


