#!/usr/bin/env python
#
# Example script to demostrate how to subscribe to the message stream
# from the Dock Controller.
#
import sys
import zmq
import msgpack


def subscription(sock, sentinel=(), timeout=2500):
    """
    Generator to return messages from a SUB socket until *sentinel*
    is seen or *timeout* expires.
    """
    poll = zmq.Poller()
    poll.register(sock)
    socks = dict(poll.poll(timeout))
    while socks.get(sock) == zmq.POLLIN:
        result = sock.recv_multipart()
        if result[0] not in sentinel:
            yield result
            socks = dict(poll.poll(timeout))
        else:
            break


def format_data(sensor, t, data):
    rec = '{0},{1:d},{2}'.format(sensor, t, str(data))
    print rec


def format_event(event, contents):
    print event, contents


def read_stream(host, port):
    ctx = zmq.Context()
    sub = ctx.socket(zmq.SUB)
    sub.setsockopt(zmq.SUBSCRIBE, 'DATA')
    sub.setsockopt(zmq.SUBSCRIBE, 'EVENT')
    sub.connect('tcp://{0}:{1:d}'.format(host, port))
    for msg in subscription(sub, timeout=20000):
        mtype, subtype, contents = msg
        if mtype == 'DATA':
            secs, usecs, data = msgpack.unpackb(contents)
            format_data(subtype, secs, data)
        elif mtype == 'EVENT':
            format_event(subtype, contents)
    sub.close()


def main():
    try:
        host = sys.argv[1]
    except IndexError:
        host = '127.0.0.1'

    try:
        read_stream(host, 5500)
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
