#!/usr/bin/env python
#
# Store Deep Profiler data in a RethinkDB database.
#
import msgpack
import os
import rethinkdb as r
import argparse
from datetime import datetime
from dateutil.tz import tzutc


def db_setup(conn, db_name):
    if not db_name in r.db_list().run(conn):
        r.db_create(db_name).run(conn)
    db = r.db(db_name)
    return db


def store_dataset(infile, conn, db, name):
    unpacker = msgpack.Unpacker(infile)
    for secs, usecs, data in unpacker:
        if not isinstance(data, dict):
            raise TypeError('Cannot store raw-data archives')
        t = secs + usecs * 1.0e-6
        data['timestamp'] = r.epoch_time(t)
        db.table(name).insert(data).run(conn)


def store_archive(dirname, conn, db, cache_size=1024*1024):
    """
    Read an archive directory and store each data-set.
    """
    tables = db.table_list().run(conn)
    if not 'profiles' in tables:
        db.table_create('profiles',
                        cache_size=cache_size,
                        primary_key='t_start').run(conn)
    _, tstamp, pnum = os.path.basename(dirname).split('_')
    pnum = int(pnum)
    t_start = datetime.strptime(tstamp,
                                '%Y%m%dT%H%M%S').replace(tzinfo=tzutc())
    print 'Storing profile', pnum
    for fname in os.listdir(dirname):
        parts = fname.split('_')
        name = '_'.join(parts[:2])
        if not name in tables:
            db.table_create(name,
                            cache_size=cache_size,
                            primary_key='timestamp').run(conn)
        store_dataset(open(os.path.join(dirname, fname), 'r'), conn, db, name)
    # Grab some info from the mmp_1 table for the profile entry
    query = db.table('mmp_1').filter(r.row['pnum'] == pnum).order_by('timestamp')
    mmp = list(query.run(conn))
    db.table('profiles').insert({'pnum': int(pnum),
                                 't_start': t_start,
                                 'mode': mmp[0]['mode'],
                                 't_end': mmp[-1]['timestamp'],
                                 'pr_start': mmp[0]['pressure'],
                                 'pr_end': mmp[-1]['pressure']}).run(conn)


def main():
    """
    Store Deep Profiler data in a RethinkDB database.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('datasets', help='data archive directories',
                        nargs='+')
    parser.add_argument('--dbname', help='database name (default: fhl)',
                        default='fhl')
    parser.add_argument('--dbhost', help='database host (default: localhost)',
                        default='localhost')
    parser.add_argument('--dbport', help='database port (default: 28015)',
                        default=28015, type=int)
    parser.add_argument('--rootdir', help='root directory for data archive',
                        default='.')
    args = parser.parse_args()

    conn = r.connect(host=args.dbhost, port=args.dbport)
    db = db_setup(conn, args.dbname)
    for dirname in args.datasets:
        path = os.path.join(args.rootdir, dirname)
        store_archive(path, conn, db)


if __name__ == '__main__':
    main()
