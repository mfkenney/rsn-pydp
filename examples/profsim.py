#!/usr/bin/env python
#
# Simulate the periodic messages sent by the Profiler
#
import sys
import time
import SimPy.SimulationRT as sim
import simplejson as json
import random
import math
import logging
import yaml
import gevent
from base64 import b64encode
from pydp import imm2, serialcom
from pydp.messaging import line_captured, line_released
from pydp.messages import StatusMessage

class Battery(object):
    """
    Model the exponential voltage decay of a battery.
    """
    v0 = 13.6
    t_dead = 48*3600
    noise = 0.01
    def __init__(self):
        self.t0 = time.time()
        
    def voltage(self):
        t = (time.time() - self.t0)/self.t_dead
        return math.exp(-t)*self.v0 + random.uniform(-self.noise, self.noise)


class Profiler(sim.Process):
    min_pressure = 1200.
    max_pressure = 2500.
    dock_pressure = 2510.
    dock_time = 120
    docking_time = 30
    turn_around_time = 30
    speed = 0.25
    docking_speed = 0.125
    interval = 4
    
    def __init__(self, serialdev, p_start=None):
        sim.Process.__init__(self, name='profiler')
        self.state = 'STOPPED'
        self.profile = 0
        self.pressure = p_start or self.min_pressure
        self.log = None
        self.serialdev = serialdev

    def sender(self, text):
        try:
            self.imm.command(b'#G0:%s' % (text,), 6)
        except (imm2.IMMError,) as e:
            logging.critical('Error sending command: (%s)', str(e))
        except:
            if self.log:
                self.log.close()
            raise
            
    def create_message(self):
        t = long(time.time() * 1000000L)
        v = int(self.battery.voltage() * 1000)
        cur = int(random.triangular(0.2, 0.4) * 1000)
        msg = StatusMessage(time=t, pressure=int(self.pressure*1000),
                            state=self.state, profile=self.profile,
                            voltage=v, current=cur)
        return b'S:' + b64encode(msg.serialize())

    def wait_for_commands(self, t_limit):
        dt = t_limit - time.time()
        while dt > 0:
            _, line = self.imm.readline(dt)
            if line:
                try:
                    msg = json.loads(str(line).strip())
                    for k, v in msg.items():
                        setattr(self, k, v)
                    reply = {'status': 'ok'}
                except Exception as e:
                    reply = {'status': 'error',
                             'type': str(e)}
                self.imm.send(json.dumps(reply), timeout=5)
            dt = t_limit - time.time()

    def run(self):
        """
        If moving down, advance to :attr:`max_pressure` and then dock. If moving up,
        advance to :attr:`min_pressure` and then turn around.
        """
        self.battery = Battery()
        port = serialcom.Port(self.serialdev)
        port.open(sessionlog=self.log)
        self.imm = imm2.IMM(port)
        self.imm.wakeup()
        self.imm.set('thost2', 1000)
        self.imm.set('enableecho', 0)
        self.imm.set('enablehostwakeupcr', 0)
        dp = self.speed*self.interval
        dp_dock = self.docking_speed*self.interval
        self.state = 'PROFILE_DOWN'

        while True:
            logging.info('Starting down profile')
            with line_captured(self.imm):
                while self.pressure < self.max_pressure:
                    self.pressure += (dp*random.triangular(0.9, 1.1))
                    yield sim.hold, self, self.interval
                    self.sender(self.create_message())
                logging.info('Begin docking')
                self.state = 'DOCKING'
                while self.pressure < self.dock_pressure:
                    self.pressure += (dp_dock*random.triangular(0.9, 1.1))
                    yield sim.hold, self, self.interval
                    self.sender(self.create_message())
                logging.info('Profiler docked')
                self.state = 'DOCKED'
                self.sender(self.create_message())

            # Check for commands while docked
            t_limit = time.time() + self.dock_time
            gevent.sleep(2)
            with line_released(self.imm):
                self.wait_for_commands(t_limit)
            gevent.sleep(1)
            # Get the simulation clock caught up to real-time
            yield sim.hold, self, self.dock_time

            # Recalculate delta-p in case parameters have changed
            dp = self.speed*self.interval
            dp_dock = self.docking_speed*self.interval

            with line_captured(self.imm):
                self.state = 'PROFILE_UP'
                self.profile += 1
                logging.info('Starting up profile')
                while self.pressure > self.min_pressure:
                    self.pressure -= (dp*random.triangular(0.9, 1.1))
                    yield sim.hold, self, self.interval
                    self.sender(self.create_message())
                logging.info('At top of profile')
                self.state = 'STOPPED'
                self.sender(self.create_message())

            t_limit = time.time() + self.turn_around_time
            gevent.sleep(2)
            with line_released(self.imm):
                self.wait_for_commands(t_limit)
            gevent.sleep(1)
            yield sim.hold, self, self.turn_around_time
            self.state = 'PROFILE_DOWN'
            self.profile += 1
            
def show_message(text):
    msg = StatusMessage()
    msg.deserialize(text)
    print json.dumps(msg)
    
def main(args):
    try:
        cfg = yaml.load(open(args[0], 'r'))
    except IndexError:
        sys.stderr.write('Usage: profsim.py configfile\n')
        sys.exit(1)
        
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S')

    for k, v in cfg['profiler'].items():
        setattr(Profiler, k, v)

    sim.initialize()
    prf = Profiler(cfg['simulation']['port'],
                   p_start=cfg['simulation']['p_start'])

    sim.activate(prf, prf.run())
    try:
        sim.simulate(real_time=True, rel_speed=1,
                     until=cfg['simulation']['duration'])
    except KeyboardInterrupt:
        pass
    
if __name__ == '__main__':
    main(sys.argv[1:])