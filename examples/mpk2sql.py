#!/usr/bin/env python
#
"""
Store DP data in an SQLite database
"""
import msgpack
import argparse
import sqlite3
import re
import sys


def insert_row(cursor, table, t_secs, t_usecs, data, cols):
    vals = []
    for name in cols:
        m = re.match(r'([a-z]+)_(\d+)', name, re.I)
        if m:
            vals.append(data[m.group(1)][int(m.group(2))])
        else:
            vals.append(data[name])
    tstamp = long(t_secs * 1000000) + t_usecs
    vals.append(tstamp)
    cols.append('timestamp')
    ph = ['?'] * len(vals)
    cmd = 'insert into {0} ({1}) values ({2})'.format(table,
                                                      ','.join(cols),
                                                      ','.join(ph))
    cursor.execute(cmd, tuple(vals))


def main():
    parser = argparse.ArgumentParser(usage=__doc__)
    parser.add_argument('name', help='sensor name')
    parser.add_argument('dbname', help='SQLite database file')
    parser.add_argument('infiles', help='input archive files',
                        type=argparse.FileType('rb'),
                        nargs='+')
    args = parser.parse_args()

    conn = sqlite3.connect(args.dbname)
    c = conn.cursor()
    for f in args.infiles:
        unpacker = msgpack.Unpacker(f)
        for secs, usecs, data in unpacker:
            names = []
            for name in data.keys():
                val = data[name]
                if isinstance(val, tuple) or isinstance(val, list):
                    names.extend([name + '_' + str(i)
                                  for i in range(len(val))])
                else:
                    names.append(name)
            try:
                insert_row(c, args.name, secs, usecs, data, names)
            except sqlite3.IntegrityError as e:
                sys.stderr.write(repr(e) + '\n')
                sys.stderr.write('Skipping row @[{0:d}, {1:d}]\n'.format(secs, usecs))
        conn.commit()
    conn.close()


if __name__ == '__main__':
    main()
