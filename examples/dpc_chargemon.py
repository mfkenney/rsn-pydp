#!/usr/bin/env python
#
# Monitor the state of the charging process in the DPC.
#
import sys
import redis
import time
from pydp.adc import Ts4200Adc


def tovolts(c):
    """
    Convert ADC counts to volts.
    """
    return c * 10.24 / 32768.


def main():
    rd = redis.StrictRedis()
    t = time.time()
    bstatus = rd.hgetall('battery_status')
    adc = Ts4200Adc(channels=(3, 4, 5, 6))
    temps = [tovolts(adc[i]) * 36.71 - 6.02 for i in (4, 5, 6)]
    fields = []
    fields.append(str(int(t)))
    fields.extend(['{0:.2f}'.format(x) for x in temps])
    for key in ('voltage', 'current', 'energy', 'rel_charge'):
        fields.append(bstatus[key])
    sys.stdout.write(','.join(fields) + '\n')


if __name__ == '__main__':
    main()
