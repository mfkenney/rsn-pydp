#!/usr/bin/env python
#
# Example Profiler monitoring daemon.
#
# Provides a ZeroMQ interface to send and receive messages from the Profiler
# via the Inductive Modem.
#
import sys
from gevent_zeromq import zmq
import gevent
from gevent import spawn
from gevent.queue import Queue, Empty
from gevent import monkey
monkey.patch_all()
import logging
import uuid
import struct
import os
from base64 import b64decode
import yaml
import msgpack
import redis
from pydp.serialcom import Port
from pydp.imm2 import IMM, IMMError
from pydp.messaging import putmsg, line_released
from pydp.messages import StatusMessage, SensorMessage, BatteryMessage, \
    ChargingMessage, pb_to_dict, WrappedMessage


def data_records(msg):
    """
    Generator to construct outgoing data records from an input
    Protocol Buffer message.
    """
    if isinstance(msg, SensorMessage):
        for name, data in pb_to_dict(msg).iteritems():
            secs, usecs = divmod(data['time'], 1000000)
            del data['time']
            source = 'sensor:{0}_1'.format(name)
            yield source, msgpack.packb([secs, usecs, data])
    else:
        if isinstance(msg, StatusMessage):
            source = 'status'
        elif isinstance(msg, BatteryMessage):
            source = 'battery'
        elif isinstance(msg, ChargingMessage):
            source = 'charging'
        data = pb_to_dict(msg)
        secs, usecs = divmod(data['time'], 1000000)
        del data['time']
        yield source, msgpack.packb([secs, usecs, data])


class Controller(object):
    """
    Manage the interface between ZeroMQ and the IMM.

    This class starts two :class:`gevent.Greenlet` based tasks. One task
    monitors a ZeroMQ ROUTER socket for messages which will forwarded over
    the IMM. A second task monitors the IMM for messages which will be
    distributed over a PUB-SUB socket.
    """
    def __init__(self, imm, peer_sn, in_endpoint, out_endpoint,
                 context=None, imm_timeout=5):
        self.imm = imm
        self.peer_sn = peer_sn
        self.in_endpoint = in_endpoint
        self.out_endpoint = out_endpoint
        self.context = context or zmq.Context.instance()
        self.outbox = Queue()
        self.imm_timeout = imm_timeout

    def initialize(self):
        self.evsocket = self.context.socket(zmq.PUB)
        self.evsocket.bind(self.out_endpoint)
        self.evsocket.linger = 0
        self.msgsocket = self.context.socket(zmq.ROUTER)
        self.msgsocket.bind(self.in_endpoint)
        self.msgsocket.linger = 0

    def socket_listen(self):
        while True:
            sender, tag, msg = self.msgsocket.recv_multipart()
            msg_id = uuid.uuid4().hex
            self.outbox.put((sender, msg_id, msg))
            self.msgsocket.send_multipart([sender,
                                           b'QUEUED',
                                           uuid.UUID(hex=msg_id).bytes,
                                           struct.pack('!l',
                                                       self.outbox.qsize())])

    def imm_listen(self):
        msg_class = {
            'S': StatusMessage,
            'D': SensorMessage,
            'B': BatteryMessage,
            'C': ChargingMessage
        }
        rd = redis.StrictRedis()
        with line_released(self.imm):
            while True:
                t_recv, line = self.imm.readline(self.imm_timeout)
                if line:
                    msg = None
                    try:
                        prefix, data = line.strip().split(':', 1)
                        klass = msg_class.get(str(prefix))
                        if klass:
                            msg = klass.deserialize(b64decode(str(data)))
                        else:
                            msg = data
                    except Exception:
                        logging.exception('Cannot decode message: %r', line)

                    if isinstance(msg, WrappedMessage):
                        for source, data_rec in data_records(msg):
                            self.evsocket.send_multipart([b'DATA',
                                                          source,
                                                          data_rec])
                            rd.set(source, data_rec)
                    elif msg:
                        try:
                            event, contents = msg.split(' ', 1)
                        except ValueError:
                            event = msg
                            contents = ''
                        self.evsocket.send_multipart([b'EVENT', event,
                                                      contents])

                else:
                    # Timed-out, check for outgoing messages
                    self.check_outbox()

    def start(self):
        self.initialize()
        self.socket_task = spawn(self.socket_listen)
        self.imm_task = spawn(self.imm_listen)
        return self.socket_task, self.imm_task

    def stop(self):
        self.check_delay = 0
        self.socket_task.kill()
        self.imm_task.kill()
        self.msgsocket.close()
        self.evsocket.close()

    def check_outbox(self):
        """
        Send the next message from the outbox queue.
        """
        sender, msg_id, outmsg = b'', b'', b''
        try:
            logging.info('Checking outbox')
            sender, msg_id, outmsg = self.outbox.get_nowait()
            self.imm.wakeup()
            reply = putmsg(self.imm, self.peer_sn, outmsg, async=False)
            # If remote host is busy, requeue the message
            if reply.find('BUSY'):
                self.outbox.put((sender, msg_id, outmsg))
            else:
                self.msgsocket.send_multipart([sender,
                                               b'REPLY',
                                               uuid.UUID(hex=msg_id).bytes,
                                               reply.text])
                self.evsocket.send_multipart([b'REPLY',
                                              uuid.UUID(hex=msg_id).bytes,
                                              '',
                                              reply.text])
        except Empty:
            logging.info('No outgoing messages')
            pass
        except IMMError as e:
            if e.etype == 'TIMEOUT':
                # Could just be a transient error, requeue the message
                self.outbox.put((sender, msg_id, outmsg))
            else:
                raise
        except Exception as e:
            self.msgsocket.send_multipart([sender,
                                           b'ERROR',
                                           uuid.UUID(hex=msg_id).bytes,
                                           '='.join([e.__class__.__name__,
                                                     str(e)])])
            self.evsocket.send_multipart([b'ERROR',
                                          uuid.UUID(hex=msg_id).bytes,
                                          e.__class__.__name__,
                                          str(e)])
        finally:
            if self.imm.awake:
                self.imm.sleep()


def load_config(cfgfile):
    cfg = yaml.load(open(cfgfile, 'r'))
    for k, v in cfg['sockets'].items():
        if v.startswith('ipc://'):
            pathname = os.path.expanduser(v[6:])
            cfg['sockets'][k] = 'ipc://{0}'.format(pathname)
    return cfg


def main():
    try:
        cfg = load_config(sys.argv[1])
    except IndexError:
        sys.stderr.write('Usage: profmon.py cfgfile\n')
        sys.exit(1)

    logging.basicConfig(level=logging.INFO)
    port = Port(cfg['imm']['device'])
    port.open()
    with IMM(port) as imm:
        imm.wakeup()
        for k, v in cfg['imm']['settings'].items():
            imm.set(k, v)
        imm.sleep()
        ctl = Controller(imm,
                         cfg['imm']['peer'],
                         cfg['sockets']['command'],
                         cfg['sockets']['data'],
                         imm_timeout=cfg['imm']['timeout'])
        try:
            tasks = ctl.start()
            gevent.joinall(tasks)
        finally:
            ctl.stop()

if __name__ == '__main__':
    main()
