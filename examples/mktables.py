#!/usr/bin/env python
#
# Create SQLite tables for the Deep Profiler data.
#
"""
Create SQLite tables for the Deep Profiler data.
"""
import yaml
import sqlite3
import argparse


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('datadesc', help='YAML data description file')
    parser.add_argument('dbname', help='SQLite database file name')
    args = parser.parse_args()

    conn = sqlite3.connect(args.dbname)
    c = conn.cursor()
    c.execute('''create table metadata
                  (sensor text, varname text, units text,
                   precision text, scale real)''')
    c.execute('''create table profiles
                  (start datetime, end datetime, pnum integer
                   mode text)''')
    metadata = []
    with open(args.datadesc) as f:
        cfg = yaml.load(f)
        for k, v in cfg.iteritems():
            columns = ['timestamp integer unique']
            for j, desc in enumerate(v['data']):
                if 'precision' in desc:
                    precision = desc['precision']
                    if desc['precision'] == '1':
                        ctype = 'integer'
                    else:
                        ctype = 'real'
                else:
                    if desc.get('tostr') == str:
                        ctype = 'text'
                    else:
                        precision = '1'
                        ctype = 'integer'
                units = desc.get('units', '')
                nvals = desc.get('nvals', 1)
                scale = desc.get('scale', 1.0)
                if nvals == 1:
                    metadata.append((k, desc['name'], units, precision, scale))
                    columns.append('{0} {1}'.format(desc['name'], ctype))
                else:
                    for i in range(nvals):
                        colname = '{0}_{1:d}'.format(desc['name'], i)
                        metadata.append((k,
                                         colname,
                                         units, precision, scale))
                        columns.append('{0} {1}'.format(colname, ctype))
            cmd = 'create table {0} ({1})'.format(k, ','.join(columns))
            c.execute(cmd)
    c.executemany('insert into metadata values (?, ?, ?, ?, ?)', metadata)
    conn.commit()
    conn.close()


if __name__ == '__main__':
    main()
